import scipy
import numpy as np

class Optimizer():

    alpha = 0.01
    x_0 = None
    length = None
    theta_j = None
    hyp = None
    x_data = None
    hyp_array = []

    def __init__(self, m, y, x):
        length = m
        x_0 = np.ones(length)
        theta_0 = x_0[0]
        theta_j = theta_0 + x_0[1:m]
        x_data = x
        y_data = y
        hyp = np.dot(theta_j[1:m], np.transpose(x_data))

    # Cost function
    def cost_function(m, y_data, hyp):
        cost = 0
        for idx in range(1,m):
            cost = (1/2*m) * (cost + (hyp[m] - y_data[m]) ^ 2)
        return cost

    def objective_function(x, x0, *params):
        m = length
        theta_0 = theta_j[0]
        hyp = np.dot(theta_j[1:m], np.transpose(x_data))
        cost = cost_function(m, y_data, hyp)
        hyp_array.append(hyp)
        return cost
        
    def spl_callback(theta_j):
        m = length
        x_deviation = np.array([])
        for idx in range(1,length):
            np.append(x_deviation, (hyp - y_data[length]) * x_data[length])

        theta_j = theta_j - (alpha/m *(x_deviation))
        return theta_j

    def func_minimize(theta_j, length, method):
        optimization_result = minimize(self.objective_function, theta_j, callback=spl_callback, method=method)
        return optimization_result