import numpy as np
import pandas as pd
import scipy
from scipy.spatial.distance import canberra
import pymysql
import io, os, sys

batch_sql = """
SELECT batch, cluster, npi, specialty_description, drug_name,
specialty_factor_drug FROM `{table}` WHERE drug_name = '{drug_name}' AND 
specialty_description = '{specialty_description}' AND npi = '{npi}';
"""

canberra_distance_sql = """
SELECT claim_per_dollar, supply_per_claim, drug_mean_claim_per_dollar, 
drug_mean_supply_per_claim, drug_std_claim_per_dollar,
drug_std_supply_per_claim, drug_weight, specialty_weight, bene_risk, 
IF(medicare_prvdr_enroll_status = 'E', 1, 0) as enroll_status, npi_claim_count,
pa_specialty_entropy.median_count,
IF(drug_state_category = 'opioid', pa_cost_function.opioid, 
IF(drug_state_category = 'antibiotic', pa_cost_function.antibiotic, 
IF(drug_state_category = 'antipsychotic', pa_cost_function.antipsychotic, 
IF(drug_state_category = 'HRM', pa_cost_function.hrm, 
pa_cost_function.generic)))) as health_risk_ratio
FROM pa_cost_function
INNER JOIN claim_aggregates.pa_specialty_entropy ON 
pa_specialty_entropy.specialty_description = pa_cost_function.specialty_description
WHERE pa_cost_function.drug_name = '{drug_name}' AND 
pa_cost_function.specialty_description = '{specialty_description}' AND pa_cost_function.npi = '{npi}'
ORDER BY 
supply_per_claim, (drug_mean_supply_per_claim / drug_std_supply_per_claim),
(IF(drug_state_category = 'opioid', pa_cost_function.opioid, 
IF(drug_state_category = 'antibiotic', pa_cost_function.antibiotic, 
IF(drug_state_category = 'antipsychotic', pa_cost_function.antipsychotic, 
IF(drug_state_category = 'HRM', pa_cost_function.hrm, 
pa_cost_function.generic)))));
"""

other_data_sql = """
SELECT pa_cost_function.specialty_description, pa_cost_function.drug_name, pa_cost_function.npi, 
pa_kmeans.cluster, pa_kmeans.batch,
claim_per_dollar, supply_per_claim, drug_mean_claim_per_dollar, 
drug_mean_supply_per_claim, drug_std_claim_per_dollar,
drug_std_supply_per_claim, drug_weight, specialty_weight, bene_risk, 
IF(medicare_prvdr_enroll_status = 'E', 1, 0) as enroll_status, npi_claim_count,
pa_specialty_entropy.median_count,
IF(drug_state_category = 'opioid', pa_cost_function.opioid, 
IF(drug_state_category = 'antibiotic', pa_cost_function.antibiotic, 
IF(drug_state_category = 'antipsychotic', pa_cost_function.antipsychotic, 
IF(drug_state_category = 'HRM', pa_cost_function.hrm, 
pa_cost_function.generic)))) as health_risk_ratio
FROM pa_cost_function
INNER JOIN claim_aggregates.pa_specialty_entropy ON 
pa_specialty_entropy.specialty_description = pa_cost_function.specialty_description
INNER JOIN `{table}` pa_kmeans ON
pa_kmeans.specialty_description = pa_cost_function.specialty_description AND 
pa_kmeans.drug_name = pa_cost_function.drug_name AND 
pa_kmeans.npi = pa_cost_function.npi
WHERE pa_cost_function.drug_name = '{drug_name}' AND 
pa_cost_function.specialty_description = '{specialty_description}'
ORDER BY 
supply_per_claim, (drug_mean_supply_per_claim / drug_std_supply_per_claim),
(IF(drug_state_category = 'opioid', pa_cost_function.opioid, 
IF(drug_state_category = 'antibiotic', pa_cost_function.antibiotic, 
IF(drug_state_category = 'antipsychotic', pa_cost_function.antipsychotic, 
IF(drug_state_category = 'HRM', pa_cost_function.hrm, 
pa_cost_function.generic)))));
"""

cluster_batch_data_sql = """
SELECT pa_cost_function.specialty_description, pa_cost_function.drug_name, pa_cost_function.npi, 
claim_per_dollar, supply_per_claim, drug_mean_claim_per_dollar, 
drug_mean_supply_per_claim, drug_std_claim_per_dollar,
drug_std_supply_per_claim, pa_factors.drug_weight, pa_factors.specialty_weight, bene_risk, 
IF(medicare_prvdr_enroll_status = 'E', 1, 0) as enroll_status, npi_claim_count,
pa_specialty_entropy.median_count,
IF(drug_state_category = 'opioid', pa_cost_function.opioid, 
IF(drug_state_category = 'antibiotic', pa_cost_function.antibiotic, 
IF(drug_state_category = 'antipsychotic', pa_cost_function.antipsychotic, 
IF(drug_state_category = 'HRM', pa_cost_function.hrm, 
pa_cost_function.generic)))) as health_risk_ratio
FROM pa_cost_function
INNER JOIN claim_aggregates.pa_specialty_entropy ON 
pa_specialty_entropy.specialty_description = pa_cost_function.specialty_description
INNER JOIN prescription_aggregates.pa_factors ON
pa_factors.specialty_description = pa_cost_function.specialty_description
AND pa_factors.drug_name = pa_cost_function.drug_name
INNER JOIN `{table}` pa_kmeans ON
pa_kmeans.specialty_description = pa_cost_function.specialty_description
AND pa_kmeans.drug_name = pa_cost_function.drug_name
AND pa_kmeans.npi = pa_cost_function.npi
WHERE pa_kmeans.cluster = '{cluster}' AND 
pa_kmeans.batch = '{batch}'
"""

opt_connection = pymysql.connect(user='root', password='root',
                                 host='localhost',
                                 db='optimization_tables')

def get_batches_npi(drug_name, specialty_description, npi, table='pa_kmeans_y_pred_spl_factor_drug'):
    cursor = opt_connection.cursor()
    cursor.execute(batch_sql.format(drug_name=drug_name, specialty_description=specialty_description,
                    npi=npi, table=table))
    batches_npi = cursor.fetchall()
    cursor.close()
    return batches_npi

def get_canberra_distance_npi(drug_name, specialty_description, npi, table='pa_kmeans_y_pred_spl_factor_drug'):
    cursor = opt_connection.cursor()
    cursor.execute(canberra_distance_sql.format(drug_name=drug_name, specialty_description=specialty_description,
                    npi=npi, table=table))
    canberra_npi = cursor.fetchall()
    cursor.close()
    return canberra_npi

def get_other_data(drug_name, specialty_description, npi, table='pa_kmeans_y_pred_spl_factor_drug'):
    cursor = opt_connection.cursor()
    cursor.execute(other_data_sql.format(drug_name=drug_name, specialty_description=specialty_description,
    table=table))
    other_npi_data = cursor.fetchall()
    cursor.close()
    return other_npi_data

def get_cluster_batch_data(cluster, batch, table='pa_kmeans_y_pred_spl_factor_drug'):
    cursor = opt_connection.cursor()
    cursor.execute(cluster_batch_data_sql.format(cluster=cluster, batch=batch, table=table))
    cluster_batch_data = cursor.fetchall()
    cursor.close()
    return cluster_batch_data

if __name__ == "__main__":
    
    print("The canberra distance for the same Domain records are: ")

    canberra_distance_array = []
    for item in other_npi_data:
        canberra_distance_array.append(scipy.spatial.distance.canberra(list(canberra_npi[0]), list(item[5:])))

    idx = 0
    for item in other_npi_data:
        print("The distance metric in comparing ('XOLAIR', 'Allergy/Immunology', '1851444269', '{0}', '{1}') with: ".
            format(cluster, batch))
        print(" ('{drug_name}', '{specialty}', '{npi}', '{cluster}', '{batch}')".format(drug_name=item[1], specialty=item[0],
            npi=item[2], batch=item[4], cluster=item[3]))
        print(" is: " + canberra_distance_array[idx].__str__() + "\n")
        idx = idx + 1

    print("The canberra distance with the same Cluster Batch records are: ")

    canberra_cluster = []
    for item in cluster_batch_data:
        canberra_cluster.append(scipy.spatial.distance.canberra(canberra_npi, item[3:]))

    idx = 0
    for item in cluster_batch_data:
        
        print("The distance metric in comparing ('XOLAIR', 'Allergy/Immunology', '1851444269') with: ")
        print(" ('{drug_name}', '{specialty}', '{npi}')".format(drug_name=item[1], specialty=item[0],
            npi=item[2]))
        print(" is: " + canberra_cluster[idx].__str__() + "\n")
        idx = idx + 1

