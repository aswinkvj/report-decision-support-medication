import numpy as np
import pandas as pd

from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import GradientBoostingRegressor

from Database import Partition, Database
import CostFunction
import math
import io
import sys, os
import argparse
import pickle
from Plotting import plot_decision_path

args = dict()

partition = None

parser = argparse.ArgumentParser(description='The Gradient Boosting Regressor - Ensemble Boosting Method')

parser.add_argument('--state', action='store', default=False)
parser.add_argument('--write-database', action='store', default=False)
parser.add_argument('--write-pickle', action='store', default=False)

parser.add_argument('--run-boosting', action='store', default=False)

args = parser.parse_args()
STATE = args.state

PICKLE_PATH = os.path.abspath("../" + os.path.dirname(__file__)) + "\\Notebook\\Exports\\metaobjects\\cost_quality\\" + STATE

if not (os.path.exists(PICKLE_PATH)):
    os.mkdir(PICKLE_PATH)
    print("The path for Pickle Object Storage has been created")
else:
    print("The path to the Pickle Object Storage Directory for the State = '" + STATE + "' exists")

def costfunction_get_safety_z_cost_function_trawling_dataframe(start, limit):
    result = {'X': [], 'y': []}
    cursor = database.opt_connection.cursor()
    cursor.execute(database_get_safety_z_cost_function_trawling_dataframe(start, limit))
    X = cursor.fetchall()
    for item in X:
        result['X'].append(list(item))
    cursor.close()

    cursor = database.opt_connection.cursor()
    cursor.execute(database.get_spl_weight_cost_function(start, limit))
    y = cursor.fetchall()
    for item in y:
        result['y'].append(item[0])
    cursor.close()
    return result

def database_get_safety_z_cost_function_trawling_dataframe(start, limit):
    global partition
    return """
        SELECT pa_cost_function.specialty_description,
        pa_cost_function.drug_name, pa_cost_function.npi
        claim_per_dollar, supply_per_claim,
        CAST(specialty_std_claim_per_dollar / specialty_mean_claim_per_dollar AS DECIMAL(18,15)), 
        CAST(specialty_std_supply_per_claim / specialty_mean_supply_per_claim AS DECIMAL(18,15)),
        """ + partition.state + """_specialty_entropy.npi_count,
        npi_drug_cost, npi_day_supply, """ + partition.state + """_specialty_entropy.drug_count
        FROM """ + partition.state + """_cost_function
        INNER JOIN claim_aggregates.""" + partition.state + """_specialty_entropy ON 
        """ + partition.state + """_specialty_entropy.specialty_description = 
        """ + partition.state + """_cost_function.specialty_description
        ORDER BY 
        claim_per_dollar, supply_per_claim
        LIMIT {0},{1}
        """.format(start, limit)

if __name__ == "__main__":

    if args.run_boosting == '1':
        
        limit = 18446744073709551615
        regressor = GradientBoostingRegressor(n_estimators=100)

        partition = Partition(STATE)
        database = Database(partition)
        costfunction = CostFunction.CostFunction(database)

        z_safety = costfunction_get_safety_z_cost_function_trawling_dataframe(0, limit)

        print("Data has been fetched from the Database")

        X = np.array(z_safety['X'])
        y = np.array(z_safety['y'])

        regressor.fit(X[:, 3:], y)

        print("Gradient Boosting Regressor has been fitted")

    if args.write_pickle == '1':

        pickle.dump(regressor.apply(X[:, 3:]), open("pa_cost_function_estimators.pkl", "wb"))

        output = io.StringIO()
        idx = 0
        for estimator in regressor.estimators_:
            file_object = open("logs\\estimator.log", "a+")
            file_object.write("The Decision Tree: " + idx.__str__())
            plot_decision_path.plot_decision_path(estimator[0], output)
            file_object.write(output.getvalue())
            file_object.write("\r\n\r\n\r\n---------------------------------------------")
            idx = idx + 1
            
            file_object.flush()
            file_object.close()

    if args.write_database == '1':

        partition = Partition(STATE)
        database = Database(partition)
    
        X_test = pickle.load(open("pa_cost_function_estimators.pkl", "rb"))

        cursor = database.presc_connection.cursor()

        database.presc_connection.begin()

        try:
            idx = 0
            # order by claim_per_dollar and supply_per_claim
            while(idx < len(X_test)):
                test_item = X_test[idx, :]

                cursor.execute("""
                    INSERT INTO pa_cost_function_estimator_indices
                    VALUES
                    ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, 
                    {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21}, {22}, {23}, {24}, {25}, 
                    {26}, {27}, {28}, {29}, {30}, {31}, {32}, {33}, {34}, {35}, {36}, {37}, {38}, {39}, 
                    {40}, {41}, {42}, {43}, {44}, {45}, {46}, {47}, {48}, {49}, {50}, {51}, {52}, {53}, 
                    {54}, {55}, {56}, {57}, {58}, {59}, {60}, {61}, {62}, {63}, {64}, {65}, {66}, {67}, 
                    {68}, {69}, {70}, {71}, {72}, {73}, {74}, {75}, {76}, {77}, {78}, {79}, {80}, {81}, 
                    {82}, {83}, {84}, {85}, {86}, {87}, {88}, {89}, {90}, {91}, {92}, {93}, {94}, {95}, 
                    {96}, {97}, {98}, {99}); """.format(*test_item))
                
                idx = idx + 1

                if idx % 1000 == 0:
                    database.presc_connection.commit()

        except Exception as e:
            raise e
            database.opt_connection.rollback()

        database.opt_connection.commit()
        database.opt_connection.close()
        cursor.close()