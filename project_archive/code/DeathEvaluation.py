import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
import pymysql
import matplotlib
import seaborn
import argparse

from sklearn.tree import DecisionTreeRegressor, DecisionTreeClassifier
from sklearn.tree import export_graphviz

from Database import Database, Partition

parser = argparse.ArgumentParser(description='The Death Evaluation segment involving the analysis of the classifier')

parser.add_argument('--write-database', action='store', default=False, dest=WRITE_DATABASE)
parser.add_argument('--write-pickle', action='store', default=False, dest=WRITE_PICKLE)

PICKLE_PATH = os.path.dirname(os.path.dirname((__file__))) + "/Notebook/Exports/metaobjects/sigh/" + STATE

if not (os.path.exists(PICKLE_PATH)):
    os.mkdir(PICKLE_PATH)
    print("The path for Pickle Object Storage has been created")
else:
    print("The path to the Pickle Object Storage Directory for the State = '" + STATE + "' exists")

class DeathEvaluation():

    classifier = DecisionTreeClassifier(criterion='entropy', max_depth=2, min_samples_split=12, 
                                   min_samples_leaf=1, max_features=3,
                                   max_leaf_nodes=23)

    regressor = DecisionTreeRegressor(max_depth=2, min_samples_split=12, 
                                    min_samples_leaf=1, max_features=3,
                                    max_leaf_nodes=23)

    net_total_claim_count = 1258795672.00
    net_total_30_day_fill_count = 1842106437.9756508

    self.state_ptr = []
    self.state_data = []
    self.sigh = []
    self.deaths = []
    self.population = []
    self.death_ratio = []
    self.states = None

    def get_states_integration():
        cursor = database.backup_connection.cursor()
        cursor.execute(database.get_states_integration())
        self.states = cursor.fetchall()
        cursor.close()

    def get_death_variance():
        for state in self.states:
            self.deaths.append(state[4])
            self.population.append(state[5])
            self.death_ratio.append(state[2]*10000)
        return variance_deaths = np.var(deaths)

    def get_sigh_values():
        for state in self.states:
            state_data.append([state[2]*10000, state[4], state[5]])
            sigh.append((state[2]*10000)**2 + 0.5 * variance_deaths/100000)
            state_ptr.append(state[1])

    def fit_classifier():
        classifier.fit(X=self.state_data, y=self.state_ptr)
    
    def get_decision_path():
        # Code sample from understanding decision path
        n_nodes = classifier.tree_.node_count
        children_left = classifier.tree_.children_left
        children_right = classifier.tree_.children_right
        feature = classifier.tree_.feature
        threshold = classifier.tree_.threshold

        # The tree structure can be traversed to compute various properties such
        # as the depth of each node and whether or not it is a leaf.
        node_depth = np.zeros(shape=n_nodes, dtype=np.int64)
        is_leaves = np.zeros(shape=n_nodes, dtype=bool)
        stack = [(0, -1)]  # seed is the root node id and its parent depth
        while len(stack) > 0:
            node_id, parent_depth = stack.pop()
            node_depth[node_id] = parent_depth + 1

            # If we have a test node
            if (children_left[node_id] != children_right[node_id]):
                stack.append((children_left[node_id], parent_depth + 1))
                stack.append((children_right[node_id], parent_depth + 1))
            else:
                is_leaves[node_id] = True

        print("The binary tree structure has %s nodes and has "
            "the following tree structure:"
            % n_nodes)
        for i in range(n_nodes):
            if is_leaves[i]:
                print("%snode=%s leaf node." % (node_depth[i] * "\t", i))
            else:
                print("%snode=%s test node: go to node %s if X[:, %s] <= %s else to "
                    "node %s."
                    % (node_depth[i] * "\t",
                        i,
                        children_left[i],
                        feature[i],
                        threshold[i],
                        children_right[i],
                        ))

class PickleWrites():
    
    @staticmethod
    def write_pickle_objects(factoranalysis):
        attributes = dir(factoranalysis)
        for attribute in attributes:
            if not os.path.exists(PICKLE_PATH + "/" + attribute + ".pkl"):
                pickle.save(factoranalysis[attribute], PICKLE_PATH + "/" + attribute + ".pkl")
            else:
                print("Path exists: " + PICKLE_PATH + "/" + attribute + ".pkl")


if _name__ == "__main__":

    print(" Death Evaluation running for State = '" + STATE + "'")

    partition = Partition(STATE)
    database = Database(partition)
    deathevaluation = DeathEvaluation()

if WRITE_PICKLE == True:
    PickleWrites().write_pickle_objects(deathevaluation)