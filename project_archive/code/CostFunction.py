import numpy as np
import pandas as pd
import matplotlib
import scipy
import sys, os
import pymysql
import scipy
import pickle
import argparse
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.ensemble import AdaBoostRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn import metrics
from Plotting import plot_bias_variance, visuals

from Database import Database, Partition
from sklearn.cluster import AffinityPropagation
from sklearn.cluster import AgglomerativeClustering

# Avoid calling main during module import
STATE, WRITE_DATABASE, WRITE_PICKLE = [None] * 3

args = dict()

try:
    if sys.argv.index('CostFunction.py') == 0:

        COMMANDLINE_MODE = True

        parser = argparse.ArgumentParser(description='The Cost Function involving Newton-CG, L-BFGS; \
        Optimization involving Sigmoid with CG and LBFGS; and Boosters Involving AdaBoost, GradientBooster and Random Forest')

        parser.add_argument('--retention-safety', action="store", default=True)
        parser.add_argument('--retention-health', action="store", default=True)
        parser.add_argument('--growth-safety', action="store", default=True)
        parser.add_argument('--growth-health', action="store", default=True)
        parser.add_argument('--retention-latent-safety', action="store", default=True)
        parser.add_argument('--retention-latent-health', action="store", default=True)

        parser.add_argument('--state', action='store', default=False)
        parser.add_argument('--write-database', action='store', default=False)
        parser.add_argument('--write-pickle', action='store', default=False)

        parser.add_argument('--run-affinity', action='store', default=False)

        args = parser.parse_args()
        STATE = args.state

        PICKLE_PATH = os.path.abspath("../" + os.path.dirname(__file__)) + "\\Notebook\\Exports\\metaobjects\\cost_quality\\" + STATE

        if not (os.path.exists(PICKLE_PATH)):
            os.mkdir(PICKLE_PATH)
            print("The path for Pickle Object Storage has been created")
        else:
            print("The path to the Pickle Object Storage Directory for the State = '" + STATE + "' exists")

    else:
        COMMANDLINE_MODE = False

except Exception as e:
    COMMANDLINE_MODE = False
    print("CostFunction.py Not in Command line Mode")

class CostFunction():

    database = None

    safety_regressor_y = None
    safety_regressor_ya = None
    health_regressor_y = None
    health_regressor_ya = None

    X_train = None
    y_train = None
    X_test = None
    y_test = None        
    
    def __init__(self, db, r1=None, r2=None, r3=None, r4=None):
        self.database = db
        self.safety_regressor_y = r1
        self.safety_regressor_ya = r2
        self.health_regressor_y = r3
        self.health_regressor_ya = r4
        # Fitting Cost per claim and Claim per DOLLAR

    def __dir__(self):
        return []

    def regressors(self):
        return (safety_regressor_y, safety_regressor_ya, health_regressor_y, health_regressor_ya)
        
    def regression_specialty_weight(self, X, y):
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
        # Comparing with the z and λ vectors
        # The z and Safety grid
        self.safety_regressor_y.fit(X_train, y_train)
        y_pred = self.safety_regressor_y.predict(X)
        return (X_train, X_test, y_train, y_test, y_pred)

    def aff_specialty_weight(self, X, y, cluster):
        cluster.fit(X, y)
        return cluster.predict(X)

    def agg_specialty_weight(self, X, y, cluster):
        cluster.fit(X, y)
        return cluster.fit_predict(X)

    def aff_score(self, X, y, y_pred):
        return metrics.adjusted_mutual_info_score(y, y_pred)

    def regression_drug_weight(self, X, y):
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
        # The z and Health grid
        self.health_regressor_y.fit(X_train, y_train)
        y_pred = self.health_regressor_y.predict(X)
        return y_pred

    def regression_drug_factor_specialty(self):
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
        # The λ and Safety grid
        self.safety_regressor_ya.fit(X_train, y_train, class_weight=self.spl_dict)
        y_pred = self.safety_regressor_ya.predict(X)
        return y_pred

    def regression_specialty_factor_drug(self):
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
        # The λ and Health grid
        self.health_regressor_ya.fit(X_train, y_train, class_weight=self.drug_dict)
        y_pred = self.health_regressor_ya.predict(X)
        return y_pred

    def get_safety_z_cost_function(self, start, limit):
        result = {'X': [], 'y': []}
        cursor = self.database.opt_connection.cursor()
        cursor.execute(self.database.get_safety_z_cost_function(start, limit))
        X = cursor.fetchall()
        for item in X:
            result['X'].append(list(item))
        cursor.close()

        cursor = self.database.opt_connection.cursor()
        cursor.execute(self.database.get_spl_weight_cost_function(start, limit))
        y = cursor.fetchall()
        for item in y:
            result['y'].append(item[0])
        cursor.close()
        return result

    def get_safety_z_cost_function_trawling(self, start, limit):
        result = {'X': [], 'y': []}
        cursor = self.database.opt_connection.cursor()
        cursor.execute(self.database.get_safety_z_cost_function_trawling(start, limit))
        X = cursor.fetchall()
        for item in X:
            result['X'].append(list(item))
        cursor.close()

        cursor = self.database.opt_connection.cursor()
        cursor.execute(self.database.get_spl_weight_cost_function(start, limit))
        y = cursor.fetchall()
        for item in y:
            result['y'].append(item[0])
        cursor.close()
        return result

    def get_safety_z_cost_function_maximised(self, start, limit):
        result = {'X': [], 'y': []}
        cursor = self.database.opt_connection.cursor()
        cursor.execute(self.database.get_safety_z_cost_function_maximised(start, limit))
        X = cursor.fetchall()
        for item in X:
            result['X'].append(list(item))
        cursor.close()

        cursor = self.database.opt_connection.cursor()
        cursor.execute(self.database.get_spl_weight_cost_function(start, limit))
        y = cursor.fetchall()
        for item in y:
            result['y'].append(item[0])
        cursor.close()
        return result

    def get_safety_z_cost_function_optimized(self, start, limit):
        result = {'X': [], 'y': []}
        cursor = self.database.opt_connection.cursor()
        cursor.execute(self.database.get_safety_z_cost_function_optimized(start, limit))
        X = cursor.fetchall()
        for item in X:
            result['X'].append(list(item))
        cursor.close()

        cursor = self.database.opt_connection.cursor()
        cursor.execute(self.database.get_spl_weight_cost_function(start, limit))
        y = cursor.fetchall()
        for item in y:
            result['y'].append(item[0])
        cursor.close()
        return result

    def get_safety_z_cost_function_minimised(self, start, limit):
        result = {'X': [], 'y': []}
        cursor = self.database.opt_connection.cursor()
        cursor.execute(self.database.get_safety_z_cost_function_minimised(start, limit))
        X = cursor.fetchall()
        for item in X:
            result['X'].append(list(item))
        cursor.close()

        cursor = self.database.opt_connection.cursor()
        cursor.execute(self.database.get_spl_weight_cost_function(start, limit))
        y = cursor.fetchall()
        for item in y:
            result['y'].append(item[0])
        cursor.close()
        return result

    def get_safety_z_classifier_function(self, start=0, limit=1):
        result = {'X': [], 'y': []}
        cursor = self.database.opt_connection.cursor()
        cursor.execute(self.database.get_safety_z_cost_function(start, limit))
        X = cursor.fetchall()
        for item in X:
            result['X'].append(list(item))
        cursor.close()

        cursor = self.database.opt_connection.cursor()
        cursor.execute(self.database.get_specialty_cost_function(start, limit))
        y = cursor.fetchall()
        for item in y:
            result['y'].append(item[0])
        cursor.close()
        return result

    def get_health_z_cost_function(self, start, limit):
        result = {'X': [], 'y': []}
        cursor = self.database.opt_connection.cursor()
        cursor.execute(database.get_health_z_cost_function(start, limit))
        X = cursor.fetchall()
        for item in X:
            result['X'].append(list(item))
        cursor.close()

        cursor = self.database.opt_connection.cursor()
        cursor.execute(self.database.get_drug_weight_cost_function(start, limit))
        y = cursor.fetchall()
        for item in y:
            result['y'].append(item[0])
        cursor.close()
        return result

    def get_health_z_classifier_function(self, start, limit):
        result = {'X': [], 'y': []}
        cursor = self.database.opt_connection.cursor()
        cursor.execute(database.get_health_z_cost_function(start, limit))
        X = cursor.fetchall()
        for item in X:
            result['X'].append(list(item))
        cursor.close()

        cursor = self.database.opt_connection.cursor()
        cursor.execute(self.database.get_drug_cost_function(start, limit))
        y = cursor.fetchall()
        for item in y:
            result['y'].append(item[0])
        cursor.close()
        return result

    def get_safety_lambda_cost_function(self, start, limit):
        result = {'X': [], 'y': []}
        cursor = self.database.opt_connection.cursor()
        cursor.execute(self.database.get_safety_lambda_cost_function(start, limit))
        X = cursor.fetchall()
        for item in X:
            result['X'].append(list(item))
        cursor.close()

        cursor = self.database.opt_connection.cursor()
        cursor.execute(self.database.get_drug_factor_specialty_cost_function(start, limit))
        y = cursor.fetchall()
        for item in y:
            result['y'].append(item[0])
        cursor.close()
        return result

    def get_health_lambda_cost_function(self, start, limit):
        result = {'X': [], 'y': []}
        cursor = self.database.opt_connection.cursor()
        cursor.execute(self.database.get_health_lambda_cost_function(start, limit))
        X = cursor.fetchall()
        for item in X:
            result['X'].append(list(item))
        cursor.close()

        cursor = self.database.opt_connection.cursor()
        cursor.execute(self.database.get_specialty_factor_drug_cost_function(start, limit))
        y = cursor.fetchall()
        for item in y:
            result['y'].append(item[0])
        cursor.close()
        return result


class PickleWrites():
    
    @staticmethod
    def write_pickle_objects(costfunction):
        attributes = dir(costfunction)
        for attribute in attributes:
            if not os.path.exists(PICKLE_PATH + "\\" + attribute + ".pkl"):
                pickle.save(costfunction[attribute], PICKLE_PATH + "\\" + attribute + ".pkl")
            else:
                print("Path exists: " + PICKLE_PATH + "\\" + attribute + ".pkl")


if __name__ == "__main__" and COMMANDLINE_MODE == True:

    try:

        limit = 18446744073709551615

        print("\nEnter 'NA' for Not Applicable")
        
        if 'state' not in args:
            STATE = input("\nEnter the State: ")
        else:
            STATE = args.state
        
        if not WRITE_DATABASE:
            WRITE_DATABASE = input("\nShould a database entry be written, enter 1 or 0: ")

        if not WRITE_PICKLE:
            WRITE_PICKLE = input("\nShould the variables be serialized for Reporting, enter 1 or 0: ")

        print("\n Cost Functions and Optimization running for State = '" + STATE + "'")

        partition = Partition(STATE)
        database = Database(partition)
        costfunction = CostFunction(database)

    except Exception as e:
        print("Main exception")
        raise e

if WRITE_PICKLE == True:
    PickleWrites().write_pickle_objects(factoranalysis)

    print("Pickle Objects have been created")