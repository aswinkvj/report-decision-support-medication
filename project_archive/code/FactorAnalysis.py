import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.mixture import GaussianMixture
from sklearn.covariance import EmpiricalCovariance
import os, sys
import math
import pymysql
import pymysql.cursors
import seaborn
import scipy
import matplotlib
import argparse
import pickle
from Database import Database, Partition

TAU, PEARSONR, STATE, WRITE_DATABASE, WRITE_PICKLE, WRITE_COEFF = [None] * 6

parser = argparse.ArgumentParser(description='The Factor Analysis Algorithm')

parser.add_argument('--tau', action="store", default=True)
parser.add_argument('--pearsonr', action="store", default=True)
parser.add_argument('--state', action='store', default=False)

parser.add_argument('--write-database', action='store', default=False)
parser.add_argument('--write-pickle', action='store', default=False)
parser.add_argument('--write-coeff', action='store', default=0)

args = parser.parse_args()

PICKLE_PATH = os.path.abspath("../" + os.path.dirname(__file__)) + "\\Notebook\\Exports\\metaobjects\\npi\\" + args.state

if not (os.path.exists(PICKLE_PATH)):
    os.mkdir(PICKLE_PATH)
    print("The path for Pickle Object Storage has been created")
else:
    print("The path to the Pickle Object Storage Directory for the State = '" + args.state + "' exists")

class FactorAnalysis():
    
    database = None

    def __init__(self, db):
        database = db
        self.spl_weights = {}
        self.spl_dict = {}
        self.state_data = {}
        self.agg_append = dict()
        self.x_append = dict()
        self.spl_grid_weights = dict()
        self.state_drug_data = dict()
        self.drug_agg_append = dict()
        self.drug_dict = {}
        self.category_array = {}
        self.agg_ratio = []
        self.cost_quality_bene = {}
        self.cost_quality_npi_dict = {}
        self.risk_value_dict = {}
        self.net_cost_spl = []
        self.distinct_spl_dict = {}
        self.drugs_all = {}

        self.drug_factor_spl = pd.DataFrame(columns=['specialty_description', 'drug_name', 'factor'])
        self.spl_factor_drugs = pd.DataFrame(columns=['specialty_description', 'drug_name', 'factor'])
        self.drug_weights = pd.DataFrame(columns=['drug_name', 'factor'])

        # Drugs Weights from Health and Drug Influence Grid, calculated using drug types
        self.drug_manual_weights = {
            'HRM': 5,
            'antipsychotic': 4,
            'opioid': 3,
            'antibiotic': 2,
            'generic': 1,
            'other': 1
        }

        # field names for claim count
        self.field_names = {
            'HRM': 'hrm_claim_count_ge65',
            'antipsychotic': 'antipsych_claim_count_ge65',
            'opioid': 'opioid_claim_count',
            'antibiotic': 'antibiotic_claim_count',
            'generic': 'generic_claim_count',
            'other': 'other_claim_count',
            'brand': 'brand_claim_count'
        }

    def __dir__(self):
        return ['spl_weights', 'spl_dict', 'state_data', 'spl_grid_weights', 
            'state_drug_data', 'drug_agg_append', 'net_cost_spl',
            'drug_dict', 'category_array', 'cost_quality_bene', 'cost_quality_npi_dict', 'risk_value_dict']

    def execute_state_data(self):
        info_cursor = database.info_connection.cursor()
        info_cursor.execute(database.state_wise_mean_data_spl())
        self.state_data = info_cursor.fetchall()
        info_cursor.close()

    def execute_spl_dict(self):
        for item in self.state_data:
            self.spl_dict[item[0]] = item[1]

    def execute_grid_weights(self):
        for item in iter(self.state_data):
            self.spl_grid_weights[item[0]] = get_specialty_weight_values(item[0], self.spl_dict[item[0]], database, self)

    def execute_state_wise_drug(self):
        info_cursor = database.info_connection.cursor()    
        info_cursor.execute(database.state_wise_drug())
        self.state_drug_data = info_cursor.fetchall()
        info_cursor.close()

    def execute_drug_dict(self):
        for item in self.state_drug_data:
            self.drug_dict[item[0]] = item[0]

    def execute_drug_factor_spl(self):
        cursor = database.info_connection.cursor()
        cursor.execute(database.cost_quality_table())
        item = cursor.fetchone()
        while(item):
            self.drug_factor_spl = self.drug_factor_spl.append({'specialty_description': item[0], 
                                    'drug_name': item[1], 
                                    'factor': get_drug_factor_spl(item[1], item[0], database, self)}, ignore_index=True)
            item = cursor.fetchone()

    def execute_category_array(self):
        cursor = database.aggregate_connection.cursor()
        cursor.execute(database.find_drug_type())
        categories = cursor.fetchall()
        cursor.close()
        for item in categories:
            self.category_array[item[0]] = item[1]

    def calculate_entropy(self, probability):
        return np.float64(probability * math.log(probability)) + np.float64((1 - probability) * math.log(1 - probability))

    def get_distinct_spl_dict(self):
        cursor = database.info_connection.cursor()
        cursor.execute(database.get_distinct_spl_dict())
        value = cursor.fetchone()
        while(value):
            if value[1] not in self.distinct_spl_dict:
                self.distinct_spl_dict[value[1]] = []
            self.distinct_spl_dict[value[1]].append(value[0])
            value = cursor.fetchone()
        cursor.close()

    # returns average of Claim Count Over Health Risk
    def get_drug_weight(self, drug, drug_type):
        drug_item = self.distinct_spl_dict[drug]
        ratio = []
        for item in drug_item:
            value = npi_drug_state_table_summary(drug, item, drug_type, database)
            ratio.append(value)
        return math.fsum(ratio) / len(ratio)

    def get_agg_ratio(self):
        self.agg_ratio = []
        for item in self.drug_dict.keys():
            if item not in self.category_array:
                drug_type = 'generic'
            else:
                drug_type = self.category_array[item]
            self.agg_ratio.append(self.get_drug_weight(item, drug_type))

    def get_drug_weights(self):
        drug_weights = pd.DataFrame(columns=['drug_name', 'factor'])
        idx = 0
        try:
            for item in self.drug_dict.keys():
                if item not in self.category_array:
                    drug_type = 'generic'
                else:
                    drug_type = self.category_array[item]
                self.drug_weights = self.drug_weights.append({'drug_name': item, 'factor': -1 * np.float64(
                        self.calculate_entropy(1 / ( 1 + self.agg_ratio[idx] )) * 
                        np.float64(math.atanh(self.agg_ratio[idx] / 100)) *
                        self.drug_manual_weights[drug_type])}, ignore_index=True)
                idx = idx + 1
        except Exception as e:
            print(idx, self.agg_ratio[idx])

        print(len(self.drug_weights['factor'].values.tolist()))
        return dict(zip(self.drug_dict.keys(), self.drug_weights['factor'].values.tolist()))

    def execute_cost_quality_npi_dict(self):
        cursor = database.info_connection.cursor()
        cursor.execute(database.cost_quality_npi_count())
        cost_quality = cursor.fetchall()
        for item in cost_quality:
            self.cost_quality_npi_dict[item[0]] = item[1]
        cursor.close()

    def execute_cost_quality_bene_dict(self):
        cursor = database.info_connection.cursor()
        cursor.execute(database.cost_quality_bene_dict())
        cost_quality = cursor.fetchall()
        for item in cost_quality:
            self.cost_quality_bene[item[0].__str__() + "___" + item[1].__str__()] = item[2]
        cursor.close()

    def execute_bene_risk_sql(self):
        presc_cursor = database.presc_connection.cursor()
        presc_cursor.execute(database.bene_risk_sql())
        risk = presc_cursor.fetchall()
        for item in risk:
            self.risk_value_dict[item[0]] = item[1]
        presc_cursor.close()

    def execute_spl_factor_drugs(self):
        idx = 0
        for item in iter(self.drug_factor_spl.values.tolist()):
            value = get_spl_factor_drug(item[0], item[1], self)
            self.spl_factor_drugs = self.spl_factor_drugs.append({'specialty_description': item[0], 'drug_name': item[1], 'factor': value}, 
                                                    ignore_index=True)
            idx = idx + 1

    def get_net_cost_drugs_for_specialties(self):
        cursor = database.info_connection.cursor()
        cursor.execute(database.get_net_cost_drugs_for_specialties())
        data = cursor.fetchone()
        while(data):
            self.net_cost_spl.append(data[0])
            data = cursor.fetchone()
        cursor.close()
        return self.net_cost_spl

# The weight is based on how far the claims have gone and the no. of allowable prescription drugs
# times the Supervised Entry for Specialties based on their Primary Ownership
def get_specialty_weight_values(specialty, npi_count, database, factoranalysis):
    cursor = database.claim_connection.cursor()
    cursor.execute(database.specialty_factor_sql(specialty))
    result = cursor.fetchone()
    weight = result[3]
    factoranalysis.spl_weights[specialty] = weight
    cursor.close()
    return (weight * get_chebychevs_probability(specialty, 'claim', database, factoranalysis) *  (result[5] / npi_count))

def get_aggregated_values(database, factoranalysis):
    idx = 0
    info_cursor = database.info_connection.cursor()
    info_cursor.execute(database.state_wise_spl())
    agg_values = info_cursor.fetchone()
    for key in factoranalysis.spl_dict.keys():
        specialty = key
        factoranalysis.agg_append[specialty] = agg_values
        agg_values = info_cursor.fetchone()
        idx = idx + 1
    info_cursor.close()

# An sgn function
def determine_zero_value(mean, sigma):
    if(mean < sigma and sigma - mean >= mean):
        return mean
    elif(sigma < mean and mean - sigma >= sigma):
        return sigma
    elif(mean > sigma and mean - sigma <= sigma):
        return mean - sigma
    elif(sigma > mean and sigma - mean <= mean):
        return sigma - mean
    return mean

# Returns a Dictionary of numpy arrays
# if else clauses for selecting the non-zero element
def get_all_x_values(specialty, database, factoranlaysis):
    try:
        info_cursor = database.info_connection.cursor()
        info_cursor.execute(database.x_values(specialty))
        x_values = info_cursor.fetchone()
        factoranalysis.x_append[specialty] = np.array([])
        while(x_values):
            mean_claim_count, mean_cost_per_claim, mean_supply_per_claim, std_cost_per_claim, std_supply_per_claim, mean_claim_per_dollar, std_claim_per_dollar = [None] * 7
            specialty_description, drug_name, total_claim_count, total_day_supply, drug_cost = x_values
            if(total_claim_count == 0):
                new_cursor = database.info_connection.cursor()
                new_cursor.execute(database.get_unit_values(drug_name))
                values = new_cursor.fetchall()
                mean_claim_count, mean_cost_per_claim, mean_supply_per_claim, std_cost_per_claim, std_supply_per_claim, mean_claim_per_dollar, std_claim_per_dollar = values[0]
                drug_cost = determine_zero_value(mean_cost_per_claim, std_cost_per_claim)
                total_day_supply = determine_zero_value(mean_supply_per_claim, std_supply_per_claim)
                total_claim_count = 1
                new_cursor.close()
            if(drug_cost == 0):
                new_cursor = database.info_connection.cursor()
                new_cursor.execute(database.get_unit_values(drug_name))
                values = new_cursor.fetchall()
                mean_claim_count, mean_cost_per_claim, mean_supply_per_claim, std_cost_per_claim, std_supply_per_claim, mean_claim_per_dollar, std_claim_per_dollar = values[0]
                total_claim_count = determine_zero_value(mean_claim_per_dollar, std_claim_per_dollar)
                drug_cost = 1
                new_cursor.close()
            np.append(factoranalysis.x_append[specialty], 
            [total_claim_count / drug_cost, 
            total_day_supply / total_claim_count, 
            drug_cost / total_claim_count])
            x_values = info_cursor.fetchone()
        info_cursor.close()
    except Exception as e:
        print("Exception: ", x_values, specialty)
    return factoranalysis.x_append[specialty]

def get_chebychevs_probability(specialty, domain='supply', database=None, factoranalysis=None):
    # from the dataframe
    x_values = get_all_x_values(specialty, database, factoranalysis)
    mu = factoranalysis.agg_append[specialty]
    if(domain == 'supply'):
        std_value = mu[5]
        mu_value = mu[3]
    elif(domain == 'cost'):
        std_value = mu[4]
        mu_value = mu[2]
        mu_value = mu_value[0]
    elif(domain == 'claim'):
        std_value = mu[7]
        mu_value = mu[6]
    
    count = 0
    ratio = 0
    for value in x_values:
        if (abs(value - mu_value) / std_value) > 1:
            ratio = ratio + abs( value - mu_value ) / std_value # Ratio of those which are exceeding
            count = count + 1
        ratio = ratio / count
    
    k = None
    if (ratio >= 2):
        k = floor(ratio)
    
    if (k == None): 
        return 1
    else:
        return (1 - 1/k**2)

def get_drug_reduction(specialty, database):
    cursor = database.connection.cursor()
    cursor.execute(database.drug_factor_vector(specialty))
    vector = cursor.fetchone()
    cursor.close()
    return vector

def get_drug_aggregated_values(database, factoranalysis):
    cursor = database.info_connection.cursor()
    cursor.execute(database.state_wise_mean_data_drug())
    agg_values = cursor.fetchone()
    for key in factoranalysis.drug_dict.keys():
        factoranalysis.drug_agg_append[key] = agg_values
        agg_values = cursor.fetchone()
        cursor.close()

def get_drug_factor_spl(drug, specialty, database, factoranalysis):
    cursor = database.info_connection.cursor()
    cursor.execute(database.drug_prescriptions_sql(drug))
    result = cursor.fetchone()
    mu = factoranalysis.drug_agg_append[drug]
    supply_per_claim = float(mu[3])
    cost_per_claim = float(mu[2])
    claim_per_dollar = float(mu[6])
    opioid_prescriber_count = result[2]
    npi_count = result[3]
    cursor.close()
    vector = get_drug_reduction(specialty, database) # count and reduction
    
    return float(npi_count) * cost_per_claim * supply_per_claim * (
        float(opioid_prescriber_count / npi_count) * claim_per_dollar + 
        float(vector[2]) * float(vector[3]) / float(result[1])
    )

def npi_drug_state_table_summary(drug, specialty, drug_type, database):
    drug_type = str.lower(drug_type)
    cursor = database.presc_connection.cursor()
    query = database.presc_drug_risk(drug_type, specialty)
    cursor.execute(query)
    ratio = cursor.fetchone()
    cursor.close()
    return ratio[0]

def get_spl_factor_drug(spl, drug, factoranalysis):
    try:
        mu = factoranalysis.agg_append[spl]
        supply_per_claim = mu[3]
        bene_count = float(0.000001)
        if (spl + "___" + drug) not in factoranalysis.cost_quality_bene or factoranalysis.cost_quality_bene[spl + "___" + drug] == 0:
            bene_count = float(0.000001)
        else:
            bene_count = float(factoranalysis.cost_quality_bene[spl + "___" + drug])

        drug_weight = factoranalysis.drugs_all[drug]
        
        value = factoranalysis.cost_quality_npi_dict[spl] * supply_per_claim * (float(factoranalysis.spl_dict[spl]) * bene_count + float(drug_weight) * float(factoranalysis.risk_value_dict[spl]))
    except Exception as e:
        print(spl, drug, 
        factoranalysis.cost_quality_npi_dict[spl], supply_per_claim, 
        factoranalysis.spl_dict[spl], bene_count, drug_weight, factoranalysis.risk_value_dict[spl])
    return value

class CorrelationCoefficient():

    coefficients = pd.DataFrame(columns=["Use Case", "Correlation PearsonR", "Kendall TAU"])
    factoranalysis = None

    def __init__(self, fac):
        factoranalysis = fac
        self.drug_weights_values = None
    
    def write_to_df(self, use_case, pearsonr, tau):
        coefficients = coefficients.append({'Use Case': use_case, 
        "Correlation PearsonR": pearsonr, "Kendall TAU": tau})

    def get_coefficients(self):
        return coefficients

    def write_coefficients(self):
        pickle.dump(get_coefficients(), open(PICKLE_PATH + "\\coefficients.pkl", 'bw'))

    def specialty_influence(self):
        # PearsonR and Kendall Tau for Specialty weights and Specialty Factor for Drugs 
        # The Specialty Influence

        print("-- PearsonR and Kendall Tau for Specialty weights and Specialty Factor for Drugs --")
        pa_cqt_vector = {'x': [], 'y': []}
        values = factoranalysis.spl_factor_drugs['factor'].get_values()
        idx = 0
        for item in iter(factoranalysis.spl_factor_drugs.values.tolist()):
            pa_cqt_vector['x'].append(factoranalysis.spl_grid_weights[item[0]])
            pa_cqt_vector['y'].append(values[idx])
            idx = idx + 1

        corr_coeff = scipy.stats.pearsonr(x=pa_cqt_vector['x'], y=pa_cqt_vector['y'])

        print("The PearsonR Correlation Coefficient is found to be: \n" + corr_coeff.__str__())

        kendall_tau = scipy.stats.kendalltau(x=pa_cqt_vector['x'], y=pa_cqt_vector['y'])

        print("The kendall Tau Ranking Correlation Coefficient is found to be: \n" + kendall_tau.__str__())

        return [corr_coeff, kendall_tau]

    def lambda_vector(self):
        # PearsonR and Kendall Tau for Specialty weights and Drug weights 
        # The Lambda vector

        print("-- PearsonR and Kendall Tau for Specialty weights and Drug weights --")
        pa_cqt_vector = {'x': [], 'y': []}
        self.drug_weights_values = dict(zip(factoranalysis.drug_dict.keys(), 
        factoranalysis.drug_weights['factor'].get_values().tolist()))
        idx = 0
        for item in iter(factoranalysis.spl_factor_drugs.values.tolist()):
            pa_cqt_vector['x'].append(factoranalysis.spl_grid_weights[item[0]])
            pa_cqt_vector['y'].append(self.drug_weights_values[item[1]])
            idx = idx + 1

        corr_coeff = scipy.stats.pearsonr(x=pa_cqt_vector['x'], y=pa_cqt_vector['y'])

        print("The PearsonR Correlation Coefficient is found to be: \n" + corr_coeff.__str__())

        kendall_tau = scipy.stats.kendalltau(x=pa_cqt_vector['x'], y=pa_cqt_vector['y'])

        print("The kendall Tau Ranking Correlation Coefficient is found to be: \n" + kendall_tau.__str__())

        return [corr_coeff, kendall_tau]

    def safety_vector(self):
        # PearsonR and Kendall Tau for Factor weights
        # The Safety Vector

        print("-- PearsonR and Kendall Tau for Factor weights, the Safety Factor --")
        pa_cqt_weights = {'x': [], 'y': []}
        drug_factors = factoranalysis.drug_factor_spl['factor'].values.tolist()
        idx = 0
        for item in iter(factoranalysis.drug_factor_spl.values.tolist()):
            pa_cqt_weights['x'].append(factoranalysis.spl_grid_weights[item[0]])
            pa_cqt_weights['y'].append(drug_factors[idx])
            idx = idx + 1

        corr_coeff = scipy.stats.pearsonr(x=pa_cqt_weights['x'], y=pa_cqt_weights['y'])

        print("The PearsonR Correlation Coefficient is found to be: \n" + corr_coeff.__str__())

        kendall_tau = scipy.stats.kendalltau(x=pa_cqt_weights['x'], y=pa_cqt_weights['y'])

        print("The kendall Tau Ranking Correlation Coefficient is found to be: \n" + kendall_tau.__str__())

        return [corr_coeff, kendall_tau]

    def drug_influence(self):
        # PearsonR and Kendall Tau for Factor weights
        # The Drug Influence

        print("-- PearsonR and Kendall Tau for Factor weights, the Drug Influence --")
        pa_cqt_weights = {'x': [], 'y': []}
        drug_factors = factoranalysis.drug_factor_spl['factor'].values.tolist()
        idx = 0
        for item in iter(factoranalysis.drug_factor_spl.values.tolist()):
            pa_cqt_weights['x'].append(drug_factors[idx])
            pa_cqt_weights['y'].append(self.drug_weights_values[item[1]])
            idx = idx + 1

        corr_coeff = scipy.stats.pearsonr(x=pa_cqt_weights['x'], y=pa_cqt_weights['y'])

        print("The PearsonR Correlation Coefficient is found to be: \n" + corr_coeff.__str__())

        kendall_tau = scipy.stats.kendalltau(x=pa_cqt_weights['x'], y=pa_cqt_weights['y'])

        print("The kendall Tau Ranking Correlation Coefficient is found to be: \n" + kendall_tau.__str__())

        return [corr_coeff, kendall_tau]

    def z_vector(self):
        # PearsonR and Kendall Tau for Factor weights
        # The z vector

        print("-- PearsonR and Kendall Tau for Factor weights, the z vector --")
        pa_cqt_weights = {'x': [], 'y': []}
        drug_factors = factoranalysis.drug_factor_spl['factor'].values.tolist()
        values = factoranalysis.spl_factor_drugs['factor'].get_values()
        idx = 0
        for item in iter(factoranalysis.drug_factor_spl.values.tolist()):
            pa_cqt_weights['x'].append(drug_factors[idx])
            pa_cqt_weights['y'].append(values[idx])
            idx = idx + 1

        corr_coeff = scipy.stats.pearsonr(x=pa_cqt_weights['x'], y=pa_cqt_weights['y'])

        print("The PearsonR Correlation Coefficient is found to be: \n" + corr_coeff.__str__())

        kendall_tau = scipy.stats.kendalltau(x=pa_cqt_weights['x'], y=pa_cqt_weights['y'])

        print("The kendall Tau Ranking Correlation Coefficient is found to be: \n" + kendall_tau.__str__())

        return [corr_coeff, kendall_tau]

    def health_vector(self):
        # PearsonR and Kendall Tau for Factor weights
        # The Health vector

        print("-- PearsonR and Kendall Tau for Factor weights, the Health vector --")
        pa_cqt_weights = {'x': [], 'y': []}
        values = factoranalysis.spl_factor_drugs['factor'].get_values()
        idx = 0
        for item in iter(factoranalysis.drug_factor_spl.values.tolist()):
            pa_cqt_weights['x'].append(self.drug_weights_values[item[1]])
            pa_cqt_weights['y'].append(values[idx])
            idx = idx + 1

        corr_coeff = scipy.stats.pearsonr(x=pa_cqt_weights['x'], y=pa_cqt_weights['y'])

        print("The PearsonR Correlation Coefficient is found to be: \n" + corr_coeff.__str__())

        kendall_tau = scipy.stats.kendalltau(x=pa_cqt_weights['x'], y=pa_cqt_weights['y'])

        print("The kendall Tau Ranking Correlation Coefficient is found to be: \n" + kendall_tau.__str__())

        return [corr_coeff, kendall_tau]

class DatabaseWrites():

    database = None
    factoranalysis = None
    corr = None
    
    def __init__(self, db, fac, corr):
        database = db
        factoranalysis = fac
        corr = corr
    
    @staticmethod
    def insert_factor_vectors(factoranalysis, corr):
        cursor = database.presc_connection.cursor()
        spl_factor_drugs_dict = factoranalysis.spl_factor_drugs.to_dict()
        drug_factor_spl_dict = factoranalysis.drug_factor_spl.to_dict()
        idx = 0
        try:
            database.presc_connection.begin()
            for item in iter(factoranalysis.spl_factor_drugs.values.tolist()):
                cursor.execute(database.insert_factor_vactors(item[0], item[1], 
                factoranalysis.spl_grid_weights[item[0]], corr.drug_weights_values[item[1]], 
                drug_factor_spl_dict['factor'][idx], spl_factor_drugs_dict['factor'][idx]))
                cursor = database.presc_connection.cursor()
                idx = idx + 1
                if idx % 100:
                    database.presc_connection.commit()
        except Exception as e:
            database.presc_connection.rollback()
            raise e
        cursor.close()

class PickleWrites():
    
    @staticmethod
    def write_pickle_objects(factoranalysis):
        attributes = dir(factoranalysis)
        for attribute in attributes:
            if not os.path.exists(PICKLE_PATH + "\\" + attribute + ".pkl"):
                pickle.dump(factoranalysis.__getattribute__(attribute), open(PICKLE_PATH + "\\" + attribute + ".pkl", 'bw'))
            else:
                print("Path exists: " + PICKLE_PATH + "\\" + attribute + ".pkl")
    

if __name__ == "__main__":

    print("\nEnter 'NA' for Not Applicable")
    
    if not args.state:
        STATE = input("\nEnter the State: ")
    else:
        STATE = args.state
    
    if not WRITE_DATABASE:
        WRITE_DATABASE = input("\nShould a database entry be written, enter 1 or 0: ")

    if not WRITE_PICKLE:
        WRITE_PICKLE = input("\nShould the variables be serialized for Reporting, enter 1 or 0: ")

    print("\n Factor Analysis algorithm running for State = '" + STATE + "'")
    
    partition = Partition(STATE)
    database = Database(partition)

    factoranalysis = FactorAnalysis(database)

    factoranalysis.execute_state_data()
    factoranalysis.execute_spl_dict()
    get_aggregated_values(database, factoranalysis)

    factoranalysis.execute_grid_weights()
    factoranalysis.execute_state_wise_drug()
    factoranalysis.execute_drug_dict()
    get_drug_aggregated_values(database, factoranalysis)

    factoranalysis.execute_drug_factor_spl()
    factoranalysis.execute_category_array()
    factoranalysis.get_distinct_spl_dict()
    factoranalysis.get_agg_ratio()
    factoranalysis.get_drug_weights()
    factoranalysis.drugs_all = dict(zip(factoranalysis.drug_dict.keys(), factoranalysis.drug_weights['factor'].values.tolist()))
    factoranalysis.execute_cost_quality_npi_dict()
    factoranalysis.execute_cost_quality_bene_dict()
    factoranalysis.execute_bene_risk_sql()
    factoranalysis.execute_spl_factor_drugs()

    if args.tau or args.pearsonr:
        data = {}
        corr = CorrelationCoefficient(factoranalysis)
        data['Specialty Influence'] = corr.specialty_influence()
        data['Lambda vector'] = corr.lambda_vector()
        data['Health vector'] = corr.health_vector()
        data['Drug Influence'] = corr.drug_influence()
        data['Safety vector'] = corr.safety_vector()
        data['Z vector'] = corr.z_vector()
    
    if args.tau or args.pearsonr:
        TAU = input("\nEnter the minimum bound of Kendall's TAU coefficient, to be recorded: Say -0.001: ")
        PEARSONR = input("\nEnter the minimum bound of PearsonR coefficient, to be recorded, Say -0.001: ")
    
    if args.write_coeff == 1:
        WRITE_COEFF = input("\nProcedd with Writing ? Enter 'Y' or 'NA': ")
        if WRITE_COEFF == 'Y' and len(data.keys()) > 0:
            for key, item in data:
                if item[0] >= int(TAU) and item[1] >= int(PEARSONR):
                    CorrelationCoefficient().write_to_df(key, item[0], item[1])
            CorrelationCoefficient().write_coefficients()

    if args.write_pickle:
        PickleWrites().write_pickle_objects(factoranalysis)

        print("\nPickle Objects have been created\n")

    if args.write_database:
        DatabaseWrites(database, factoranalysis, corr).insert_factor_vectors(factoranalysis, corr)

        print("\nDatabase has been written\n")
