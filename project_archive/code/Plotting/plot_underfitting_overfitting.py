"""
============================
Underfitting vs. Overfitting
============================

This example demonstrates the problems of underfitting and overfitting and
how we can use linear regression with polynomial features to approximate
nonlinear functions. The plot shows the function that we want to approximate,
which is a part of the cosine function. In addition, the samples from the
real function and the approximations of different models are displayed. The
models have polynomial features of different degrees. We can see that a
linear function (polynomial with degree 1) is not sufficient to fit the
training samples. This is called **underfitting**. A polynomial of degree 4
approximates the true function almost perfectly. However, for higher degrees
the model will **overfit** the training data, i.e. it learns the noise of the
training data.
We evaluate quantitatively **overfitting** / **underfitting** by using
cross-validation. We calculate the mean squared error (MSE) on the validation
set, the higher, the less likely the model generalizes correctly from the
training data.
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression, SGDRegressor
from sklearn.model_selection import cross_val_score

n_samples = 30

def f_specialty_weight(X_test):
    
    coef = [ -3.00278049e+08,  -3.69372357e+12,  -1.22213532e+11,  -5.51955225e+10,
    7.08110967e+13,  -1.02927087e+14,  -5.63431472e+14,   5.47323315e+13,
    -7.54333864e+11]
    values = []
    for item in X_test:
        values.append(-6.03038598e+10 + np.dot(coef, item)) # Intercept term
    return values

# X_test is an index based array

def plot_function(X, y, X_test, y_true):
    plt.figure(figsize=(14, 5))
    for i in range(len(X_test)):
        ax = plt.subplot(1, len(X_test), i + 1)
        plt.setp(ax, xticks=(), yticks=())

        polynomial_features = SGDRegressor()

        pipeline = Pipeline([("sgd_regressor", polynomial_features)])
        pipeline.fit(X, y)

        # Evaluate the models using crossvalidation
        scores = cross_val_score(pipeline, X, y,
                                scoring="neg_mean_squared_error", cv=10)

        plt.plot(X_test[i], pipeline.predict(X_test[i]), label="Model")
        plt.xlabel("x")
        plt.ylabel("y")
        plt.legend(loc="best")
        plt.title("MSE = {:.2e}(+/- {:.2e})".format(
            -scores.mean(), scores.std()))
    plt.show()
