import pymysql

class Partition():
    
    def __init__(self, state):
        self.state = state

class Database(object):
    
    def __init__(self, partition):
        self.partition = partition
        self.connection = pymysql.connect(user='root', password='root',
                              host='localhost',
                              db='drug_database')

        self.info_connection = pymysql.connect(user='root', password='root',
                                        host='localhost',
                                        db='drug_information')

        self.presc_connection = pymysql.connect(user='root', password='root',
                                        host='localhost',
                                        db='prescription_aggregates')

        self.aggregate_connection = pymysql.connect(user='root', password='root',
                                        host='localhost',
                                        db='drug_aggregates')

        self.claim_connection = pymysql.connect(user='root', password='root',
                                        host='localhost',
                                        db='claim_aggregates')

        self.opt_connection = pymysql.connect(user='root', password='root',
            host='localhost',
            db='optimization_tables')

    def get_distinct_spl_dict(self):
        return """
            SELECT specialty_description, drug_name FROM """ + self.partition.state + """_cost_quality_table GROUP BY drug_name;
            """
    
    def specialty_factor_sql(self, specialty):
        return ("""
            SELECT * FROM """ + self.partition.state + """_specialty_entropy
            WHERE specialty_description = '{0}'; """).format(specialty)

    def state_wise_mean_data_spl(self):
        return """
            SELECT specialty_description, 
            COUNT(DISTINCT npi) as npi_count FROM """ + self.partition.state + """_state_spl_drug_npi
            GROUP BY specialty_description; """

    def state_wise_spl(self):
        return """
        SELECT specialty_description, mean_claim_count, mean_cost_per_claim,
            mean_supply_per_claim, std_cost_per_claim, std_supply_per_claim, 
            mean_claim_per_dollar, std_claim_per_dollar
        FROM """ + self.partition.state + """_state_wise_specialty;
        """

    def x_values(self, specialty):
        return ("""
            SELECT specialty_description, drug_name, total_claim_count, total_day_supply, drug_cost
            FROM """ + self.partition.state + """_state_spl_drug_npi 
            WHERE specialty_description = '{0}'""").format(specialty)

    def get_unit_values(self, drug):
        return (""" 
            SELECT mean_claim_count, mean_cost_per_claim, mean_supply_per_claim, 
            std_cost_per_claim, std_supply_per_claim, mean_claim_per_dollar, 
            std_claim_per_dollar FROM """ + self.partition.state + """_state_wise_drug
            WHERE drug_name = '{0}'; """).format(drug)

    def drug_factor_vector(self, specialty):
        return ("""
            SELECT * FROM """ + self.partition.state + """_drug_factor_vector 
            WHERE specialty_description = '{0}';
            """).format(specialty)

    def state_wise_mean_data_drug(self):
        return """
            SELECT drug_name, mean_claim_count, mean_cost_per_claim, mean_supply_per_claim, 
            std_cost_per_claim, std_supply_per_claim, mean_claim_per_dollar, 
            std_claim_per_dollar FROM """ + self.partition.state + """_state_wise_drug; """

    def drug_prescriptions_sql(self, drug):
        return ("""
            SELECT drug_name, claim_count, opioid_prescriber_count, npi_count, day_supply FROM 
            """ + self.partition.state + """_drug_prescriptions
            WHERE drug_name = '{0}'; """).format(drug)

    def presc_drug_risk(self, drug_type, specialty):
        return """
            SELECT IF(AVG(`""" + drug_type + """`) = 0, 1.01, 
            AVG(`""" + drug_type + """`)) FROM """ + self.partition.state + """_pres_drug_risk
            GROUP BY specialty_description
            HAVING specialty_description = '""" + specialty + """'"""
    
    def cost_quality_table(self):
        return """
            SELECT specialty_description, drug_name 
            FROM """ + self.partition.state + """_cost_quality_table GROUP BY specialty_description, drug_name;
            """

    def state_wise_drug(self):
        return """
            SELECT DISTINCT(drug_name) FROM """ + self.partition.state + """_cost_quality_table;
            """

    @staticmethod
    def find_drug_type():
        return """
            SELECT drug_name, category
            FROM drug_categories
            """

    def cost_quality_npi_count(self):
        return """
            SELECT specialty_description, COUNT(DISTINCT drug_name) as drug_count
            FROM """ + self.partition.state + """_cost_quality_table GROUP BY specialty_description;
            """

    def cost_quality_bene_dict(self):
        return """
            SELECT specialty_description, drug_name, AVG(total_bene_count / total_claim_count) as average
            FROM """ + self.partition.state + """_cost_quality_table
            GROUP BY specialty_description, drug_name
            HAVING average != 0;
            """

    def bene_risk_sql(self):
        return """
            SELECT specialty_description, 
            IF(AVG(beneficiary_average_risk_score) = 0, 0.01, AVG(beneficiary_average_risk_score)) as risk
            FROM table_summary
            WHERE nppes_provider_state = '""" + str.upper(self.partition.state) + """'
            GROUP BY specialty_description;
            """

    def insert_factor_vactors(self, specialty, drug_name, spl_weight, drug_weight, drug_factor_spl, spl_factor_drug):
        return ("""
            INSERT INTO """ + self.partition.state + """_factors VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')
            """).format(str.upper(self.partition.state), specialty, drug_name, spl_weight, drug_weight, drug_factor_spl, spl_factor_drug)

    def get_factors_from_all(self):
        return """
            SELECT """ + self.partition.state + """_state_spl_drug_npi.specialty_description, """ + self.partition.state + """_state_spl_drug_npi.drug_name, 
            """ + self.partition.state + """_state_spl_drug_npi.npi, 
            """ + self.partition.state + """_state_spl_drug_npi.total_claim_count/ """ + self.partition.state + """_state_spl_drug_npi.drug_cost as claim_per_dollar, 
            """ + self.partition.state + """_state_spl_drug_npi.total_day_supply/ """ + self.partition.state + """_state_spl_drug_npi.total_claim_count as supply_per_claim, 
            """ + self.partition.state + """_state_spl_drug_npi.drug_cost/ """ + self.partition.state + """_state_spl_drug_npi.total_claim_count as cost_per_claim, 
            """ + self.partition.state + """_factors.specialty_weight, """ + self.partition.state + """_factors.drug_factor_specialty,
            """ + self.partition.state + """_factors.drug_weight, """ + self.partition.state + """_factors.specialty_factor_drug,
            """ + self.partition.state + """_state_wise_specialty.mean_cost_per_claim, """ + self.partition.state + """_state_wise_specialty.mean_supply_per_claim,
            """ + self.partition.state + """_state_wise_specialty.mean_claim_per_dollar, """ + self.partition.state + """_state_wise_specialty.std_cost_per_claim,
            """ + self.partition.state + """_state_wise_specialty.std_supply_per_claim, """ + self.partition.state + """_state_wise_specialty.std_claim_per_dollar,
            """ + self.partition.state + """_state_wise_drug.mean_cost_per_claim, """ + self.partition.state + """_state_wise_drug.mean_supply_per_claim,
            """ + self.partition.state + """_state_wise_drug.mean_claim_per_dollar, """ + self.partition.state + """_state_wise_drug.std_cost_per_claim, 
            """ + self.partition.state + """_state_wise_drug.std_supply_per_claim, """ + self.partition.state + """_state_wise_drug.std_claim_per_dollar
            FROM """ + self.partition.state + """_factors
            INNER JOIN 
            drug_information.pa """ + self.partition.state + """_state_spl_drug_npi AS """ + self.partition.state + """_state_spl_drug_npi
            ON """ + self.partition.state + """_state_spl_drug_npi.specialty_description = """ + self.partition.state + """_factors.specialty_description
            AND """ + self.partition.state + """_state_spl_drug_npi.drug_name = """ + self.partition.state + """_factors.drug_name
            INNER JOIN drug_information. """ + self.partition.state + """_state_wise_specialty ON 
            """ + self.partition.state + """_state_wise_specialty.specialty_description = """ + self.partition.state + """_factors.specialty_description
            INNER JOIN drug_information. """ + self.partition.state + """_state_wise_drug ON
            """ + self.partition.state + """_state_wise_drug.drug_name = """ + self.partition.state + """_factors.drug_name; """

    def get_cost_function(self):
        return """
            SELECT """ + self.partition.state + """_cost_function.specialty_description, drug_name, npi, 
            claim_per_dollar, supply_per_claim, cost_per_claim, 
            specialty_weight, drug_factor_specialty, drug_weight, specialty_factor_drug, 
            specialty_mean_cost_per_claim, specialty_mean_supply_per_claim, specialty_mean_claim_per_dollar, 
            specialty_std_cost_per_claim, specialty_std_supply_per_claim, specialty_std_claim_per_dollar, 
            drug_mean_cost_per_claim, drug_mean_supply_per_claim, drug_mean_claim_per_dollar, drug_std_cost_per_claim, 
            drug_std_supply_per_claim, drug_std_claim_per_dollar, bene_count, npi_bene_count, med_bene_count,
            npi_claim_count, npi_drug_cost, npi_day_supply,  """ + self.partition.state + """_specialty_entropy.npi_count, 
            prescribers, bene_risk, medicare_prvdr_enroll_status FROM  """ + self.partition.state + """_cost_function
            INNER JOIN claim_aggregates. """ + self.partition.state + """_specialty_entropy
            ON  """ + self.partition.state + """_specialty_entropy.specialty_description = 
             """ + self.partition.state + """_cost_function.specialty_description
            """

    @staticmethod
    def get_states_integration():
        return """
            SELECT * FROM states_integration
            """
    
    # Reporting Function
    def get_net_cost_drugs_for_specialties(self):
        return """
            SELECT SUM(EXP(total_drug_cost / POW(10,8)))
            FROM """ + self.partition.state + """_cost_quality_table
            GROUP BY specialty_description;
            """

    def get_safety_z_cost_function_minimised(self, start, limit):
        return """
            SELECT claim_per_dollar, supply_per_claim, cost_per_claim
            FROM """ + self.partition.state + """_cost_function
            ORDER BY 
            claim_per_dollar, supply_per_claim
            LIMIT {0},{1}
            """.format(start, limit)

    def get_safety_z_cost_function_maximised(self, start, limit):
        return """
            SELECT claim_per_dollar, supply_per_claim,
            CAST(specialty_std_claim_per_dollar / specialty_mean_claim_per_dollar AS DECIMAL(18,15)), 
            CAST(specialty_std_supply_per_claim / specialty_mean_supply_per_claim AS DECIMAL(18,15)),
            """ + self.partition.state + """_specialty_entropy.npi_count,
            npi_drug_cost, npi_day_supply, """ + self.partition.state + """_specialty_entropy.drug_count,
            pa_cost_function.specialty_weight
            FROM """ + self.partition.state + """_cost_function
            INNER JOIN claim_aggregates.""" + self.partition.state + """_specialty_entropy ON 
            """ + self.partition.state + """_specialty_entropy.specialty_description = 
            """ + self.partition.state + """_cost_function.specialty_description
            ORDER BY 
            claim_per_dollar, supply_per_claim
            LIMIT {0},{1}
            """.format(start, limit)

    def get_safety_z_cost_function_optimized(self, start, limit):
        return """
            SELECT claim_per_dollar, supply_per_claim,
            CAST(specialty_std_claim_per_dollar / specialty_mean_claim_per_dollar AS DECIMAL(18,15)), 
            CAST(specialty_std_supply_per_claim / specialty_mean_supply_per_claim AS DECIMAL(18,15)),
            """ + self.partition.state + """_specialty_entropy.npi_count,
            npi_drug_cost, npi_day_supply, """ + self.partition.state + """_specialty_entropy.drug_count,
            pa_cost_function.drug_weight
            FROM """ + self.partition.state + """_cost_function
            INNER JOIN claim_aggregates.""" + self.partition.state + """_specialty_entropy ON 
            """ + self.partition.state + """_specialty_entropy.specialty_description = 
            """ + self.partition.state + """_cost_function.specialty_description
            ORDER BY 
            claim_per_dollar, supply_per_claim
            LIMIT {0},{1}
            """.format(start, limit)

    def get_safety_z_cost_function_trawling(self, start, limit):
        return """
            SELECT claim_per_dollar, supply_per_claim,
            CAST(specialty_std_claim_per_dollar / specialty_mean_claim_per_dollar AS DECIMAL(18,15)), 
            CAST(specialty_std_supply_per_claim / specialty_mean_supply_per_claim AS DECIMAL(18,15)),
            """ + self.partition.state + """_specialty_entropy.npi_count,
            npi_drug_cost, npi_day_supply, """ + self.partition.state + """_specialty_entropy.drug_count
            FROM """ + self.partition.state + """_cost_function
            INNER JOIN claim_aggregates.""" + self.partition.state + """_specialty_entropy ON 
            """ + self.partition.state + """_specialty_entropy.specialty_description = 
            """ + self.partition.state + """_cost_function.specialty_description
            ORDER BY 
            claim_per_dollar, supply_per_claim
            LIMIT {0},{1}
            """.format(start, limit)

    def get_safety_z_cost_function(self, start, limit):
        return """
            SELECT claim_per_dollar, supply_per_claim,
            CAST(specialty_std_claim_per_dollar / specialty_mean_claim_per_dollar AS DECIMAL(18,15)), 
            CAST(specialty_std_supply_per_claim / specialty_mean_supply_per_claim AS DECIMAL(18,15)),
            """ + self.partition.state + """_specialty_entropy.npi_count,
            npi_drug_cost, npi_day_supply, """ + self.partition.state + """_specialty_entropy.drug_count,
            specialty_integration.weight
            FROM """ + self.partition.state + """_cost_function
            INNER JOIN claim_aggregates.""" + self.partition.state + """_specialty_entropy ON 
            """ + self.partition.state + """_specialty_entropy.specialty_description = 
            """ + self.partition.state + """_cost_function.specialty_description
            INNER JOIN backup_table_metadata.specialty_integration
            ON specialty_integration.specialty = """ + self.partition.state + """_specialty_entropy.specialty_description
            ORDER BY 
            claim_per_dollar, supply_per_claim
            LIMIT {0},{1}
            """.format(start, limit)

    # Conjugate Gradient
    def get_safety_z_cost_function_conjugate_gradient(self, start, limit):
        return """
            SELECT claim_per_dollar, supply_per_claim,
            CAST(specialty_std_claim_per_dollar / specialty_mean_claim_per_dollar AS DECIMAL(18,15)), 
            CAST(specialty_std_supply_per_claim / specialty_mean_supply_per_claim AS DECIMAL(18,15)),
            """ + self.partition.state + """_specialty_entropy.npi_count,
            npi_drug_cost, npi_day_supply, """ + self.partition.state + """_specialty_entropy.drug_count
            FROM """ + self.partition.state + """_cost_function
            INNER JOIN claim_aggregates.""" + self.partition.state + """_specialty_entropy ON 
            """ + self.partition.state + """_specialty_entropy.specialty_description = 
            """ + self.partition.state + """_cost_function.specialty_description
            ORDER BY 
            """ + self.partition.state + """_specialty_entropy.npi_count,
            """ + self.partition.state + """_specialty_entropy.drug_count,
            """ + self.partition.state + """_cost_function.npi_day_supply
            LIMIT {0},{1}
            """.format(start, limit)

    def get_spl_weight_cost_function(self, start, limit):
        return """
            SELECT specialty_weight FROM """ + self.partition.state + """_cost_function
            ORDER BY 
            claim_per_dollar, supply_per_claim
            LIMIT {0},{1}
            """.format(start, limit)

    def get_specialty_cost_function(self, start, limit):
        return """
            SELECT specialty_description FROM """ + self.partition.state + """_cost_function
            ORDER BY 
            claim_per_dollar, supply_per_claim
            LIMIT {0},{1}
            """.format(start, limit)

    # Conjugate Gradient
    def get_health_z_cost_function(self, start, limit):
        return """
            SELECT cost_per_claim, supply_per_claim,
            CAST(drug_std_cost_per_claim / drug_mean_cost_per_claim AS DECIMAL(18,15)), 
            CAST(drug_std_supply_per_claim / drug_mean_supply_per_claim AS DECIMAL(18,15)),
            swd.prescribers, swd.part_d_claims, pct.total_day_supply,
            IF(drug_state_category = 'opioid', 2, IF(drug_state_category = 'antibiotic', 3, 
            IF(drug_state_category = 'antipsychotic', 4, IF(drug_state_category = 'HRM', 5, 1)))) as risk_weight
            FROM """ + self.partition.state + """_cost_function
            INNER JOIN claim_aggregates.""" + self.partition.state + """_specialty_entropy ON 
            """ + self.partition.state + """_specialty_entropy.specialty_description = 
            """ + self.partition.state + """_cost_function.specialty_description
            FROM """ + self.partition.state + """_cost_function
            INNER JOIN drug_aggregates.state_wise_drug_state swd
            ON swd.drug_name = """ + self.partition.state + """_cost_function.drug_name
            INNER JOIN drug_information.pa_cost_quality_table pct
            ON pct.specialty_description = """ + self.partition.state + """_cost_function.specialty_description
            AND pct.drug_name = """ + self.partition.state + """_cost_function.drug_name
            ORDER BY 
            swd.prescribers,
            swd.part_d_claims,
            pct.total_day_supply
            LIMIT {0},{1}
            """.format(start, limit)

    def get_drug_weight_cost_function(self, start, limit):
        return """
            SELECT drug_weight FROM """ + self.partition.state + """_cost_function
            ORDER BY 
            cost_per_claim, supply_per_claim
            LIMIT {0},{1}
            """.format(start, limit)

    def get_drug_cost_function(self, start, limit):
        return """
            SELECT drug_name FROM """ + self.partition.state + """_cost_function
            ORDER BY 
            cost_per_claim, supply_per_claim
            LIMIT {0},{1}
            """.format(start, limit)

    # AdaBoostRegressor
    def get_health_lambda_cost_function(self, start, limit):
        return """
            SELECT claim_per_dollar, supply_per_claim, drug_mean_claim_per_dollar, 
            drug_mean_supply_per_claim, drug_std_claim_per_dollar,
            drug_std_supply_per_claim, drug_weight, specialty_weight, bene_risk, 
            IF(medicare_prvdr_enroll_status = 'E', 1, 0) as enroll_status, npi_claim_count,
            """ + self.partition.state + """_specialty_entropy.median_count,
            IF(drug_state_category = 'opioid', pa_cost_function.opioid, 
            IF(drug_state_category = 'antibiotic', pa_cost_function.antibiotic, 
            IF(drug_state_category = 'antipsychotic', pa_cost_function.antipsychotic, 
            IF(drug_state_category = 'HRM', pa_cost_function.hrm, 
            pa_cost_function.generic)))) as health_risk_ratio
            FROM """ + self.partition.state + """_cost_function
            INNER JOIN """ + self.partition.state + """_specialty_entropy
            ON """ + self.partition.state + """_specialty_entropy.specialty_description = """ + self.partition.state + """_cost_function.specialty_description
            ORDER BY 
            supply_per_claim, (drug_mean_supply_per_claim / drug_std_supply_per_claim),
            (IF(drug_state_category = 'opioid', pa_cost_function.opioid, 
            IF(drug_state_category = 'antibiotic', pa_cost_function.antibiotic, 
            IF(drug_state_category = 'antipsychotic', pa_cost_function.antipsychotic, 
            IF(drug_state_category = 'HRM', pa_cost_function.hrm, 
            pa_cost_function.generic)))))
            LIMIT {0},{1}
            """.format(start, limit)

    def get_specialty_factor_drug_cost_function(self, start, limit):
        return """
            SELECT specialty_factor_drug FROM """ + self.partition.state + """_cost_function
            ORDER BY 
            supply_per_claim, (drug_mean_supply_per_claim / drug_std_supply_per_claim),
            (IF(drug_state_category = 'opioid', pa_cost_function.opioid, 
            IF(drug_state_category = 'antibiotic', pa_cost_function.antibiotic, 
            IF(drug_state_category = 'antipsychotic', pa_cost_function.antipsychotic, 
            IF(drug_state_category = 'HRM', pa_cost_function.hrm, 
            pa_cost_function.generic)))))
            LIMIT {0},{1}
            """.format(start, limit)

    # AdaboostRegressor
    def get_safety_lambda_cost_function(self, start, limit):
        return """
            SELECT cost_per_claim, supply_per_claim, 
            specialty_mean_cost_per_claim, specialty_mean_supply_per_claim, 
            specialty_std_cost_per_claim,
            specialty_std_supply_per_claim, drug_weight, specialty_weight, bene_risk,
            IF(drug_state_category = 'opioid', 2, IF(drug_state_category = 'antibiotic', 3, 
            IF(drug_state_category = 'antipsychotic', 4, IF(drug_state_category = 'HRM', 5, 1)))) as risk_weight, 
            agg_cost_part_d, med_bene_count
            FROM """ + self.partition.state + """_cost_function
            ORDER BY 
            supply_per_claim, 
            (specialty_std_claim_per_dollar/specialty_mean_claim_per_dollar),
            bene_risk
            LIMIT {0},{1}
            """.format(start, limit)

    def get_drug_factor_specialty_cost_function(self, start, limit):
        return """
            SELECT drug_factor_specialty FROM """ + self.partition.state + """_cost_function
            ORDER BY 
            supply_per_claim, 
            (specialty_std_claim_per_dollar/specialty_mean_claim_per_dollar),
            bene_risk
            LIMIT {0},{1}
            """.format(start, limit)

    