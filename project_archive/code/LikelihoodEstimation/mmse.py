import numpy as np
import pymysql

class Statistical():

    domain = object()
    rule = object()
    aggregated = object()
    length = None

    def __init__(self, x, x_cap, x_mean):
        self.domain.x = x
        self.rule.x_cap = x_cap
        self.aggregated.x_mean = x_mean
        self.length = len(x)

    def get_domain_mse(self):
        collection = []
        for idx in range(1,self.length):
            collection.append((self.rule.x_cap - self.domain.x)^2)
        return np.mean(collection)

    def get_domain_aggregated(self):
        return self.aggregated.x_mean

    
class BayesianInference():

    domain = object()
    rule = object()
    aggregated = object()

    def __init__(self, x, x_cap, x_mean, y, y_mean):
        self.domain.x = x
        self.rule.x_cap = x_cap
        self.aggregated.x_mean = x_mean
        self.domain.y = y
        self.domain.y_mean = y_mean

    def get_rule_mmse_func(self):
        stacked_array = np.vstack((self.domain.x, self.domain.y))
        covariance = np.cov(stacked_array)
        cov_xy = covariance[0][1]
        cov_y = covariance[1][1]
        return self.aggregated.x_mean + cov_xy * 1/cov_y * (self.aggregated.y - self.aggregated.y_mean)

    def get_rule_mmse(self, mmse):
        collection = []
        for item in mmse:
            collection.append((mmse - self.domain.x) ^ 2)
        return np.mean(collection)