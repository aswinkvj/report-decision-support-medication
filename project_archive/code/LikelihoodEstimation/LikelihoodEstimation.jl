using MySQL
using PyCall
using Logging
loggerFile = Logger("loggerFile")
@pyimport pickle
@pyimport sklearn.preprocessing as preprocessing
@pyimport csv
file_object = open("logfile.log", "a")
Logging.configure(loggerFile, output=file_object, level=DEBUG)
using DataFrames

# Calculating the likelihood value for Safety, CD and SC

presc_connection = mysql_connect("localhost", "root", "root", "prescription_aggregates")
info_connection = mysql_connect("localhost", "root", "root", "drug_information")
opt_connection = mysql_connect("localhost", "root", "root", "optimization_tables")
aggregates_connection = mysql_connect("localhost", "root", "root", "drug_aggregates")

split_criteria = Dict("npi" => 200, "specialty" => 1, "drug" => 10) # whichever is the split with largest

function likelihood_function(func_value::Float64, determinant, n)
    1 / ((2 * pi)^(n/2) * determinant^0.5) * exp(func_value)
end

EXECUTE_HEALTH_LIKELIHOOD = False
EXECUTE_SAFETY_LIKELIHOOD = False
idx = "Not Assigned"
for (idx, item) in ARGS

    if idx == "safety"
        EXECUTE_SAFETY_LIKELIHOOD = True
    elseif idx == "health"
        EXECUTE_HEALTH_LIKELIHOOD = True
    end

end

function execute_safety_likelihood(X::DataFrames.DataFrame, determinant_supplied::Float64)
    total_size = size(X)[1]
    
    values = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", 
        "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", 
        "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", 
        "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", 
        "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", 
        "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", 
        "97", "98", "99", "100", "101", "102", "103", "104", "105", "106", "107", "108", "109", "110", 
        "111", "112", "113", "114", "115", "116", "117", "118", "119", "120", "121", "122", "123", "124", 
        "125", "126", "127", "128", "129", "130", "131", "132", "133", "134", "135", "136", "137", "138", 
        "139", "140", "141", "142", "143", "144", "145", "146", "147", "148", "149", "150", "151", "152", 
        "153", "154", "155", "156", "157", "158", "159", "160", "161", "162", "163", "164", "165", "166", 
        "167", "168", "169", "170", "171", "172", "173", "174", "175", "176", "177", "178", "179", "180", 
        "181", "182", "183", "184", "185", "186", "187", 
        "188", "189", "190", "191", "192", "193", "194", "195", "196", "197", "198", "199", "200"]

    # splitting by npi
    n = split_criteria["npi"]
    λ = Matrix{Complex}(n, 2) # R(n x k)
    ψ = Matrix{Float64}(n, n) # R(n x n)
    psi = Array{Float64, 1}(n)

    # splitting criteria taken for estimation
    splitting_factor = ceil(total_size / n)
    for item = 1:n
        m = Int32(splitting_factor)
        range_1 = Int32(1 + m *(item - 1)) # range_1 = 1246935
        range_2 = Int32(m * item) # range_2 = 1253038
        if total_size <= range_2
            range_2 = total_size
            m = total_size - range_1 + 1
        end
        # calculate the λ vector and X values 
        # Assuming the likelihood function has already been formed from the Medicare Claims data
        lambda = transpose(convert(Array, 
            X[range_1:range_2, 
                    [:claim_per_dollar, :supply_per_claim]]) - 
            convert(Array, X[range_1:range_2, 
            [:specialty_mean_claim_per_dollar, :specialty_mean_supply_per_claim]])) * 
            preprocessing.minmax_scale(convert(Array, 
            X[range_1:range_2, 
                [:specialty_weight, :drug_factor_specialty]]
            ), feature_range=(0,1)) # R(2 x 2), equated lambda with x values

        λ[item, 1] = log(complex(lambda[1, 1], lambda[2, 1]) / sqrt(lambda[1, 1] ^ 2 + lambda[2, 1] ^ 2)) # Equivalent to spl factor drug claim
        λ[item, 2] = log(complex(lambda[1, 2], lambda[2, 2]) / sqrt(lambda[1, 2] ^ 2 + lambda[2, 2] ^ 2)) # Equivalent to spl factor drug supply

        # covariance ψ
        psi_cov = cov(convert(Array, X[range_1:range_2, 
            [:specialty_mean_claim_per_dollar, 
            :specialty_mean_supply_per_claim]]) + preprocessing.minmax_scale(convert(Array, 
            X[range_1:range_2, 
                [:specialty_weight, :drug_factor_specialty]]
            ), feature_range=(0,1)) * transpose(lambda)) # Initial dataset to understand the covariance matrix
        
        psi[item, 1] = psi_cov[2, 1]

        func_value = (-0.5 * 
        (convert(Array, 
            X[range_1:range_2, 
                    [:claim_per_dollar, :supply_per_claim]]) - 
            convert(Array, X[range_1:range_2, 
            [:specialty_mean_claim_per_dollar, 
            :specialty_mean_supply_per_claim]])) * inv(transpose(lambda) * lambda + psi_cov) * transpose(convert(Array, 
            X[range_1:range_2, 
                    [:claim_per_dollar, :supply_per_claim]]) - convert(Array, X[range_1:range_2, 
            [:specialty_mean_claim_per_dollar, :specialty_mean_supply_per_claim]])))
        
        file = open(string("Exports\\metaobjects\\cost_quality\\PA\\Safety\\Analysis\\Overall_Signal_Covariance_", 
                    values[item], ".csv"), "w")
        likelihood_file = open(string("Exports\\metaobjects\\cost_quality\\PA\\Safety\\Likelihood\\Likelihood_", 
                    values[item], ".csv"), "w")
        try
            for func_index = 1:(range_2 - range_1 + 1)
                x_row = string(
                    convert(Array, X[range_1:range_2, 
                            [:npi, :specialty_description, :drug_name]])[func_index, :])
                row = string(sum(func_value[func_index, :]))
                write(file, 
                    x_row[2:length(x_row) - 1] * ", " * row * "\n")
            end
            for func_index = 1:(range_2 - range_1 + 1)
                x_row = string(
                    convert(Array, X[range_1:range_2, 
                            [:npi, :specialty_description, :drug_name]])[func_index, :])
                row = string(sum(log(exp.(func_value[func_index, :]) * 1 / ((2 * pi)^(n/2) * (determinant_supplied)^0.5))))
                write(likelihood_file, 
                    x_row[2:length(x_row) - 1] * "," * row * "\n")
            end
        catch e
            print("Exception")
            throw(e)
        finally
            flush(file)
            close(file)
            flush(likelihood_file)
            close(likelihood_file)
        end
    end

    # Rewriting ψ as the diagonal matrix
    ψ = diagm(psi)
    determinant = norm(λ * transpose(conj(λ)) + ψ, 2)

    Dict("psi" => ψ, "norm_determinant" => determinant)

end

function execute_health_likelihood(X::DataFrames.DataFrame, determinant_supplied::Float64)
    total_size = size(X)[1]
    
    values = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", 
        "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", 
        "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", 
        "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", 
        "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", 
        "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", 
        "97", "98", "99", "100", "101", "102", "103", "104", "105", "106", "107", "108", "109", "110", 
        "111", "112", "113", "114", "115", "116", "117", "118", "119", "120", "121", "122", "123", "124", 
        "125", "126", "127", "128", "129", "130", "131", "132", "133", "134", "135", "136", "137", "138", 
        "139", "140", "141", "142", "143", "144", "145", "146", "147", "148", "149", "150", "151", "152", 
        "153", "154", "155", "156", "157", "158", "159", "160", "161", "162", "163", "164", "165", "166", 
        "167", "168", "169", "170", "171", "172", "173", "174", "175", "176", "177", "178", "179", "180", 
        "181", "182", "183", "184", "185", "186", "187", 
        "188", "189", "190", "191", "192", "193", "194", "195", "196", "197", "198", "199", "200"]

    # splitting by npi
    n = split_criteria["npi"]
    λ = Matrix{Complex}(n, 2) # R(n x k)
    ψ = Matrix{Float64}(n, n) # R(n x n)
    psi = Array{Float64, 1}(n)

    # splitting criteria taken for estimation
    splitting_factor = ceil(total_size / n)
    for item = 1:n
        m = Int32(splitting_factor)
        range_1 = Int32(1 + m *(item - 1)) # range_1 = 1246935
        range_2 = Int32(m * item) # range_2 = 1253038
        if total_size <= range_2
            range_2 = total_size
            m = total_size - range_1 + 1
        end
        # calculate the λ vector and X values 
        lambda = transpose(convert(Array, 
            X[range_1:range_2, 
                    [:cost_per_claim, :supply_per_claim]]) - 
            convert(Array, X[range_1:range_2, 
            [:specialty_mean_cost_per_claim, :specialty_mean_supply_per_claim]])) * 
            preprocessing.minmax_scale(convert(Array, 
            X[range_1:range_2, 
                [:drug_weight, :specialty_factor_drug]]
            ), feature_range=(0,1)) # R(2 x 2), equated lambda with x values

        λ[item, 1] = log(complex(lambda[1, 1], lambda[2, 1]) / sqrt(lambda[1, 1] ^ 2 + lambda[2, 1] ^ 2)) # Equivalent to spl factor drug claim
        λ[item, 2] = log(complex(lambda[1, 2], lambda[2, 2]) / sqrt(lambda[1, 2] ^ 2 + lambda[2, 2] ^ 2)) # Equivalent to spl factor drug supply

        # covariance ψ
        psi_cov = cov(convert(Array, X[range_1:range_2, 
            [:specialty_mean_cost_per_claim, 
            :specialty_mean_supply_per_claim]]) + preprocessing.minmax_scale(convert(Array, 
            X[range_1:range_2, 
                [:drug_weight, :specialty_factor_drug]]
            ), feature_range=(0,1)) * transpose(lambda)) # Initial dataset to understand the covariance matrix
        
        psi[item, 1] = psi_cov[2, 1]

        func_value = (-0.5 * 
        (convert(Array, 
            X[range_1:range_2, 
                    [:cost_per_claim, :supply_per_claim]]) - 
            convert(Array, X[range_1:range_2, 
            [:specialty_mean_cost_per_claim, 
            :specialty_mean_supply_per_claim]])) * inv(transpose(lambda) * lambda + psi_cov) * transpose(convert(Array, 
            X[range_1:range_2, 
                    [:cost_per_claim, :supply_per_claim]]) - convert(Array, X[range_1:range_2, 
            [:specialty_mean_cost_per_claim, :specialty_mean_supply_per_claim]])))
        
        file = open(string("Exports\\metaobjects\\cost_quality\\PA\\Health\\Analysis\\Overall_Signal_Covariance_", 
                    values[item], ".csv"), "w")
        likelihood_file = open(string("Exports\\metaobjects\\cost_quality\\PA\\Health\\Likelihood\\Likelihood_", 
                    values[item], ".csv"), "w")
        try
            for func_index = 1:(range_2 - range_1 + 1)
                x_row = string(
                    convert(Array, X[range_1:range_2, 
                            [:npi, :specialty_description, :drug_name]])[func_index, :])
                row = string(sum(func_value[func_index, :]))
                write(file, 
                    x_row[2:length(x_row) - 1] * ", " * row * "\n")
            end
            for func_index = 1:(range_2 - range_1 + 1)
                x_row = string(
                    convert(Array, X[range_1:range_2, 
                            [:npi, :specialty_description, :drug_name]])[func_index, :])
                row = string(sum(log(exp.(func_value[func_index, :]) * 1 / ((2 * pi)^(n/2) * (determinant_supplied)^0.5))))
                write(likelihood_file, 
                    x_row[2:length(x_row) - 1] * "," * row * "\n")
            end
        catch e
            print("Exception")
            throw(e)
        finally
            flush(file)
            close(file)
            flush(likelihood_file)
            close(likelihood_file)
        end
    end

    # Rewriting ψ as the diagonal matrix
    ψ = diagm(psi)
    determinant = norm(λ * transpose(conj(λ)) + ψ, 2)

    Dict("psi" => ψ, "norm_determinant" => determinant)

end

if EXECUTE_HEALTH_LIKELIHOOD == True:

    health_X = mysql_execute(opt_connection, "SELECT pa_state_spl_drug_npi.npi, 
    pa_state_spl_drug_npi.specialty_description, pa_state_spl_drug_npi.drug_name, 
    pa_cost_function.cost_per_claim,
    pa_cost_function.supply_per_claim, pa_cost_function.claim_per_dollar,
    pa_cost_function.bene_risk,
    pa_cost_function.drug_weight,
    pa_cost_function.specialty_factor_drug,
    pa_cost_function.specialty_mean_supply_per_claim, 
    pa_cost_function.specialty_mean_cost_per_claim
    FROM pa_cost_function
    INNER JOIN drug_information.pa_state_spl_drug_npi ON
    pa_state_spl_drug_npi.specialty_description = pa_cost_function.specialty_description
    AND pa_state_spl_drug_npi.drug_name = pa_cost_function.drug_name
    AND pa_state_spl_drug_npi.npi = pa_cost_function.npi
    ORDER BY 
    pa_cost_function.specialty_mean_supply_per_claim / pa_cost_function.specialty_std_supply_per_claim DESC,
    pa_cost_function.bene_risk DESC,
    pa_state_spl_drug_npi.drug_cost / pa_state_spl_drug_npi.total_day_supply DESC")

    health_object = execute_health_likelihood(health_X, 6.37181e8)

if EXECUTE_SAFETY_LIKELIHOOD == True:

    # Safety
    X = mysql_execute(opt_connection, "SELECT pa_state_spl_drug_npi.npi, 
    pa_state_spl_drug_npi.specialty_description, pa_state_spl_drug_npi.drug_name, 
    pa_cost_function.claim_per_dollar,
    pa_cost_function.supply_per_claim, pa_cost_function.cost_per_claim,
    pa_cost_function.bene_risk,
    pa_cost_function.specialty_weight,
    pa_cost_function.drug_factor_specialty,
    pa_cost_function.specialty_mean_claim_per_dollar, pa_cost_function.specialty_mean_supply_per_claim, 
    pa_cost_function.specialty_mean_cost_per_claim
    FROM pa_cost_function
    INNER JOIN drug_information.pa_state_spl_drug_npi ON
    pa_state_spl_drug_npi.specialty_description = pa_cost_function.specialty_description
    AND pa_state_spl_drug_npi.drug_name = pa_cost_function.drug_name
    AND pa_state_spl_drug_npi.npi = pa_cost_function.npi
    ORDER BY 
    pa_cost_function.specialty_mean_supply_per_claim / pa_cost_function.specialty_std_supply_per_claim DESC,
    pa_cost_function.bene_risk DESC,
    pa_state_spl_drug_npi.drug_cost / pa_state_spl_drug_npi.total_day_supply DESC
    ")

    determinant_supplied = 44521.01497670779
    safety_object = execute_safety_likelihood(X, determinant_supplied)
