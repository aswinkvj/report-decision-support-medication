import pickle
import pymysql
import os, sys, io
import numpy as np
sys.path.append(os.path.abspath(os.path.dirname('../code/')))
from sklearn.cluster import KMeans
from Database import Database, Partition
import CostFunction
import argparse

partition = Partition('PA')
database = Database(partition)
costfunction = CostFunction.CostFunction(database)

args = dict()

parser = argparse.ArgumentParser(description="KMeans Data Load")

parser.add_argument("--batch", action='store', default=False)
parser.add_argument("--start", action="store", default=False)
parser.add_argument("--run-lambda-health", action="store", default=False)
parser.add_argument("--run-lambda-safety", action="store", default=False)

parser.add_argument("--run-z-safety", action="store", default=False)
parser.add_argument("--run-z-health", action="store", default=False)

parser.add_argument("--state", action="store", default=False)

args = parser.parse_args()

state = args.state

batch = int(args.batch) # Increment the batches
start = int(args.start)

limit = 18446744073709551615

if args.run_lambda_health == '1':

    def database_get_health_lambda_cost_function(start, limit):
        return """
            SELECT claim_per_dollar, supply_per_claim, drug_mean_claim_per_dollar, 
            drug_mean_supply_per_claim, drug_std_claim_per_dollar,
            drug_std_supply_per_claim, drug_weight, specialty_weight, bene_risk, 
            IF(medicare_prvdr_enroll_status = 'E', 1, 0) as enroll_status, npi_claim_count,
            """ + partition.state + """_specialty_entropy.median_count,
            IF(drug_state_category = 'opioid', pa_cost_function.opioid, 
            IF(drug_state_category = 'antibiotic', pa_cost_function.antibiotic, 
            IF(drug_state_category = 'antipsychotic', pa_cost_function.antipsychotic, 
            IF(drug_state_category = 'HRM', pa_cost_function.hrm, 
            pa_cost_function.generic)))) as health_risk_ratio
            FROM """ + partition.state + """_cost_function
            INNER JOIN claim_aggregates.""" + partition.state + """_specialty_entropy
            ON """ + partition.state + """_specialty_entropy.specialty_description 
            = """ + partition.state + """_cost_function.specialty_description
            ORDER BY 
            supply_per_claim, (drug_mean_supply_per_claim / drug_std_supply_per_claim),
            (IF(drug_state_category = 'opioid', pa_cost_function.opioid, 
            IF(drug_state_category = 'antibiotic', pa_cost_function.antibiotic, 
            IF(drug_state_category = 'antipsychotic', pa_cost_function.antipsychotic, 
            IF(drug_state_category = 'HRM', pa_cost_function.hrm, 
            pa_cost_function.generic)))))
            LIMIT {0},{1}
            """.format(start, limit)

    def get_health_lambda_cost_function(start, limit):
            result = {'X': [], 'y': []}
            cursor = database.opt_connection.cursor()
            cursor.execute(database_get_health_lambda_cost_function(start, limit))
            X = cursor.fetchall()
            for item in X:
                result['X'].append(list(item))
            cursor.close()
            return result

if args.run_lambda_safety == '1':

    def database_get_safety_lambda_cost_function(start, limit):
        return """
            SELECT cost_per_claim, supply_per_claim, 
            specialty_mean_cost_per_claim, specialty_mean_supply_per_claim, 
            specialty_std_cost_per_claim,
            specialty_std_supply_per_claim, drug_weight, specialty_weight, bene_risk,
            IF(drug_state_category = 'opioid', 2, IF(drug_state_category = 'antibiotic', 3, 
            IF(drug_state_category = 'antipsychotic', 4, IF(drug_state_category = 'HRM', 5, 1)))) as risk_weight, 
            agg_cost_part_d, med_bene_count
            FROM """ + partition.state + """_cost_function
            ORDER BY 
            supply_per_claim, 
            (specialty_std_claim_per_dollar/specialty_mean_claim_per_dollar),
            bene_risk
            LIMIT {0},{1}
            """.format(start, limit)

    def get_safety_lambda_cost_function(start, limit):
        result = {'X': [], 'y': []}
        cursor = database.opt_connection.cursor()
        cursor.execute(database_get_safety_lambda_cost_function(start, limit))
        X = cursor.fetchall()
        for item in X:
            result['X'].append(list(item))
        cursor.close()
        return result

if args.run_z_safety == "1":
    def database_get_safety_z_cost_function(start, limit):
        return """
            SELECT claim_per_dollar, supply_per_claim,
            CAST(specialty_std_claim_per_dollar / specialty_mean_claim_per_dollar AS DECIMAL(18,15)), 
            CAST(specialty_std_supply_per_claim / specialty_mean_supply_per_claim AS DECIMAL(18,15)),
            """ + partition.state + """_specialty_entropy.npi_count,
            npi_drug_cost, npi_day_supply, """ + partition.state + """_specialty_entropy.drug_count
            FROM """ + partition.state + """_cost_function
            INNER JOIN claim_aggregates.""" + partition.state + """_specialty_entropy ON 
            """ + partition.state + """_specialty_entropy.specialty_description = 
            """ + partition.state + """_cost_function.specialty_description
            ORDER BY 
            """ + partition.state + """_specialty_entropy.npi_count,
            """ + partition.state + """_specialty_entropy.drug_count,
            """ + partition.state + """_cost_function.npi_day_supply
            LIMIT {0},{1}
            """.format(start, limit)

    def get_safety_z_cost_function(start, limit):
        result = {'X': [], 'y': []}
        cursor = database.opt_connection.cursor()
        cursor.execute(database_get_safety_z_cost_function(start, limit))
        X = cursor.fetchall()
        for item in X:
            result['X'].append(list(item))
        cursor.close()
        return result


if args.run_z_health == "1":
    def database_get_health_z_cost_function(start, limit):
        return """
            SELECT cost_per_claim, supply_per_claim,
            CAST(IF(drug_mean_cost_per_claim  != 0, 
            drug_std_cost_per_claim / drug_mean_cost_per_claim, 
            drug_std_cost_per_claim) AS DECIMAL(22,15)), 
            CAST(IF(drug_std_supply_per_claim != 0, 
            drug_mean_supply_per_claim / drug_std_supply_per_claim, 
            drug_mean_supply_per_claim) AS DECIMAL(22,15)),
            swd.prescribers, swd.part_d_claims, pct.total_day_supply,
            IF(drug_state_category = 'opioid', 2, IF(drug_state_category = 'antibiotic', 3, 
            IF(drug_state_category = 'antipsychotic', 4, IF(drug_state_category = 'HRM', 5, 1)))) as risk_weight
            FROM """ + partition.state + """_cost_function
            INNER JOIN drug_aggregates.state_wise_drug_state swd
            ON swd.drug_name = """ + partition.state + """_cost_function.drug_name
            AND swd.state = 'Pennsylvania'
            INNER JOIN drug_information.pa_cost_quality_table pct
            ON pct.specialty_description = """ + partition.state + """_cost_function.specialty_description
            AND pct.drug_name = """ + partition.state + """_cost_function.drug_name
            ORDER BY 
            swd.prescribers,
            swd.part_d_claims,
            pct.total_day_supply
            LIMIT {0},{1}
            """.format(start, limit)

    def get_health_z_cost_function(start, limit):
        result = {'X': [], 'y': []}
        cursor = database.opt_connection.cursor()
        cursor.execute(database_get_health_z_cost_function(start, limit))
        X = cursor.fetchall()
        for item in X:
            result['X'].append(list(item))
        cursor.close()
        return result

if __name__ == "__main__" and args.run_lambda_health == "1":

    # observation quantities
    z_safety = get_health_lambda_cost_function(start, 150000)
    X = np.array(z_safety['X'], dtype=np.dtype(np.float64))

    kmeans = KMeans(n_clusters=50, n_jobs=-1)
    kmeans_y_pred = kmeans.fit_predict(X)
    score = kmeans.score(X)
    print(score)

    cursor = database.opt_connection.cursor()
    cursor.execute("""SELECT specialty_description, drug_name, npi, specialty_factor_drug FROM pa_cost_function
    ORDER BY supply_per_claim, (drug_mean_supply_per_claim / drug_std_supply_per_claim),
    (IF(drug_state_category = 'opioid', pa_cost_function.opioid, 
    IF(drug_state_category = 'antibiotic', pa_cost_function.antibiotic, 
    IF(drug_state_category = 'antipsychotic', pa_cost_function.antipsychotic, 
    IF(drug_state_category = 'HRM', pa_cost_function.hrm, 
    pa_cost_function.generic))))) LIMIT {start},150000
    """.format(start=start))
    elements = cursor.fetchall()

    idx = 0
    cursor = database.opt_connection.cursor()
    database.opt_connection.begin()
    for item in elements:
        spl = item[0]
        drug = item[1]
        npi = item[2]
        spl_factor_drug = item[3]
        cluster = kmeans_y_pred[idx]
        cursor.execute("""
        INSERT INTO pa_kmeans_y_pred_spl_factor_drug
        VALUES
        ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')
        """.format(spl, drug, npi, spl_factor_drug, cluster, batch))
        idx = idx + 1
    database.opt_connection.commit()
    cursor.close()
    
if __name__ == "__main__" and args.run_lambda_safety == "1":

    # observation quantities
    lambda_safety = get_safety_lambda_cost_function(start, 150000)
    X = np.array(lambda_safety['X'], dtype=np.dtype(np.float64))
    kmeans = KMeans(n_clusters=100, n_jobs=-1)
    kmeans_drug_factor_spl_y_pred = kmeans.fit_predict(X)

    cursor = database.opt_connection.cursor()
    cursor.execute("""SELECT specialty_description, drug_name, npi, drug_factor_specialty FROM pa_cost_function
    ORDER BY 
    supply_per_claim, 
    (specialty_std_claim_per_dollar/specialty_mean_claim_per_dollar),
    bene_risk LIMIT {start},150000
    """.format(start=start))
    elements = cursor.fetchall()

    idx = 0
    cursor = database.opt_connection.cursor()
    database.opt_connection.begin()
    for item in elements:
        spl = item[0]
        drug = item[1]
        npi = item[2]
        drug_factor_spl = item[3]
        cluster = kmeans_drug_factor_spl_y_pred[idx]
        cursor.execute("""
        INSERT INTO pa_kmeans_y_pred_drug_factor_spl
        VALUES
        ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')
        """.format(spl, drug, npi, drug_factor_spl, cluster, batch))
        idx = idx + 1
    database.opt_connection.commit()
    cursor.close()

if __name__ == "__main__" and args.run_z_safety == "1":
    
    # Availability and Awareness
    while(batch <= 52):
        z_safety = get_safety_z_cost_function(start, 25000)
        X = np.array(z_safety['X'], dtype=np.dtype(np.float64))

        kmeans = KMeans(n_clusters=300, n_jobs=-1)
        kmeans_spl_weight_y_pred = kmeans.fit_predict(X)

        cursor = database.opt_connection.cursor()
        cursor.execute("""SELECT pa_cost_function.specialty_description,
        pa_cost_function.drug_name, pa_cost_function.npi, pa_cost_function.specialty_weight
        FROM pa_cost_function
        INNER JOIN claim_aggregates.pa_specialty_entropy ON
        pa_specialty_entropy.specialty_description = pa_cost_function.specialty_description
        ORDER BY 
        pa_specialty_entropy.npi_count,
        pa_specialty_entropy.drug_count,
        pa_cost_function.npi_day_supply
        LIMIT {start},{limit}
        """.format(start=start, limit=25000))
        elements = cursor.fetchall()

        idx = 0
        cursor = database.opt_connection.cursor()
        database.opt_connection.begin()
        for item in elements:
            spl = item[0]
            drug = item[1]
            npi = item[2]
            spl_weight = item[3]
            cluster = kmeans_spl_weight_y_pred[idx]
            cursor.execute("""
            INSERT INTO pa_kmeans_y_pred_spl_weight
            VALUES
            ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')
            """.format(spl, drug, npi, spl_weight, cluster, batch))
            idx = idx + 1
        database.opt_connection.commit()
        cursor.close()

        batch = batch + 1
        start = start + 25000

if __name__ == "__main__" and args.run_z_health == "1":
    
    # Availability and Awareness

    while(batch <= 52):

        z_safety = get_health_z_cost_function(start, 25000)
        X = np.array(z_safety['X'], dtype=np.dtype(np.float64))
        kmeans = KMeans(n_clusters=300, n_jobs=-1)
        kmeans_drug_weight_y_pred = kmeans.fit_predict(X)
        
        cursor = database.opt_connection.cursor()
        cursor.execute("""
        SELECT pa_cost_function.specialty_description, 
        pa_cost_function.drug_name, pa_cost_function.npi, 
        pa_cost_function.drug_weight 
        FROM pa_cost_function
        INNER JOIN drug_aggregates.state_wise_drug_state swd
        ON swd.drug_name = pa_cost_function.drug_name
        AND swd.state = 'Pennsylvania'
        INNER JOIN drug_information.pa_cost_quality_table pct
        ON pct.specialty_description = pa_cost_function.specialty_description
        AND pct.drug_name = pa_cost_function.drug_name
        ORDER BY 
        swd.prescribers,
        swd.part_d_claims,
        pct.total_day_supply
        LIMIT {start},{limit}
        """.format(start=start, limit=25000))
        elements = cursor.fetchall()

        idx = 0
        cursor = database.opt_connection.cursor()
        database.opt_connection.begin()
        for item in elements:
            spl = item[0]
            drug = item[1]
            npi = item[2]
            drug_weight = item[3]
            cluster = kmeans_drug_weight_y_pred[idx]
            cursor.execute("""
            INSERT INTO pa_kmeans_y_pred_drug_weight
            VALUES
            ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')
            """.format(spl, drug, npi, drug_weight, cluster, batch))
            idx = idx + 1
        database.opt_connection.commit()
        cursor.close()
        
        batch = batch + 1
        start = start + 25000
