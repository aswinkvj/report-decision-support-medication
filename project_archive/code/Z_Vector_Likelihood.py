import numpy as np
import pandas as pd
import scipy
import io, os, sys
sys.path.append(os.path.abspath(os.path.dirname('../code/')))
import pymysql
import CostFunction
from Database import Database, Partition
from sklearn import metrics
import seaborn
import pickle
from sklearn.svm import LinearSVC
from sklearn.gaussian_process import GaussianProcessRegressor
import argparse
from sklearn.model_selection import KFold

args = dict()

argparse.add_argument("--state", action='store', default=True)
argparse.add_argument("--run-empirical", action='store', default=False)
argparse.add_argument("--run-cognitive", action='store', default=False)
argparse.add_argument("--specialty-selected", action="store", default=False)

args = argparse.parse_agrs()

def get_gauss_data(empirical_data):
    gauss_df = pd.DataFrame(columns=['specialty_description', 
    'drug_name', 'empirical_estimation', 'Drug Count', 'Prescribers', 'Total Day Supply', 'Total Claim Count'])
    for item in empirical_data:
        gauss_df = gauss_df.append(dict(zip('specialty_description', 
    'drug_name', 'empirical_estimation', 'Drug Count', 'Prescribers', 'Total Day Supply', 'Total Claim Count'), 
                [item[1], item[2], np.float32(item[0]), np.int32(item[4]), np.int32(item[5]),
                np.int32(item[6]), np.int32(item[7])]), ignore_index=True)
    return gauss_df

def empirical_data_update(gauss_df):
    cursor = database.info_connection.cursor()
    cursor.execute("""
    SELECT DISTINCT(specialty_description) FROM pa_state_spl_drug_npi
    """)
    specialties = cursor.fetchall()
    for item in specialties:
        gauss_index = gauss_df['specialty_description'] == item[0]
        regressor_object = regressor.fit(gauss_df[gauss_index].values[:, 3:], 
                    gauss_df[gauss_index]['empirical_estimation'].values[:, 0])
        idx = 0
        cursor = database.opt_connection.cursor()
        y_pred = regressor.predict(gauss_df[gauss_index].values[:, 4:])
        for y_value in y_pred:
            try:
                specialty = item[0]
                drug_name = gauss_df[gauss_index].values[idx, 1]
                database.opt_connection.begin()
                distance = math.abs((gauss_df[gauss_index][idx, 2] - y_value) / gauss_df[gauss_index][idx, 2])
                cursor.execute("""UPDATE pa_spl_drug_shared_states SET
                empirical_error = '{0}' 
                WHERE specialty_description = '{1}'
                AND drug_name = '{2}')""".format(
                (gauss_df[gauss_index]['empirical_estimation'].values[idx, 0] - item) / 
                gauss_df[gauss_index]['empirical_estimation'].values[idx, 0], 
                specialty_description, drug_name)
                )
                if idx % 100:
                    database.opt_connection.commit()
                idx = idx + 1
            except Exception as e:
                database.opt_connection.rollback()
                raise e
            cursor.close()

if __name__ == "__main__":

    partition = Partition('PA')
    database = Database(partition)
    costfunction = CostFunction.CostFunction(database)

if __name__ == "__main__" and args.run_empirical:

    specialty_selected = args.specialty_selected

    empirical_error_sql = """
    SELECT pa_spl_drug_shared_states.empirical_estimation,
    pa_state_spl_drug_npi.specialty_description, pa_state_spl_drug_npi.drug_name, 
    pa_state_spl_drug_npi.npi, pa_specialty_entropy.drug_count, swd.prescribers, 
    pa_state_spl_drug_npi.total_day_supply, pa_state_spl_drug_npi.total_claim_count
    FROM pa_state_spl_drug_npi
    INNER JOIN claim_aggregates.pa_specialty_entropy ON 
    pa_specialty_entropy.specialty_description = pa_state_spl_drug_npi.specialty_description
    INNER JOIN drug_aggregates.state_wise_drug_state swd
    ON swd.drug_name = pa_state_spl_drug_npi.drug_name
    AND swd.state = 'Pennsylvania'
    INNER JOIN optimization_tables.pa_spl_drug_shared_states ON
    pa_spl_drug_shared_states.specialty_description = pa_state_spl_drug_npi.specialty_description AND
    pa_spl_drug_shared_states.drug_name = pa_state_spl_drug_npi.drug_name
    INNER JOIN optimization_tables.pa_kmeans_y_pred_drug_weight
    ON pa_kmeans_y_pred_drug_weight.npi = pa_state_spl_drug_npi.npi AND
    pa_state_spl_drug_npi.specialty_description = pa_kmeans_y_pred_drug_weight.specialty_description AND
    pa_state_spl_drug_npi.drug_name = pa_kmeans_y_pred_drug_weight.drug_name
    WHERE swd.state = 'Pennsylvania' AND pa_state_spl_drug_npi.specialty_description = 
    '{0}'
    """.format(specialty_selected)

    cursor = database.info_connection.cursor()
    cursor.execute(empirical_error_sql)
    gauss_array = np.array(cursor.fetchall())
    cursor.close()

    pickle.dump(gauss_array, open("pickle\\" + specialty_selected + "_empirical_data.pkl", "wb"))
    
    mean_values = np.mean(gauss_array[:, 0:], axis=0)
    array_x = np.transpose([gauss_array[:, 0] - mean_values[0], 
                            gauss_array[:, 1] - mean_values[1],
                        gauss_array[:, 2] - mean_values[2],
                        gauss_array[:, 3] - mean_values[3]])
    x = array_x
    print("Maximum of y: ", max(y))
    print("Minimum of y: ", min(y))

    print("\nEnter the Gaussian Value in the estimate: ")
    GAUSSIAN = input("\nEnter the Gaussian Value : ")

    def gaussian(x, det, value, sigma):    
        global GAUSSIAN
        return GAUSSIAN + np.exp(np.log(np.sum(np.exp(-0.5 * (1/sigma) * np.square(np.subtract(x, value))), 
                                    axis=1)) + np.log(1 / (2 * math.pi)**0.5 * 1 / det))

    covariance = np.cov(np.prod([gauss_array[:, 0], gauss_array[:, 2]], axis=0), 
                            np.prod([gauss_array[:, 1], gauss_array[:, 3]], axis=0))
    

    theta_init = [np.sqrt(np.linalg.norm(gauss_array[:, 0:4], 2)), 0.05, covariance[0,1] / np.square(len(x))]
    theta_vals, covar = 0, 0
    theta_vals, covar = curve_fit(gaussian, x, y, p0=theta_init)

    print(theta_init)
    print(theta_vals, covar)

if __name__ == "__main__" and args.run_cognitive:

    cognitive_bias_sql = """
    SELECT CONCAT(specialty_description, "_", drug_name) as y_value, specialty_description, drug_name, 
    drug_weight, specialty_weight, specialty_factor_drug, CAST(drug_factor_specialty AS UNSIGNED) as drug_factor_specialty
    FROM pa_factors;
    """

    cursor = database.presc_connection.cursor()
    cursor.execute(cognitive_bias_sql)
    cognitive_bias = np.array(cursor.fetchall())
    cursor.close()

    linear_svc = LinearSVC()
    cog_x = np.array(cognitive_bias[:, 3:7], dtype=np.dtype(np.float64))
    cog_y = cognitive_bias[:, 0]
    kf = KFold(n_splits=75, shuffle=True)
    for train_index, test_index in kf.split(cog_x):
        X_train, X_test = cog_x[train_index], cog_x[test_index]
        y_train, y_test = cog_y[train_index], cog_y[test_index]

    linear_svc.fit(X_train, y_train)

    score = linear_svc.score(cog_x, cog_y)
    cog_y_pred = linear_svc.predict(cog_x)

    print("The accuracy score of Linear SVC: ", score)

    