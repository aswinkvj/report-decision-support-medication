
Referring to the Decision Tree Classifier provided within the file: Sigh_Value.ipynb, 
the Decision Model provided below is used to evaluate the Death in terms of a hypothesis.

Less Visibility | Greater Visibility
------------ | -------------
Evidence Based | Risk Aware
Reasoning | Risk Assessor

Provided below is the list of characteristics that drug prescriptions have:

Characteristics | Contributing Factors
---------------- | --------------------
** Purpose ** | Medication Type, Confidence Level for Prescriptions
** Usage ** | Claim per DOLLAR (CD), Supply per Claim (SC)
** Cost ** | Cost per Claim (CC), Mean Cost per Claim

Death is likely when the purpose is not included within the hypothesis.

Please refer to the following table to understand where does Care, Drug, Specialty and Patient lie.

Parameter | Problem
-------- | -------
Claim per DOLLAR (CD) | Patient towards Care
Supply per Claim (SC) | Care only
Cost per Claim (CC) | Drug and Specialty towards Care
Cost per Fill (CP) | Planning towards Care

Our function must be of the form:

** f(CD, SC, CC) = np.transpose([theta_CD, theta_SC]) . X(CD) . X(SC) + np.transpose([theta_CD, theta_CC]) . X(CD) . X(CC) + np.transpose([theta_SC, theta_CC]) . X(SC) . X(CC) **

The function f must be optimised using Liblinear

Our hypotheses must be of the form:

** Lagrange(x) = Sigh(x) + SUM(Alpha(i) * Parameter_Function(i)) + SUM(Beta(i) * Hypothesis(i)) ** 

where, Sigh(x) is the distribution of Sigh Value which we have obtained;
Parameter Function is the Lagrangian's 


```python
# see regular updates
states = np.array(['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'])
# sample death_ratio_noise
death_ratio_noise = np.array([6.1903382855999993, 6.7982931708999992, 7.292783288399999, 5.4002128995999996, 5.3441316743999998, 6.8649352261000001, 6.9544484915999991, 8.1211898660999999, 5.7678324323999988, 5.4098284404999992, 5.2034490140999994, 5.6824040388999997, 5.704858269599999, 7.1344004423999996, 4.6828500345159991, 5.2692138663999994, 9.9573152604999979, 6.7749289668999992, 6.5974314375999992, 7.2102227004999992, 7.6623450215999993, 7.1236008860999993, 4.8628498388639994, 5.2148805620999994, 7.069525453599999, 5.4692737068999993, 4.4006343783999995, 7.7685200940999994, 10.3221113541, 5.9354796588999994, 10.833963597599999, 5.3229831143999995, 5.8546032404999995, 4.306436463240999, 9.5770649580999994, 8.0249398323999994, 5.7172685063999991, 8.5273517004999988, 9.4709011379999986, 6.1084471101000002, 4.5091253782409995, 7.7693405283999999, 4.9202386366239992, 8.274052254099999, 5.707532840499999, 5.3605976423999993, 5.9251867140999996, 15.3864250804, 6.1593944675999994, 7.4527682855999995])
```

Referring to the following table, following parameters are required for optimisation of algorithms:

   | Claim per DOLLAR | Supply per Claim | Cost per Claim
--  | ---------------- | ---------------- | --------------
Claim per DOLLAR |   | ** OPTIMISED CLAIMS / DOSAGES ** | ** PRICING FACTOR **
Supply per Claim |   |   | ** DRUG FACILITY **
Cost per Claim |   |   |   

We know these function variables are dependent on Specialty, Drug and Care / Patient. We chose Lagrangian in order to obtain optimisation using quadratic programming such that we obtain a relation in degree 2, involving the variables.

Hence, we have 4 sub-factors in the decision space contributing to the reverse engineered factors, Lamba and the Normalised and Scaled factor, z; found from the Factor Analysis Algorithm.

The table for distinctive factors have been represented below:

  | Safety | Health
----- | ------  | ------
Drug Influence | ** Drug Factor for Specialty ** | ** Drug Weight **
Specialty Influence | ** Specialty Weight ** | ** Specialty Factor for Drug **

From the table, what we can recollect is:
    1. Drug Awareness
    2. The effect of drugs
    3. The causal conditions for drugs involving the 3 parameters
    4. Specialty Availability
    5. Specialty's contribution towards Care
    6. Drug's Individual Coverage, so that a Medicare Plan can be devised

Safety takes control of:
    1. Claim per DOLLAR, and
    2. Supply per Claim
    
Health takes control of:
    1. Supply per Claim, and
    2. Cost per Claim
    


Let us consider the Factor Analysis for the domain of State 'Pennsylvania', with:
    - Drug Factor for Specialty, and
    - Specialty Factor for Drugs
    
From the data provided within "Part D Prescriber PUF Grand Totals and Overall Averages, Calendar Year 2015", found from:

[Medicare Provider Utilization and Payment Data: Part D Prescriber](https://www.cms.gov/Research-Statistics-Data-and-Systems/Statistics-Trends-and-Reports/Medicare-Provider-Charge-Data/Part-D-Prescriber.html)

In this case, consider we are evaluating the drug weights and specialty weights, which stands for the lambda factor in the Factor Analysis algorithm. Our zero mean and unit variance distribution would be:

Our aim is now to identify the z and lambda and apply them inside the likelihood estimation what we have achieved from the evaluation of 6-Criteria:

    1. The z vector
    2. The Lambda vector
    3. The Safety vector
    4. The Health vector
    5. The Drug Influence
    6. The Specialty Influence
    
Appropriate z and lambda values are obtained by 

    - Markov Chain Monte Carlo approximation since the gradual entry of prescription claims into the Part D Medicare is independent of the partitioned samples of dataset prior to the entry.
    - Since our specialties are reaching these medicare claims, we need a Hamiltonian to determine what is to identify and then communicate that with our vectors related to first 4 criteria
    
Please refer to:
    [http://www.mcmchandbook.net/HandbookChapter5.pdf](http://www.mcmchandbook.net/HandbookChapter5.pdf)
