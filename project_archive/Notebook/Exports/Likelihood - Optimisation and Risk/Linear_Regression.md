

```python
%matplotlib inline
import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
import sklearn.preprocessing as preprocessing
from sklearn import model_selection
import pymysql
import io, sys, os
sys.path.append(os.path.abspath(os.path.dirname('../code/')))
from Database import Database, Partition
import CostFunction
import seaborn
import matplotlib.pyplot as plt
```


```python
partition = Partition('PA')
database = Database(partition)
costfunction = CostFunction.CostFunction(database)
```


```python
opt_connection = pymysql.connect(user='root', password='root',
                                 host='localhost',
                                 db='optimization_tables')

z_health_data_sql = """
SELECT pa_cost_function.drug_weight, cost_per_claim, supply_per_claim,
CAST(IF(drug_mean_cost_per_claim  != 0, 
drug_std_cost_per_claim / drug_mean_cost_per_claim, 
drug_std_cost_per_claim) AS DECIMAL(22,15)), 
CAST(IF(drug_std_supply_per_claim != 0, 
drug_mean_supply_per_claim / drug_std_supply_per_claim, 
drug_mean_supply_per_claim) AS DECIMAL(22,15)),
swd.prescribers, swd.part_d_claims, pct.total_day_supply,
IF(drug_state_category = 'opioid', 2, IF(drug_state_category = 'antibiotic', 3, 
IF(drug_state_category = 'antipsychotic', 4, IF(drug_state_category = 'HRM', 5, 1)))) as risk_weight
FROM pa_cost_function
INNER JOIN drug_aggregates.state_wise_drug_state swd
ON swd.drug_name = pa_cost_function.drug_name
AND swd.state = 'Pennsylvania'
INNER JOIN drug_information.pa_cost_quality_table pct
ON pct.specialty_description = pa_cost_function.specialty_description
AND pct.drug_name = pa_cost_function.drug_name
ORDER BY 
swd.prescribers,
swd.part_d_claims,
pct.total_day_supply
"""

cursor = database.opt_connection.cursor()
cursor.execute(z_health_data_sql)
z_health = np.array(cursor.fetchall())
cursor.close()
```


```python
X = z_health[:, 1:]
y = z_health[:, 0]
regressor = LinearRegression()

X_train, X_test, y_train, y_test = model_selection.train_test_split(X, y, test_size=0.33, random_state=10)

regressor_object = regressor.fit(X=X_train, y=preprocessing.minmax_scale(X=y_train, feature_range=(0, 1)))

score = regressor_object.score(X_test, y_test)
```

    C:\Users\Aswin Vijayakumar\Anaconda3\Lib\site-packages\sklearn\utils\validation.py:429: DataConversionWarning: Data with input dtype object was converted to float64.
      warnings.warn(msg, _DataConversionWarning)
    


```python
from sklearn import metrics

y_pred = regressor_object.predict(X)
y_true = y

print("Algorithm score: ", score)
print("Coefficient of determination: ", metrics.r2_score(y_true, y_pred))
print("Explained Variance Score: ", metrics.explained_variance_score(y_true, y_pred))
```

    Algorithm score:  -3.01546050259
    Coefficient of determination:  -3.04594394627
    Explained Variance Score:  -1.85701554297
    


```python
z_safety_sql = """
SELECT specialty_weight, claim_per_dollar, supply_per_claim,
CAST(specialty_std_claim_per_dollar / specialty_mean_claim_per_dollar AS DECIMAL(18,15)), 
CAST(specialty_std_supply_per_claim / specialty_mean_supply_per_claim AS DECIMAL(18,15)),
pa_specialty_entropy.npi_count,
npi_drug_cost, npi_day_supply, pa_specialty_entropy.drug_count
FROM pa_cost_function
INNER JOIN claim_aggregates.""" + partition.state + """_specialty_entropy ON 
pa_specialty_entropy.specialty_description = 
pa_cost_function.specialty_description
ORDER BY 
pa_specialty_entropy.npi_count,
pa_specialty_entropy.drug_count,
pa_cost_function.npi_day_supply
"""

cursor = database.opt_connection.curs
cursor.execute(z_safety_sql)
z_safety = cursor.fetchall()
cursor.close()
```


```python
regressor_object = LinearRegression()
z_safety = np.array(z_safety)
X = z_safety[:, 1:]
y = z_safety[:, 0]
X_train, X_test, y_train, y_test = model_selection.train_test_split(X, y, test_size=0.33, random_state=10)
regressor_object.fit(X_train, y_train)
score = regressor.score(X_test, y_test)
```


```python
y_pred = regressor_object.predict(X)
y_true = y

print("Algorithm score: ", score)
print("Coefficient of determination: ", metrics.r2_score(y_true, y_pred))
print("Explained Variance Score: ", metrics.explained_variance_score(y_true, y_pred))
```

    Algorithm score:  -1.98391663669
    Coefficient of determination:  0.813625542142
    Explained Variance Score:  0.813625558966
    
