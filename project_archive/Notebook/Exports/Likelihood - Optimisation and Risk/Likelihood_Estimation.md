
Likelihood estimation with parameters: ** z, λ, covariance (ψ), x and μ **

Assuming the likelihiood function has already been formed from the Medicare Part D Claim Data, the likelihood estimation function must be of the form:

** P(x) = N(μ, λλ^T + ψ) **

From investigation, it is found that these are the values by which you should split the dataset for analysis:

Name | NPI | SPECIALTY | DRUG
-- | --- | --------- | -----
Count | 42000 | 100 | 2000

The best split by which the dataset can go is to take n = 200.

Looking at the distributions of the `pa_factors` table:
the attributes - drug weight and specialty factor for drug are noticeable as the drug weight is equal for certain rows and the factor is also equal for less observable rows. 

The bias for evaluating the usage and npi(s) must be obtained by splitting the dataset by "Specialty Factor for Drugs". The split should be done using partions of standard deviations from the mean value. The bias can be measured by taking the ratio of Standard deviation and the Mean of the Quantity - Supply per Claim or Claim per Dollar.

The likelihood estimation in this page shows the signal covariance and the noise covariance from the Gaussian distribution taken from n samples. It is also known that greater the n, greater is the predictability.

For calculation purposes, the Frobenius norm is taken from the covariance_ψ, for evaluation of the Gaussian determinant:
(λλ^T + ψ)


```julia
using MySQL
using PyCall
using Logging
loggerFile = Logger("loggerFile")
@pyimport pickle
@pyimport sklearn.preprocessing as preprocessing
@pyimport csv
file_object = open("logfile.log", "a")
Logging.configure(loggerFile, output=file_object, level=DEBUG)
PICKLE_PATH = "Y:\\Udacity\\Final_Projects\\final\\WORKSPACE\\Serialized\\PA"
using DataFrames
```

    WARNING: Method definition info(Any...) in module Base at util.jl:532 overwritten in module Logging at C:\Users\Aswin Vijayakumar\.julia\v0.6\Logging\src\Logging.jl:115.
    WARNING: Method definition warn(Any...) in module Base at util.jl:585 overwritten in module Logging at C:\Users\Aswin Vijayakumar\.julia\v0.6\Logging\src\Logging.jl:115.
    


```julia
# Calculating the likelihood value for Safety, CD and SC

presc_connection = mysql_connect("localhost", "root", "root", "prescription_aggregates")
info_connection = mysql_connect("localhost", "root", "root", "drug_information")
opt_connection = mysql_connect("localhost", "root", "root", "optimization_tables")
aggregates_connection = mysql_connect("localhost", "root", "root", "drug_aggregates")

split_criteria = Dict("npi" => 200, "specialty" => 1, "drug" => 10) # whichever is the split with largest
```




    Dict{String,Int64} with 3 entries:
      "specialty" => 1
      "npi"       => 200
      "drug"      => 10



This section is calcualting the Likelihood estimation based on Medication Coverage. The feature set used are:
- Drug Cost per claim
- Supply per Claim
- Beneficiary Count
- Claim Count

Preprocessing for Drug Cost and Day Supply has been conducted in order to comply with the lambda values to be generated.


```julia
# Safety
X = mysql_execute(opt_connection, "SELECT pa_state_spl_drug_npi.npi, 
pa_state_spl_drug_npi.specialty_description, pa_state_spl_drug_npi.drug_name, 
pa_cost_function.claim_per_dollar,
pa_cost_function.supply_per_claim, pa_cost_function.cost_per_claim,
pa_cost_function.bene_risk,
pa_cost_function.specialty_weight,
pa_cost_function.drug_factor_specialty,
pa_cost_function.specialty_mean_claim_per_dollar, pa_cost_function.specialty_mean_supply_per_claim, 
pa_cost_function.specialty_mean_cost_per_claim
FROM pa_cost_function
INNER JOIN drug_information.pa_state_spl_drug_npi ON
pa_state_spl_drug_npi.specialty_description = pa_cost_function.specialty_description
AND pa_state_spl_drug_npi.drug_name = pa_cost_function.drug_name
AND pa_state_spl_drug_npi.npi = pa_cost_function.npi
ORDER BY 
pa_cost_function.specialty_mean_supply_per_claim / pa_cost_function.specialty_std_supply_per_claim DESC,
pa_cost_function.bene_risk DESC,
pa_state_spl_drug_npi.drug_cost / pa_state_spl_drug_npi.total_day_supply DESC
")
1
```




    1



##### Safety Likelihood

The log likelihood value and the Gaussian Covariance value has been calculated. The batch sizes has been decided based on the factors such as debiasing the distribution, beneficiary risk and cost per supply.


```julia
function execute_safety_likelihood(X::DataFrames.DataFrame, determinant_supplied::Float64)
    total_size = size(X)[1]
    
    values = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", 
        "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", 
        "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", 
        "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", 
        "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", 
        "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", 
        "97", "98", "99", "100", "101", "102", "103", "104", "105", "106", "107", "108", "109", "110", 
        "111", "112", "113", "114", "115", "116", "117", "118", "119", "120", "121", "122", "123", "124", 
        "125", "126", "127", "128", "129", "130", "131", "132", "133", "134", "135", "136", "137", "138", 
        "139", "140", "141", "142", "143", "144", "145", "146", "147", "148", "149", "150", "151", "152", 
        "153", "154", "155", "156", "157", "158", "159", "160", "161", "162", "163", "164", "165", "166", 
        "167", "168", "169", "170", "171", "172", "173", "174", "175", "176", "177", "178", "179", "180", 
        "181", "182", "183", "184", "185", "186", "187", 
        "188", "189", "190", "191", "192", "193", "194", "195", "196", "197", "198", "199", "200"]

    # splitting by npi
    n = split_criteria["npi"]
    λ = Matrix{Complex}(n, 2) # R(n x k)
    ψ = Matrix{Float64}(n, n) # R(n x n)
    psi = Array{Float64, 1}(n)

    # splitting criteria taken for estimation
    splitting_factor = ceil(total_size / n)
    for item = 1:n
        m = Int32(splitting_factor)
        range_1 = Int32(1 + m *(item - 1)) # range_1 = 1246935
        range_2 = Int32(m * item) # range_2 = 1253038
        if total_size <= range_2
            range_2 = total_size
            m = total_size - range_1 + 1
        end
        # calculate the λ vector and X values 
        # Assuming the likelihood function has already been formed from the Medicare Claims data
        lambda = transpose(convert(Array, 
            X[range_1:range_2, 
                    [:claim_per_dollar, :supply_per_claim]]) - 
            convert(Array, X[range_1:range_2, 
            [:specialty_mean_claim_per_dollar, :specialty_mean_supply_per_claim]])) * 
            preprocessing.minmax_scale(convert(Array, 
            X[range_1:range_2, 
                [:specialty_weight, :drug_factor_specialty]]
            ), feature_range=(0,1)) # R(2 x 2), equated lambda with x values

        λ[item, 1] = log(complex(lambda[1, 1], lambda[2, 1]) / sqrt(lambda[1, 1] ^ 2 + lambda[2, 1] ^ 2)) # Equivalent to spl factor drug claim
        λ[item, 2] = log(complex(lambda[1, 2], lambda[2, 2]) / sqrt(lambda[1, 2] ^ 2 + lambda[2, 2] ^ 2)) # Equivalent to spl factor drug supply

        # covariance ψ
        psi_cov = cov(convert(Array, X[range_1:range_2, 
            [:specialty_mean_claim_per_dollar, 
            :specialty_mean_supply_per_claim]]) + preprocessing.minmax_scale(convert(Array, 
            X[range_1:range_2, 
                [:specialty_weight, :drug_factor_specialty]]
            ), feature_range=(0,1)) * transpose(lambda)) # Initial dataset to understand the covariance matrix
        
        psi[item, 1] = psi_cov[2, 1]

        func_value = (-0.5 * 
        (convert(Array, 
            X[range_1:range_2, 
                    [:claim_per_dollar, :supply_per_claim]]) - 
            convert(Array, X[range_1:range_2, 
            [:specialty_mean_claim_per_dollar, 
            :specialty_mean_supply_per_claim]])) * inv(transpose(lambda) * lambda + psi_cov) * transpose(convert(Array, 
            X[range_1:range_2, 
                    [:claim_per_dollar, :supply_per_claim]]) - convert(Array, X[range_1:range_2, 
            [:specialty_mean_claim_per_dollar, :specialty_mean_supply_per_claim]])))
        
        file = open(string("Exports\\metaobjects\\cost_quality\\PA\\Safety\\Analysis\\Overall_Signal_Covariance_", 
                    values[item], ".csv"), "w")
        likelihood_file = open(string("Exports\\metaobjects\\cost_quality\\PA\\Safety\\Likelihood\\Likelihood_", 
                    values[item], ".csv"), "w")
        try
            for func_index = 1:(range_2 - range_1 + 1)
                x_row = string(
                    convert(Array, X[range_1:range_2, 
                            [:npi, :specialty_description, :drug_name]])[func_index, :])
                row = string(sum(func_value[func_index, :]))
                write(file, 
                    x_row[2:length(x_row) - 1] * ", " * row * "\n")
            end
            for func_index = 1:(range_2 - range_1 + 1)
                x_row = string(
                    convert(Array, X[range_1:range_2, 
                            [:npi, :specialty_description, :drug_name]])[func_index, :])
                row = string(sum(log(exp.(func_value[func_index, :]) * 1 / ((2 * pi)^(n/2) * (determinant_supplied)^0.5))))
                write(likelihood_file, 
                    x_row[2:length(x_row) - 1] * "," * row * "\n")
            end
        catch e
            print("Exception")
            throw(e)
        finally
            flush(file)
            close(file)
            flush(likelihood_file)
            close(likelihood_file)
        end
    end

    # Rewriting ψ as the diagonal matrix
    ψ = diagm(psi)
    determinant = norm(λ * transpose(conj(λ)) + ψ, 2)

    Dict("psi" => ψ, "norm_determinant" => determinant)

end
```




    execute_safety_likelihood (generic function with 2 methods)



###### Console output from Julia

    - Dict{String,Any} with 2 entries:
          "psi"              => [-27575.1 0.0 … 0.0 0.0; 0.0 5.4786 … 0.0 0.0; … ; 0.0 …
          "norm_determinant" => NaN


```julia
determinant_supplied = 44521.01497670779
safety_object = execute_safety_likelihood(X, determinant_supplied)
```

    [1m[33mWARNING: [39m[22m[33mlog{T <: Number}(x::AbstractArray{T}) is deprecated, use log.(x) instead.[39m
    Stacktrace:
     [1] [1mdepwarn[22m[22m[1m([22m[22m::String, ::Symbol[1m)[22m[22m at [1m.\deprecated.jl:70[22m[22m
     [2] [1mlog[22m[22m[1m([22m[22m::Array{Float64,1}[1m)[22m[22m at [1m.\deprecated.jl:57[22m[22m
     [3] [1mexecute_safety_likelihood[22m[22m[1m([22m[22m::DataFrames.DataFrame, ::Float64[1m)[22m[22m at [1m.\In[67]:88[22m[22m
     [4] [1minclude_string[22m[22m[1m([22m[22m::String, ::String[1m)[22m[22m at [1m.\loading.jl:515[22m[22m
     [5] [1minclude_string[22m[22m[1m([22m[22m::Module, ::String, ::String[1m)[22m[22m at [1mC:\Users\Aswin Vijayakumar\.julia\v0.6\Compat\src\Compat.jl:464[22m[22m
     [6] [1mexecute_request[22m[22m[1m([22m[22m::ZMQ.Socket, ::IJulia.Msg[1m)[22m[22m at [1mC:\Users\Aswin Vijayakumar\.julia\v0.6\IJulia\src\execute_request.jl:154[22m[22m
     [7] [1meventloop[22m[22m[1m([22m[22m::ZMQ.Socket[1m)[22m[22m at [1mC:\Users\Aswin Vijayakumar\.julia\v0.6\IJulia\src\eventloop.jl:8[22m[22m
     [8] [1m(::IJulia.##14#17)[22m[22m[1m([22m[22m[1m)[22m[22m at [1m.\task.jl:335[22m[22m
    while loading In[68], in expression starting on line 2
    




    Dict{String,Any} with 2 entries:
      "psi"              => [-27575.1 0.0 … 0.0 0.0; 0.0 5.4786 … 0.0 0.0; … ; 0.0 …
      "norm_determinant" => NaN



##### Health Likelihood

The health likelihood evaluates based on Drug's Mean Cost per Claim and Drug's Mean Supply per Claim. The Health vector that is under consideration for the Gaussian z values are:
- Drug Weight
- Specialty Factor for Drugs

The lambda values are found from:

**λ = E[ (z - E(z)) * (x - E(x) ))]**

The data for health likleihood is ordered by the same ORDER BY parameters used in the data for safety likelihood.


```julia
health_X = mysql_execute(opt_connection, "SELECT pa_state_spl_drug_npi.npi, 
pa_state_spl_drug_npi.specialty_description, pa_state_spl_drug_npi.drug_name, 
pa_cost_function.cost_per_claim,
pa_cost_function.supply_per_claim, pa_cost_function.claim_per_dollar,
pa_cost_function.bene_risk,
pa_cost_function.drug_weight,
pa_cost_function.specialty_factor_drug,
pa_cost_function.specialty_mean_supply_per_claim, 
pa_cost_function.specialty_mean_cost_per_claim
FROM pa_cost_function
INNER JOIN drug_information.pa_state_spl_drug_npi ON
pa_state_spl_drug_npi.specialty_description = pa_cost_function.specialty_description
AND pa_state_spl_drug_npi.drug_name = pa_cost_function.drug_name
AND pa_state_spl_drug_npi.npi = pa_cost_function.npi
ORDER BY 
pa_cost_function.specialty_mean_supply_per_claim / pa_cost_function.specialty_std_supply_per_claim DESC,
pa_cost_function.bene_risk DESC,
pa_state_spl_drug_npi.drug_cost / pa_state_spl_drug_npi.total_day_supply DESC")
1
```




    1




```julia
function execute_health_likelihood(X::DataFrames.DataFrame, determinant_supplied::Float64)
    total_size = size(X)[1]
    
    values = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", 
        "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", 
        "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", 
        "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", 
        "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", 
        "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", 
        "97", "98", "99", "100", "101", "102", "103", "104", "105", "106", "107", "108", "109", "110", 
        "111", "112", "113", "114", "115", "116", "117", "118", "119", "120", "121", "122", "123", "124", 
        "125", "126", "127", "128", "129", "130", "131", "132", "133", "134", "135", "136", "137", "138", 
        "139", "140", "141", "142", "143", "144", "145", "146", "147", "148", "149", "150", "151", "152", 
        "153", "154", "155", "156", "157", "158", "159", "160", "161", "162", "163", "164", "165", "166", 
        "167", "168", "169", "170", "171", "172", "173", "174", "175", "176", "177", "178", "179", "180", 
        "181", "182", "183", "184", "185", "186", "187", 
        "188", "189", "190", "191", "192", "193", "194", "195", "196", "197", "198", "199", "200"]

    # splitting by npi
    n = split_criteria["npi"]
    λ = Matrix{Complex}(n, 2) # R(n x k)
    ψ = Matrix{Float64}(n, n) # R(n x n)
    psi = Array{Float64, 1}(n)

    # splitting criteria taken for estimation
    splitting_factor = ceil(total_size / n)
    for item = 1:n
        m = Int32(splitting_factor)
        range_1 = Int32(1 + m *(item - 1)) # range_1 = 1246935
        range_2 = Int32(m * item) # range_2 = 1253038
        if total_size <= range_2
            range_2 = total_size
            m = total_size - range_1 + 1
        end
        # calculate the λ vector and X values 
        lambda = transpose(convert(Array, 
            X[range_1:range_2, 
                    [:cost_per_claim, :supply_per_claim]]) - 
            convert(Array, X[range_1:range_2, 
            [:specialty_mean_cost_per_claim, :specialty_mean_supply_per_claim]])) * 
            preprocessing.minmax_scale(convert(Array, 
            X[range_1:range_2, 
                [:drug_weight, :specialty_factor_drug]]
            ), feature_range=(0,1)) # R(2 x 2), equated lambda with x values

        λ[item, 1] = log(complex(lambda[1, 1], lambda[2, 1]) / sqrt(lambda[1, 1] ^ 2 + lambda[2, 1] ^ 2)) # Equivalent to spl factor drug claim
        λ[item, 2] = log(complex(lambda[1, 2], lambda[2, 2]) / sqrt(lambda[1, 2] ^ 2 + lambda[2, 2] ^ 2)) # Equivalent to spl factor drug supply

        # covariance ψ
        psi_cov = cov(convert(Array, X[range_1:range_2, 
            [:specialty_mean_cost_per_claim, 
            :specialty_mean_supply_per_claim]]) + preprocessing.minmax_scale(convert(Array, 
            X[range_1:range_2, 
                [:drug_weight, :specialty_factor_drug]]
            ), feature_range=(0,1)) * transpose(lambda)) # Initial dataset to understand the covariance matrix
        
        psi[item, 1] = psi_cov[2, 1]

        func_value = (-0.5 * 
        (convert(Array, 
            X[range_1:range_2, 
                    [:cost_per_claim, :supply_per_claim]]) - 
            convert(Array, X[range_1:range_2, 
            [:specialty_mean_cost_per_claim, 
            :specialty_mean_supply_per_claim]])) * inv(transpose(lambda) * lambda + psi_cov) * transpose(convert(Array, 
            X[range_1:range_2, 
                    [:cost_per_claim, :supply_per_claim]]) - convert(Array, X[range_1:range_2, 
            [:specialty_mean_cost_per_claim, :specialty_mean_supply_per_claim]])))
        
        file = open(string("Exports\\metaobjects\\cost_quality\\PA\\Health\\Analysis\\Overall_Signal_Covariance_", 
                    values[item], ".csv"), "w")
        likelihood_file = open(string("Exports\\metaobjects\\cost_quality\\PA\\Health\\Likelihood\\Likelihood_", 
                    values[item], ".csv"), "w")
        try
            for func_index = 1:(range_2 - range_1 + 1)
                x_row = string(
                    convert(Array, X[range_1:range_2, 
                            [:npi, :specialty_description, :drug_name]])[func_index, :])
                row = string(sum(func_value[func_index, :]))
                write(file, 
                    x_row[2:length(x_row) - 1] * ", " * row * "\n")
            end
            for func_index = 1:(range_2 - range_1 + 1)
                x_row = string(
                    convert(Array, X[range_1:range_2, 
                            [:npi, :specialty_description, :drug_name]])[func_index, :])
                row = string(sum(log(exp.(func_value[func_index, :]) * 1 / ((2 * pi)^(n/2) * (determinant_supplied)^0.5))))
                write(likelihood_file, 
                    x_row[2:length(x_row) - 1] * "," * row * "\n")
            end
        catch e
            print("Exception")
            throw(e)
        finally
            flush(file)
            close(file)
            flush(likelihood_file)
            close(likelihood_file)
        end
    end

    # Rewriting ψ as the diagonal matrix
    ψ = diagm(psi)
    determinant = norm(λ * transpose(conj(λ)) + ψ, 2)

    Dict("psi" => ψ, "norm_determinant" => determinant)

end
```




    execute_health_likelihood (generic function with 1 method)




```julia
health_object = execute_health_likelihood(health_X, 30000000.0000)
```

    [1m[33mWARNING: [39m[22m[33mlog{T <: Number}(x::AbstractArray{T}) is deprecated, use log.(x) instead.[39m
    Stacktrace:
     [1] [1mdepwarn[22m[22m[1m([22m[22m::String, ::Symbol[1m)[22m[22m at [1m.\deprecated.jl:70[22m[22m
     [2] [1mlog[22m[22m[1m([22m[22m::Array{Float64,1}[1m)[22m[22m at [1m.\deprecated.jl:57[22m[22m
     [3] [1mexecute_health_likelihood[22m[22m[1m([22m[22m::DataFrames.DataFrame, ::Float64[1m)[22m[22m at [1m.\In[71]:87[22m[22m
     [4] [1minclude_string[22m[22m[1m([22m[22m::String, ::String[1m)[22m[22m at [1m.\loading.jl:515[22m[22m
     [5] [1minclude_string[22m[22m[1m([22m[22m::Module, ::String, ::String[1m)[22m[22m at [1mC:\Users\Aswin Vijayakumar\.julia\v0.6\Compat\src\Compat.jl:464[22m[22m
     [6] [1mexecute_request[22m[22m[1m([22m[22m::ZMQ.Socket, ::IJulia.Msg[1m)[22m[22m at [1mC:\Users\Aswin Vijayakumar\.julia\v0.6\IJulia\src\execute_request.jl:154[22m[22m
     [7] [1meventloop[22m[22m[1m([22m[22m::ZMQ.Socket[1m)[22m[22m at [1mC:\Users\Aswin Vijayakumar\.julia\v0.6\IJulia\src\eventloop.jl:8[22m[22m
     [8] [1m(::IJulia.##14#17)[22m[22m[1m([22m[22m[1m)[22m[22m at [1m.\task.jl:335[22m[22m
    while loading In[74], in expression starting on line 1
    




    Dict{String,Any} with 2 entries:
      "psi"              => [-125857.0 0.0 … 0.0 0.0; 0.0 -3.44898e6 … 0.0 0.0; … ;…
      "norm_determinant" => 6.37181e8


