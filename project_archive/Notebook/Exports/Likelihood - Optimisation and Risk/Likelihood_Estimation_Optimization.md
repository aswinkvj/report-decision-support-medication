
Likelihood estimation with parameters: ** z, λ, covariance (ψ), x and μ **

Assuming the likelihiood function has already been formed from the Medicare Part D Claim Data, the likelihood estimation function must be of the form:

** P(x) = N(μ, λλ^T + ψ) **

From investigation, it is found that these are the values by which you should split the dataset for analysis:

Name | NPI | SPECIALTY | DRUG
-- | --- | --------- | -----
Count | 42000 | 100 | 2000

The best split by which the dataset can go is to take n = 200.

Looking at the distributions of the `pa_factors` table:
the attributes - drug weight and specialty factor for drug are noticeable as the drug weight is equal for certain rows and the factor is also equal for less observable rows. 

The bias for evaluating the usage and npi(s) must be obtained by splitting the dataset by "Specialty Factor for Drugs". The split should be done using partions of standard deviations from the mean value. The bias can be measured by taking the ratio of Standard deviation and the Mean of the Quantity - Supply per Claim or Claim per Dollar.

The likelihood estimation in this page shows the signal covariance and the noise covariance from the Gaussian distribution taken from n samples. It is also known that greater the n, greater is the predictability.

For calculation purposes, the Frobenius norm is taken from the covariance_ψ, for evaluation of the Gaussian determinant:
(λλ^T + ψ)


```julia
using MySQL
using PyCall
using Logging
loggerFile = Logger("loggerFile")
@pyimport pickle
@pyimport sklearn.preprocessing as preprocessing
@pyimport csv
file_object = open("logfile.log", "a")
Logging.configure(loggerFile, output=file_object, level=DEBUG)
PICKLE_PATH = "Y:\\Udacity\\Final_Projects\\final\\WORKSPACE\\Serialized\\PA"
using DataFrames
```

    WARNING: Method definition info(Any...) in module Base at util.jl:532 overwritten in module Logging at C:\Users\Aswin Vijayakumar\.julia\v0.6\Logging\src\Logging.jl:115.
    WARNING: Method definition warn(Any...) in module Base at util.jl:585 overwritten in module Logging at C:\Users\Aswin Vijayakumar\.julia\v0.6\Logging\src\Logging.jl:115.
    


```julia
# Calculating the likelihood value for Safety, CD and SC

presc_connection = mysql_connect("localhost", "root", "root", "prescription_aggregates")
info_connection = mysql_connect("localhost", "root", "root", "drug_information")
opt_connection = mysql_connect("localhost", "root", "root", "optimization_tables")
aggregates_connection = mysql_connect("localhost", "root", "root", "drug_aggregates")

split_criteria = Dict("npi" => 200, "specialty" => 1, "drug" => 10) # whichever is the split with largest

function log_likelihood_function(func_value::Float64, determinant, n)
    log(1 / ((2 * pi)^(n/2) * 1 / (determinant)) * (func_value))
end
```




    log_likelihood_function (generic function with 1 method)



This section is calcualting the Likelihood estimation based on Medication Coverage. The feature set used are:
- Drug Cost per claim
- Supply per Claim
- Beneficiary Count
- Claim Count

Preprocessing for Drug Cost and Day Supply has been conducted in order to comply with the lambda values to be generated.


```julia
# Medication Coverage
X = mysql_execute(opt_connection, "SELECT pa_state_spl_drug_npi.npi, 
    pa_cost_function.specialty_description, pa_cost_function.drug_name,
    cost_per_claim, supply_per_claim, 
    pa_cost_function.bene_count, 
    pa_state_wise_specialty.mean_claim_count,
    pa_state_spl_drug_npi.total_claim_count,
    specialty_mean_cost_per_claim, specialty_mean_supply_per_claim,
    pa_state_wise_specialty.mean_bene_count,
    specialty_weight, specialty_factor_drug, drug_factor_specialty
    FROM pa_cost_function
    INNER JOIN drug_information.pa_state_spl_drug_npi ON pa_state_spl_drug_npi.npi = pa_cost_function.npi AND 
    pa_state_spl_drug_npi.drug_name = pa_cost_function.drug_name
    INNER JOIN drug_information.pa_state_wise_specialty ON
    pa_state_wise_specialty.specialty_description = pa_state_spl_drug_npi.specialty_description
    ORDER BY 
    pa_cost_function.prescribers,
    pa_cost_function.npi_claim_count,
    pa_cost_function.npi_day_supply
")
1
```




    1



##### Medication Coverage Likelihood

```sql
SELECT state_wise_drug_state.prescribers, pa_specialty_entropy.drug_count, pa_state_spl_drug_npi.drug_name
FROM pa_state_spl_drug_npi
INNER JOIN drug_aggregates.state_wise_drug_state ON pa_state_spl_drug_npi.drug_name = state_wise_drug_state.drug_name
INNER JOIN claim_aggregates.pa_specialty_entropy ON pa_specialty_entropy.specialty_description = pa_state_spl_drug_npi.specialty_description
```

This is used as the parameter for the medication coverage. The parameter becomes:

Empirical Error Parameter = (Prescribers - Prescribers(Specialty;Drug) / Prescribers) * 
( 1 - (Number of Prescriptions for the Drug / Total Number of Prescriptions for the Specialty) )


```julia
# ORDER BY Parameters(Medication Coverage)

function execute_medication_coverage_likelihood(X::DataFrames.DataFrame, determinant_supplied)
    
    total_size = size(X)[1]

    # splitting by npi
    n = split_criteria["npi"]
    λ = Matrix{Complex{Float64}}(n, 4) # R(n x k)
    ψ = Matrix{Float64}(n, n) # R(n x n)
    psi = Array{Float64, 1}(n)
    
    values = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", 
        "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", 
        "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", 
        "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", 
        "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", 
        "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", 
        "97", "98", "99", "100", "101", "102", "103", "104", "105", "106", "107", "108", "109", "110", 
        "111", "112", "113", "114", "115", "116", "117", "118", "119", "120", "121", "122", "123", "124", 
        "125", "126", "127", "128", "129", "130", "131", "132", "133", "134", "135", "136", "137", "138", 
        "139", "140", "141", "142", "143", "144", "145", "146", "147", "148", "149", "150", "151", "152", 
        "153", "154", "155", "156", "157", "158", "159", "160", "161", "162", "163", "164", "165", "166", 
        "167", "168", "169", "170", "171", "172", "173", "174", "175", "176", "177", "178", "179", "180", 
        "181", "182", "183", "184", "185", "186", "187", 
        "188", "189", "190", "191", "192", "193", "194", "195", "196", "197", "198", "199", "200"]
    # splitting criteria taken for estimation
    splitting_factor = ceil(1253038 / n)
        
    for item = 1:1
        m = Int32(splitting_factor)
        range_1 = Int32(1 + m *(item - 1)) # range_1 = 1246935
        range_2 = Int32(m * item) # range_2 = 1253038
        if total_size <= range_2
            range_2 = total_size
            m = 1253038 - range_1 + 1
        end
        
        # calculating the z vector for likelihood
        k = range_2 - range_1 + 1
        z = Matrix{Float64}(k, 4)
        for seq = 1:k
            prescribers = mysql_execute(aggregates_connection, 
            "SELECT prescribers FROM state_wise_drug_state WHERE drug_name = '" * X[range_1 + seq - 1, :drug_name] * "'")
            pres_spl_drug = mysql_execute(info_connection, "SELECT COUNT(npi) as npi FROM pa_state_spl_drug_npi
                WHERE specialty_description = '" * X[range_1 + seq - 1, :specialty_description] * 
                "' AND drug_name = '" * X[range_1 + seq - 1, :drug_name] * "'")
            pres_spl = mysql_execute(info_connection, "SELECT COUNT(npi) as npi FROM pa_state_spl_drug_npi
                WHERE specialty_description = '" * X[range_1 + seq - 1, :specialty_description] * "'")
            empirical_parameter = 
            abs(prescribers[1, :prescribers] - pres_spl_drug[1, :npi]) / prescribers[1, :prescribers] * 
                (1 - pres_spl_drug[1, :npi] / pres_spl[1, :npi])
            
            suitability_weight = 
            X[range_1 + seq - 1, :bene_count] / X[range_1 + seq - 1, :total_claim_count]
            
            z[seq, 1] = X[range_1 + seq - 1, :specialty_weight]
            z[seq, 2] = X[range_1 + seq - 1, :specialty_factor_drug]
            z[seq, 3] = X[range_1 + seq - 1, :drug_factor_specialty] * empirical_parameter
            z[seq, 4] = X[range_1 + seq - 1, :specialty_factor_drug] * suitability_weight
        end
        
        # calculate the λ vector and X values 
        # Assuming the likelihood function has already been formed from the Medicare Claims data
        lambda = transpose((convert(Array, 
            X[range_1:range_2, 
                    [:cost_per_claim, :supply_per_claim, :bene_count, :total_claim_count]]) - 
            convert(Array, X[range_1:range_2, 
            [:specialty_mean_cost_per_claim, :specialty_mean_supply_per_claim, 
            :mean_bene_count, :mean_claim_count]]))) * 
            preprocessing.scale(convert(Array, z)) # R(4 x 4), equated lambda with x values

        λ[item, 1] = log(complex(lambda[1:2, 1][1], lambda[1:2, 1][2]) 
            / sqrt(lambda[1:2, 1][1] ^ 2 + lambda[1:2, 1][2] ^ 2)) # Equivalent to spl factor drug claim
        λ[item, 2] = log(complex(lambda[1:2, 2][1], lambda[1:2, 2][2]) 
            / sqrt(lambda[1:2, 2][1] ^ 2 + lambda[1:2, 2][2] ^ 2)) # Equivalent to spl factor drug supply
        λ[item, 3] = log(complex(lambda[3:4, 1][1], lambda[3:4, 1][2]) 
            / sqrt(lambda[3:4, 1][1] ^ 2 + lambda[3:4, 1][2] ^ 2)) # Equivalent to spl factor drug supply
        λ[item, 2] = log(complex(lambda[3:4, 2][1], lambda[3:4, 2][2]) 
            / sqrt(lambda[3:4, 2][1] ^ 2 + lambda[3:4, 2][2] ^ 2)) # Equivalent to spl factor drug supply
        
        # covariance ψ
        psi_cov = cov(convert(Array, 
            X[range_1:range_2, [:specialty_mean_cost_per_claim, :specialty_mean_supply_per_claim, 
                            :mean_bene_count, :mean_claim_count]])
            + preprocessing.minmax_scale(convert(Array, 
            X[range_1:range_2, 
                    [:specialty_weight, :specialty_factor_drug, :w_drug_factor_specialty, 
                        :w_specialty_factor_drug]]), feature_range=(-1, 1))
            * lambda) # Initial dataset to understand the covariance matrix
        
        psi[item, 1] = psi_cov[1, 2]

        func_value = (-0.5 * 
        (convert(Array, 
            X[range_1:range_2, 
                     [:cost_per_claim, :supply_per_claim, :bene_count, :total_claim_count]]) - 
            convert(Array, 
            X[range_1:range_2, 
                    [:specialty_mean_cost_per_claim, :specialty_mean_supply_per_claim, 
                            :mean_bene_count, :mean_claim_count]])) * 
        inv(lambda * transpose(lambda) + psi_cov) * transpose(convert(Array, 
            X[range_1:range_2, 
                     [:cost_per_claim, :supply_per_claim, :bene_count, :total_claim_count]]) - 
            convert(Array, 
            X[range_1:range_2, 
                    [:specialty_mean_cost_per_claim, :specialty_mean_supply_per_claim, 
                            :mean_bene_count, :mean_claim_count]])))
        
    end
    
    # Rewriting ψ as the diagonal matrix
    ψ = diagm(psi)
    determinant = norm(λ * transpose(conj(λ)) + ψ, 2)

    Dict("psi" => ψ, "norm_determinant" => determinant)
    
end
```




    execute_medication_coverage_likelihood (generic function with 1 method)




```julia
determinant_supplied = 44521.01497670779
safety_object = execute_medication_coverage_likelihood(X, determinant_supplied)
```

##### Cost and Quality

The congitive bias problem has been treduced to the following problem where the ratio between cost per claim; and supply per fill has been considered.

```sql
SELECT (drug_cost / total_claim_count) / (total_day_supply / fill_count), npi FROM 
pa_state_spl_drug_npi
WHERE npi = '{0}' AND specialty_description = '{1}' AND drug_name = '{2}'
ORDER BY (drug_cost / total_claim_count) / (total_day_supply / fill_count)
LIMIT 0,1
```

```sql
SELECT SUM(drug_cost / total_claim_count) / (total_day_supply / fill_count) FROM
pa_state_spl_drug_npi
GROUP BY drug_name
```

The suitability weight has been considered as:
(Fill Count - Claim Count) / Fill Count
