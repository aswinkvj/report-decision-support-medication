
Less Visibility | Greater Visibility
------------ | -------------
Evidence Based | Risk Aware
Reasoning | Risk Assessor
________________________
- From the GRC definitions,

People, Data, Process and technology | People, Data, Process and technology
------- | --------
Care | Drug towards Care
Patient towards Care | Specialty towards Care
________________________
- Considering the frequent Information Exchange between Purpose, Usage and Cost

Characteristics | Contributing Factors
---------------- | --------------------
** Purpose ** | Medication Type, Confidence Level for Prescriptions
** Usage ** | Claim per DOLLAR (CD), Supply per Claim (SC)
** Cost ** | Cost per Claim (CC), Mean Cost per Claim
________________________
- From the parameters obtained from the `cost_quality_table` schema,

Parameter | Problem
-------- | -------
Claim per DOLLAR (CD) | Patient towards Care
Supply per Claim (SC) | Care only
Cost per Claim (CC) | Drug and Specialty towards Care
________________________
- Finding the realms that are useful to prescriptions,

Title | Claim per DOLLAR | Supply per Claim | Cost per Claim
--  | ---------------- | ---------------- | --------------
Claim per Dollar |   | ** OPTIMISED CLAIMS / DOSAGES ** | ** PRICING FACTOR **
Supply per Claim |   |   | ** DRUG FACILITY **
Cost per Claim |   |   |   
________________________
- From the Factor Analysis algorithm that are required to determine the Influence of Drugs and Specialties for an evaluation of Cost and Quality

Name | Safety | Health
----- | ------  | ------
Drug Influence | ** Drug Factor for Specialty ** | ** Drug Weight **
Specialty Influence | ** Specialty Weight ** | ** Specialty Factor for Drug **
