## Capstone Project                                                 

### Aswin Vijayakumar

### Machine Learning Engineer Nanodegree                            
### October 21, 2017

## An augmented Naive Bayes approach of decision support for prescribing medications ##

### Definition

Project Overview
----------------

The capstone proposal discusses about the paper on Patient Safety and Facilities within a hospital environment. The project achieves clarification of concepts used in the monitoring of the drugs supporting a check for recall. On a scale which is derived from the different types of Health Risks, the project enables the definition of a coverage factor required by the drug administration team using a likelihood estimation.

There is a new dataset related to each US state that shows a Rate and Range for Death Data corresponding to the Deaths and Population. This is useful for categorizing the US states based on a decision model such that the project makes use of these attributes when assessing a State:

    1. Risk Aware
    2. Risk Assessor
    3. Evidence Based Analysis
    4. Reasoning

The project makes use of complete list of 24 million prescriptions organised by US cities with highlights from the state of 'Pennsylvania' obtained from [http://cms.gov](http://cms.gov) provided within [Kaggle](http://www.kaggle.com). The source of the dataset is located at: 
[Medicare-Provider-Charge-Data/PartD2015.html](https://www.cms.gov/Research-Statistics-Data-and-Systems/Statistics-Trends-and-Reports/Medicare-Provider-Charge-Data/PartD2015.html)

The tabbed format of the master data is available at, termed as `backup_table_metadata` within `backup_table_metadata` and as `table_metadata` within `drug_information` database (Contains two copies, one is backup):
[PartD_Prescriber_PUF_NPI_DRUG_15.zip](http://download.cms.gov/Research-Statistics-Data-and-Systems/Statistics-Trends-and-Reports/Medicare-Provider-Charge-Data/Downloads/PartD_Prescriber_PUF_NPI_DRUG_15.zip)

The drug category list is available at, termed as `drug_categories` within `drug_aggregates` database:
[PartD_DrugCatList_2015.zip](https://www.cms.gov/Research-Statistics-Data-and-Systems/Statistics-Trends-and-Reports/Medicare-Provider-Charge-Data/Downloads/PartD_DrugCatList_2015.zip)

The summary of the prescriptions, the data mart table, are available at, termed as `table_summary` in the schema within `prescription_aggregates` database:
[PartD_Prescriber_PUF_NPI_15.zip](http://download.cms.gov/Research-Statistics-Data-and-Systems/Statistics-Trends-and-Reports/Medicare-Provider-Charge-Data/Downloads/PartD_Prescriber_PUF_NPI_15.zip)

The drug national summary table located at, termed as `drug_national_summary` within `drug_aggregates` database:
[PartD_Prescriber_PUF_Drug_Ntl_15.zip](http://download.cms.gov/Research-Statistics-Data-and-Systems/Statistics-Trends-and-Reports/Medicare-Provider-Charge-Data/Downloads/PartD_Prescriber_PUF_Drug_Ntl_15.zip)

The state-wise drug state located at, termed as `state_wise_drug_state` within `drug_aggregates` database:
[PartD_Prescriber_PUF_Drug_St_15.zip](http://download.cms.gov/Research-Statistics-Data-and-Systems/Statistics-Trends-and-Reports/Medicare-Provider-Charge-Data/Downloads/PartD_Prescriber_PUF_Drug_St_15.zip)

##### Potential sources of data used for trawling of information (not used directly in the project)

The sources for enrolees and utilizers for states and grand totals are available at:
[Medicare_Part_D_Enrollees_and_Utilizers_by_State_CY2015.zip](https://www.cms.gov/Research-Statistics-Data-and-Systems/Statistics-Trends-and-Reports/Medicare-Provider-Charge-Data/Downloads/Medicare_Part_D_Enrollees_and_Utilizers_by_State_CY2015.zip)

[Part_D_Totals_Avgs_2015.zip](https://www.cms.gov/Research-Statistics-Data-and-Systems/Statistics-Trends-and-Reports/Medicare-Provider-Charge-Data/Downloads/Part_D_Totals_Avgs_2015.zip)

Relevant information has been obtained regarding Drug categories, State wise drug listings to compare the health risks and a Summary table to detail the Specialties and their Average Beneficiary Risk Score. The motivation of the project is the improper Scheduling and facilities to make changes to a hospital environment affecting the patients' safety and happiness.

Problem Statement
-----------------

The problem presented here is about Health and Safety and the solution is to improve health by better drug administration and provide enough safety precautions towards preventing adverse effects. The prescription domain consists of Specialties, Drugs, States; and attributes such as Claims, Drug Costs, Supply and Beneficiary Risk. The measurement of likelihood of the new prescriptions indicates a likelihood value of prescribing the specialty for the drug and a recommended specialty for the patient. But in the case of a clinic or a hospital, the measurement of expectation of the new prescriptions indicate whether the specialty and drug combination is to be recalled or whether the coverage for the drug among the NPI(s) has been met so that patients could be benefitted.

It is found that the specialties in the Part D Medicare Claim Data are Opioid Prescribers for the year 2015. Health and Safety are two facets of a Healthcare Plan such as Medicare Part D. Its components are: Patient, Care, Drug and Prescriber / Specialty. 

```sql
SELECT * FROM table_metadata 
WHERE nppes_provider_state = 'PA'
AND (CAST(total_claim_count AS UNSIGNED) < 10 
OR total_claim_count = '' OR total_claim_count = 'NA')
AND (CAST(bene_count AS UNSIGNED) < 10 
OR bene_count = '' OR bene_count = 'NA')
LIMIT 0,10
```

    - The criteria for not being an Opioid Prescriber is:
    (bene_count < 10 OR bene_count = 'NA') AND 
    (total_claim_count < 10 OR total_claim_count = 'NA')

The SQL code provided above provides an analysis conducted on the dataset to find the list of Opioid Prescribers based on the formulation.

***Obtained from the create-dataset.R file from [Kaggle: US Opiate Prescriptions](https://www.kaggle.com/apryor6/us-opiate-prescriptions) dataset***

The problem is surrounded by three cyclic components: 
- Purpose,
- Usage, and
- Cost

> Purpose, Usage and Cost

![Purpose, Usage and Cost](http://localhost:3000/Images/Purpose_Usage_Cost.jpg)

It is the Usage and Cost that are the prime interests in this project for evaluating Health and Safety. While the Purpose is consisting of: the solution to health problem such as the medications, and the confidence level with which the Specialties prescribe those medicines; the Usage and Costs are relevant to the Drug used, the NPI, the State and the dataset values such as Claim Count per Dollar, Supply per Claim and Cost per Claim.
- Claim per Dollar = Total Claim Count / Total Drug Cost
- Supply per Claim = Total Day's Supply / Total Claim Count
- Cost per Claim = Total Drug Cost / Total Claim Count

The full dataset has been obtained from [Medicare Provider Utilization and Payment Data: Part D Prescriber](https://www.cms.gov/Research-Statistics-Data-and-Systems/Statistics-Trends-and-Reports/Medicare-Provider-Charge-Data/Part-D-Prescriber.html)

A reference data for the drugs and specialties involving the drug categories are provided with the Medicare Part D dataset; a master data table containing NPI datarows helps in distinguishing between a drug and a specialty; a data mart table for each state and specialty provides the summarised data corresponding to the risk types that each drug prescription handles. Partitions of the master data table by each State are created, which summarises a Cost and Quality table which consists of the three dataset values described above and Standardised Fill Count of the Drug. The table also provides insights on Factor data that is relevant for Factor analysis, such a factor data consists of:
1. Specialty Weight
2. Drug weight
3. Specialty Factor for Drugs
4. Drug Factor for Specialty

The vectors relevant to their use in the Project is mentioned as a table below:

| Name                | Safety                          | Health                          |
| ------------------- | ------------------------------- | ------------------------------- |
| Drug Influence      | ** Drug Factor for Specialty ** | ** Drug Weight **               |
| Specialty Influence | ** Specialty Weight **          | ** Specialty Factor for Drug ** |

Calculating the Signal Covariance and the Noise Covariance in the Factor Analysis Problem are important in comparison to the Likelihood Estimation from the Medicare Data that is revealed in the later sections. The data for the Original Likelihood is split and ordered using:
- a trawling method from Gradient Boosting regressor for Feature selection
- a bottom to top approach of finding the best feature set
- a convenient value of `m` training samples for each `n` batches of records is taken for each value of the dimensional entities

The specialty weights, drug weights, and the other two factors are calculated using the formula mentioned below: ***(The project has assumed the role of Data Stewards to form the formulaic metrics suitable for decision making)***

```mathematica
Specialty Weight = Weight * 
( Chebychev's Prob(Outliers such that 
| X - Mu | < k * Sigma) *
Prob(Claim Count > Median | Specialty ) )
```

```mathematica
Drug Weight = Weight * 
( 1 / tanh(Claim Count OVER Health Risk 
AGGREGATED OVER NPIs) ) * 
Entropy(Health Risk OVER Claim Count 
AGGREGATED OVER NPIs)
```

```mathematica
Drug Factor for Spl = NPI COUNT x 
COST_PER_CLAIM x SUPPLY_PER_CLAIM *
( DRUG_REDUCTION_VALUE x 
Prob( Standard Fill Count > Claim Count ) + 
CLAIM_PER_DOLLAR x Prob(Prescriber == 
Opioid.Prescriber) ) # usually this probability is 1
```

```mathematica
Spl Factor for Drug = DRUG COUNT * SUPPLY_PER_CLAIM * 
( NPI_COUNT x Prob(Claim is a Beneficiary) + 
DRUG_WEIGHT * AVG(Beneficiary Risk Scores OVER NPIs) )
```

The first two factors are independent Factor Variables which has a one-to-one mapping to their specialties and drugs, and the next two are dependent Latent variables which has a one-to-one mapping to their prescriptions. The first two are purely probabilistic and the next two are based on unit values and probability involved with prescribing medications. 

The likelihood estimation based on factor analysis from the splitting of the data reveals how likely are the prescriptions recommended for use. It is important to understand the Factor and Latent vectors/variables corresponding to the following table:

| Name   | Factor Variables       | Latent Variables                |
| ------ | ---------------------- | ------------------------------- |
| Safety | ** Specialty weight ** | ** Drug Factor for Specialty ** |
| Health | ** Drug Weight **      | ** Specialty Factor for Drug ** |

The Stochastic Gradient Descent Classifier Algorithm run on Specialty Weights formed from the formulaic metric, reveals that the score of the algorithm is around 0.330464646465. A attribute based Trawling method using an ensemble method, Gradient Boosting regression has been applied to the Specialty Weight which reveals that the score is maximised when the SQL queries are in the Trawling mode. This implies that the non-inclusion of Specialty Weight and inclusion of Specialty Weight and Drug Weight together within the feature set has provided the maximum score. The appropriate parameters referred as `Intangible values` (the inequality filters) for the Gradient Boosting Regressor has been found to be: (from the Trawling method)
- NPI Count
- Drug Count
- Day Supply

From this inference, the comparison attributes assigned to the Lambda Vector (Safety or Health) has been deduced as these attributes: 
- Supply per Claim
- Ratio of the Specialty Std and Mean of Unit Claim (For, Solution for OPTIMISED CLAIM / DOSES)
- Ratio of the Drug Mean and Std of Supply per Claim (For, Solution for DRUG FACILITY)
- Termed as Health-wise Risk Ratio, Ratio of Total Claim Count and Drug Specific Claim Count 
  (For, Specialty Factor For Drug)
- Beneficiary Risk (For, Drug Factor for Specialty)

##### Reference Table for Inferences to Gradient Boosting Attribute Trawling

| Rule 1                        | Rule 2            | Rule 3                         | Rule 4            | Rule 5                         | Rule 6                       | Rule 7                       |
| ----------------------------- | ----------------- | ------------------------------ | ----------------- | ------------------------------ | ---------------------------- | ---------------------------- |
| NPI Count                     | NPI  Count        | NPI  Count                     | NPI  Count        | NPI  Count                     | NPI  Count                   |                              |
| Std / Mean (Supply per Claim) |                   | Std  / Mean (Supply per Claim) |                   | Std  / Mean (Supply per Claim) |                              |                              |
| Claim per Dollar              | Claim  per Dollar |                                |                   | Claim  per Dollar              |                              |                              |
|                               | Supply  per Claim | Supply  per Claim              | Supply  per Claim | Supply  per Claim              |                              |                              |
| Day supply                    |                   |                                | Day  Supply       |                                |                              | Day  Supply                  |
|                               | Drug  Cost        | Drug  Cost                     |                   |                                | Drug  Cost                   |                              |
|                               | Drug  Count       | Drug  Count                    |                   | Drug  Count                    | Drug  Count                  |                              |
| Mean / Std (Cost per Claim)   |                   | Mean  / Std (Cost per Claim)   |                   | Mean  / Std (Cost per Claim)   | Mean  / Std (Cost per Claim) | Mean  / Std (Cost per Claim) |

[Gradient Boosting Feature Selection or Trawling Using the Decision Path Log file](http://localhost:3000/Logs/Feature_Selection/Cost_Function_FullDataset.log_(Backup).txt)

- The table shows the occurrences of the attributes within the log file of decision path presented here
- The Log file on the Decision Path has been provided for detailed view 

The KMeans Clustering Algorithm run on the "Drug Factor For Specialty" and "Specialty Factor For Drugs", reveals the cluster information for the datasets chosen as a 10th Fraction of the Full Dataset obtained from the State of Pennsylvania. The domain model used in the classification problem has been presented here:

> Classification Domain Model

![Classification Domain Model](http://localhost:3000/Images/Model/Classification.png)

#### Solution

In Health, when the hypothesis takes control of Cost per Claim and Supply per Claim, the solution leads to the topic presented in Capstone Proposal - `OPTIMISED MEDICATION DOSAGES`.

In Safety, when the hypothesis takes control of Claim per Dollar and Supply per Claim, the solution leads to the `EVALUATION OF DRUG FACILITY`. 

Optimisation of Medication Dosages involves taking control of the drugs recalls by the effective monitoring of the drug prescriptions. The optimisation of medication dosages is possible by changing the sequence of specialties. Given table below explains the above attribute of Health and Safety presented.

| Title            | Claim per DOLLAR | Supply per Claim                 | Cost per Claim                    |
| ---------------- | ---------------- | -------------------------------- | --------------------------------- |
| Claim per Dollar |                  | ** OPTIMISED CLAIMS / DOSAGES ** | ** PRICING FACTOR **              |
| Supply per Claim |                  |                                  | ** EVALUATION OF DRUG FACILITY ** |
| Cost per Claim   |                  |                                  |                                   |

Coverage of prescriptions by an NPI facilitates the `Evaluation of Drug Facility` within a State. With the factors obtained from z and λ, the 3 attributes (Claim per Dollar, Supply per Claim and Cost per Claim) can be regulated based on the need of prescriptions only to improve the coverage of prescriptions.

Metrics
-------

The metrics for the Gradient Boosting regressor are: 
    - Gradient Boosting Regressor R2 Score
    - Mean Square Error
    - Mean Absolute Error
    - Decision Path eliciting the Regressor Rule Constraints

The R2 score indicates the relevance of the regressor with the dataset and the decision path indicates the relevant attributes required for Analysis. 

The metrics for the K-Means Clustering Algorithm are:
    - Labels / Cluster Number
    - Intertia Score of the Labels within each Cluster, the algorithmic score

The labels or cluster number indicates the cluster to which the Expectation-Maximisation of K-means has assigned, the Specialty factor, Drug factor, Specialty weight or Drug weight. The inertia value is an overall value indicating the norm of the data points with respect to the cluster centroids.

The Stochastic Gradient Descent Regressor has identified the coefficients and intercept which when fitted with the curve has provided an underfitting problem. The score of the algorithm is negative, MSE (mean square error) is a huge number of the order of 41st exponent indicating we need a decision tree classifier. The Learning Curve of the Gradient Boosting Regressor Algorithm converges at 18,000 training points for all Maximum depths considered, 1, 3, 6, 10. The complexity curve of the Gradient Boosting regressor is as close to 1.0, nearly 0.98.

Since the rule constraints were useful to determine the relevant attributes, the problem of finding the Order Number and the Cluster Number, when the prescription enters the database, has been identified.

The Measure or Metric for investigating the fitting factor for a prescription entry towards a specified batch and cluster are:
    - A Measure of Deviance, or Entropy
    - A Measure of Minimum Mean Square Estimation (MMSE)
    - The formulaic metric of 4 Factors

MMSE for two samples has revealed values which are greater or lesser than the Mean Square Estimation value depending on the local optima. The measure of deviance is varying for a prescription when multiple sets of clusters and batches are investigated.

An observation of how far the new prescription entry is from the related points is measured using the Canberra distance. This distance metric provides a good estimate of Nearest Neighbours within the relevant cluster. Canberra distance is applied to the observations taken from experiments. In the calculation of latent variables such as: Drug Factor for Specialty, the ratio of Standard deviation and Mean is an observation in the distribution.

Metrics for finding the closeness of prescription values are:
    - Canberra Distance
    - Classification Cost

The estimation of likelihood is conducted by calculating the Lambda vector for Safety and Health realms as well as by Medication Coverage and Cost and Quality domains. The evaluation of Z vector involves calculation of naive bayes parameters such as empirical error and cognitive bias. It can be deduced that greater the empirical error, lesser the cognitive bias, and lesser the likelihood of Cost and Quality, the possibility of a check for recall is higher. Similarly the lesser the empirical error, lesser the cognitive bias and greater the likelihood of Medication Coverage, the possibility for the coverage of the drug within the specialties are higher. The maximisation of likelihood is out of scope of the project.

Metrics for measuring the Availability of Specialty for Care and the Specialty's Awareness of Drugs towards Care are:
    - Accuracy Score
    - Precision Score
    - Confusion Matrix for Awareness
    - Cohen's Kappa Score for Availability

Whereas the Distance Measure is applied to observations, the Classification Cost derived from mutual information is applied to the Clusters. The minimum mean square estimation (MMSE) which minimises the MMSE quantity that is calculated using the Bayesian Inference of the selected prescription, is applied to the batches of data, implying the entire dataset.

The specialties from the 6 categories of Specialty Description require frequent monitoring of adverse effects of drugs. The optimization by reducing the classification cost and the deviance will reveal several results based on the process conducted across the batches and across the clusters. If a new clinic or hospital is to be setup in the state of 'Pennsylvania', and the clinic requires more coverage of drugs (effective drug availability) across the batches and clusters, then the right way is to optimize the functor which returns the measure of deviance as the cost. Maximising the deviance provides the clinic effective coverage. The likelihood of the selected prescriptions can be measured from the Medication Coverage Likelihood Estimation. 

Another case is when the clinic or a healthcare requires very less clances of recall of drug-specialty combination due to their infrastructure or quality considerations. In this case the optimizer runs across the batches and clusters to obtain the prescriptions with least classification cost and the least deviance, and filter thise porescription entries based on the Cost and Quality Likelihood estimation.

If we consider `k` prescriptions in `m` batches and `n` clusters, 

the number of iterations would be:

![Iterations](http://localhost:3000/Images/Equations/optimization_iterations.png)

The order would dependent on search within Specialty Categories and Drug Categories:

![Order](http://localhost:3000/Images/Equations/optimization_order.png)

Analysis
--------

### Data Exploration

#### Prescribing Data and Death Data

By questioning the Usage and Cost, the parameters on the Purpose cycle can be drilled down into. One example has been formed with the help of Information required for Evaluating the Factor Analysis Problem. This experimentation has been conducted based on finding the parameters for evaluating the likelihood using an Excel Workbook. The experimentation resulted in some of the metrics provided in the APPENDIX such as: 
- A Pricing Factor for the Dollar
- A Multivariate Log normal in the calculation of λλ^T + ψ
- Several factors that were dependent on the weights of Specialty and Drugs

A data sample of the analysis table on the US State of 'Pennsylvania' has been provided in the Appendix.

The 2015 death data due to Drug Overdose was obtained from [State Deaths: 2015](https://www.cdc.gov/drugoverdose/data/statedeaths.html). The data gives a distribution of population for the 50 US states and a distribution of deaths. The data on Death Number provide a significant gap from Florida and Ohio compared with California, implying a split based on Death Number is a feasible rule constraint when a Decision Tree is built. 

The file at [Worksheets\overdoses_copy.xlsx](http://localhost:3000/Worksheets/overdoses_copy.xlsx) contains a manually classified overdoses file for splitting the death data in each US state. The decision tree has been provided at: 

![Decision Tree for Deaths and Population](http://localhost:3000/Images/Decision_Tree_Death.png)

The initial analysis on the data involved: these 4 facets of Risk Evaluation.

| Less Visibility | Greater Visibility |
| --------------- | ------------------ |
| Evidence Based  | Risk Aware         |
| Reasoning       | Risk Assessor      |

Risk Aware maps to the US States with High Death Ratio and High Death Number compared with the Population. The US States that require an analysis based on Risk Assessor method is categorised into High Death Ratio and Low Death Number. A schema of the NPI Prescribers for the State of Pennsylvania has been provided below:

```sql
-- Dumping database structure for drug_information
CREATE DATABASE IF NOT EXISTS `drug_information` 
/*!40100 DEFAULT CHARACTER SET utf8 */;
USE `drug_information`;

-- Dumping structure for table drug_information.pa_state_spl_drug_npi
CREATE TABLE IF NOT EXISTS `pa_state_spl_drug_npi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `npi` varchar(200) NOT NULL,
  `nppes_provider_state` varchar(50) NOT NULL,
  `specialty_description` varchar(255) NOT NULL,
  `drug_name` varchar(200) NOT NULL,
  `total_claim_count` int(11) NOT NULL DEFAULT '0',
  `bene_count` int(11) NOT NULL DEFAULT '0',
  `opioid_prescriber` tinyint(1) NOT NULL,
  `total_day_supply` double NOT NULL DEFAULT '0',
  `drug_cost` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `npi` (`npi`),
  KEY `drug_name` (`drug_name`),
  KEY `specialty_description` (`specialty_description`),
  KEY `opioid_prescriber` (`opioid_prescriber`),
  KEY `total_claim_count` (`total_claim_count`),
  KEY `bene_count` (`bene_count`)
) ENGINE=InnoDB AUTO_INCREMENT=1241152 DEFAULT 
CHARSET=utf8;
```

The remaining facets are: Evidence Based and Reasoning. The Reasoning decision facet has considered Population less than 15 million and death ratio less than 1.20, implying the analysis on these states must be done based on the reasoning for "Why the prescribing rate is bearing a value between 45 - 59 for US States under Evidence Based". The results of analysis produced:
- Range of (45.1, 47.7 and 59.8) for States under Evidence Based decision facet,
- Range between 44.3 and 110.9 for States under Reasoning decision facet,
- Range between 60.1 and 125 for States under Risk Assessor decision facet, and
- Range between 54 and 114.9 for States under Risk Aware decision facet

Interesting quantities obtained from the Decision Tree Classifier of Death Number and Population with State data are:
- The Death Ratio, and
- The Variance of Death Number

The decision tree classifier reveals a split by death ratio in the initial node, and then a rule filter by Death Numbers at around 500 to 700 and then split by death ratio. Eventhough, the split was not exactly the same, it was possible to effectively map the quantities closer to the expectations. A kernel density estimation plot based on Gaussian Model have been provided below.

> Kernel Density Estimation Plot

![Kernel Density Estimation Plot](http://localhost:3000/Images/Death_Evaluation/kde_death_ratio.png)

> Joint Plot between Death Ratio and Variance of Death Number, termed as Sigh Value; and the Death Number

![Joint Plot between Death Ratio and Variance of Death Number, termed as Sigh Value; and the Death Number](http://localhost:3000/Images/Death_Evaluation/joint_plot_death.png)

A dataset that is close to the requirement for detailed analysis done here is the dataset about the Death Number and Age adjusted rate for US States, indicating the change in rate as well, causing due to the prescription of "Natural and Semi-Synthetic Opioids and Methadone" and "Synthetic Opioids other than Methadone". The datasets have been provided below for reference.

> [Death due to Natural and Semi-Synthetic Opioid Intake / Overdose](http://localhost:3000/Datasets/Natural_Semisynthetic_Death_Rate_Prescribing_Rate_map.csv)

> [Death due to Synthetic Opioids other than Methadone](http://localhost:3000/Datasets/Synthetic_Death_Rate_Prescribing_Rate_map.csv)

#### Information Requirements for Factor Analysis

The analysis in this section is based on the Information Architecture and Reference Tables provided for evaluating Health and Safety vectors. The health and safety vectors as described earlier are dependent only on the Specialty and the Drug. The project depends on the role of Data Stewards who are qualified to enter the formulaic metrics on a routine basis such that the KMeans Cluster integrated within the proposed system can make decisions on the Specialty and the Drug. The purpose is to optimise the KMeans Clustering results and the values for the Gaussian Signal Covariance in the Log Likelihood Estimation. Provided below is the graphic on the Four Factors that have been analysed.

> Kernel Density Estimation plot of Specialty Weights

![Kernel Density Estimation plot of Specialty Weights](http://localhost:3000/Images/4_Factors/kde_specialty_grid_weights.png)

> Joint Plot between Specialty Weights and 
> Standard Deviation of the Unit Claim Cost of the Drug 
> that each Specialty is dealing with

![Joint Plot between Specialty Weights and Standard Deviation of the Unit Claim Cost of the Drug that each Specialty is dealing with](http://localhost:3000/Images/4_Factors/joint_plot_specialty_grid_vs_drug_cost.png)

    - Joint Plot between Specialty Weights and Standard Deviation of 
    the Unit Claim Cost of the Drug that each Specialty is dealing with
    - The Joint plot takes into consideration, an 
    exponential distribution of the Total Drug Cost 
    for a Specialty and a Drug Combination

In the database tables, the ratio of STDDEV / AVG for quantities such as Cost Per Claim of Specialties was found to be increasing for datasets with greater Supply and Cost. Since the usual standardisation of μ, σ distributions was not possible, the bias due to the large unit values was negated by taking the ratio of (σ / μ). For drug specific tables, the ratio of μ / σ is feasible to consider for an experiment such as: "Finding the Cluster of the Prescription using Order Number".

```sql
SELECT specialty_description, specialty_weight, 
specialty_mean_claim_per_dollar, specialty_mean_supply_per_claim,
specialty_std_cost_per_claim, specialty_std_claim_per_dollar, 
specialty_std_supply_per_claim,
pa_cost_quality_table.total_drug_cost
pa_specialty_entropy.median_count, 
pa_specailty_entropy.npi_count
FROM pa_cost_function
INNER JOIN pa_specialty_entropy ON 
pa_specialty_entropy.specialty_description = 
pa_cost_function.specialty_description;
INNER JOIN pa_cost_quality_table ON
pa_cost_quality_table .specialty_description = 
pa_cost_function.specialty_description
```

This SQL code is provided to show the consistency with the Trawling method of Gradient Boosting Regression conducted in the Problem Statement section. From the Trawling method, it is found that the Order Parameters must be either the ratio of AVG / STDDEV of Supply per Claim or STDDEV / AVG of Unit Claim depending on the entity and the Use Case; and of Cost per Claim otherwise. The graphs plotted show the density estimation of the Qualitative analysis of Specialty Weights in the Safety Grid compared to the Quantitative analysis of the Cost in the Health Grid. Viceversa, the Drug weights and the Unit Cost Claim of the Drug can be plotted.

The comparison provided below is useful as it outweighs the Health and Safety parameters defined by the Usage and Cost described in the Problem Statement and the Solution sections. 

> kde Plot for Drug Factor for Specialties

![kde Plot for Drug Factor for Specialties](http://localhost:3000/Images/4_Factors/kde_drug_factor_spl.png)

    - The kde plot is used to understand the 
    latent variable, Drug Factor for Specialties

> kde plot for Standard Deviation Cost Per Claim

![kde plot for Standard Deviation Cost Per Claim](http://localhost:3000/Images/4_Factors/kde_std_cost_per_claim.png)

    - The kde plot here shows the distribution 
    of Standard Deviation Cost per Claim in comparison 
    with the Drug Factor for Specialties

```sql
-- Dumping database structure for prescription_aggregates
CREATE DATABASE IF NOT EXISTS `prescription_aggregates` 
/*!40100 DEFAULT CHARACTER SET utf8 */;
USE `prescription_aggregates`;

-- Dumping structure for table prescription_aggregates.table_summary
CREATE TABLE IF NOT EXISTS `table_summary` (
  `npi` varchar(50) NOT NULL,
  `nppes_provider_last_org_name` varchar(200) NOT NULL,
  `nppes_provider_first_name` varchar(200) NOT NULL,
  `nppes_provider_mi` varchar(5) NOT NULL DEFAULT '',
  `nppes_provider_credentials` varchar(50) NOT NULL DEFAULT '',
  `nppes_provider_gender` varchar(3) NOT NULL DEFAULT '',
  `nppes_entity_code` varchar(3) NOT NULL DEFAULT '',
  `nppes_provider_street1` varchar(200) NOT NULL DEFAULT '',
  `nppes_provider_street2` varchar(200) NOT NULL DEFAULT '',
  `nppes_provider_city` varchar(100) NOT NULL,
  `nppes_provider_zip5` varchar(50) NOT NULL DEFAULT '',
  `nppes_provider_zip4` varchar(50) NOT NULL DEFAULT '',
  `nppes_provider_state` varchar(5) NOT NULL,
  `nppes_provider_country` varchar(10) NOT NULL,
  `specialty_description` varchar(128) NOT NULL,
  `description_flag` varchar(3) NOT NULL DEFAULT '',
  `medicare_prvdr_enroll_status` varchar(3) NOT NULL DEFAULT '',
  `total_claim_count` int(11) NOT NULL DEFAULT '0',
  `total_30_day_fill_count` int(11) NOT NULL DEFAULT '0',
  `total_drug_cost` double NOT NULL DEFAULT '0',
  `total_day_supply` int(11) NOT NULL DEFAULT '0',
  `bene_count` int(11) DEFAULT NULL,
  `ge65_sup` varchar(5) NOT NULL DEFAULT '',
  `total_claim_count_ge65` int(11) DEFAULT NULL,
  `total_30_day_fill_count_ge65` int(11) DEFAULT NULL,
  `total_drug_cost_ge65` double DEFAULT NULL,
  `total_day_supply_ge65` int(11) DEFAULT NULL,
  `bene_count_ge65_suppress_flag` varchar(5) NOT NULL DEFAULT '',
  `bene_count_ge65` int(11) DEFAULT NULL,
  `brand_suppress_flag` varchar(5) NOT NULL DEFAULT '',
  `brand_claim_count` int(11) DEFAULT NULL,
  `brand_drug_cost` double DEFAULT NULL,
  `generic_suppress_flag` varchar(5) NOT NULL DEFAULT '',
  `generic_claim_count` int(11) DEFAULT NULL,
  `generic_drug_cost` double DEFAULT NULL,
  `other_suppress_flag` varchar(5) NOT NULL DEFAULT '',
  `other_claim_count` int(11) DEFAULT NULL,
  `other_drug_cost` double DEFAULT NULL,
  `mapd_suppress_flag` varchar(5) NOT NULL DEFAULT '',
  `mapd_claim_count` int(11) DEFAULT NULL,
  `mapd_drug_cost` double DEFAULT NULL,
  `pdp_suppress_flag` varchar(5) NOT NULL DEFAULT '',
  `pdp_claim_count` int(11) DEFAULT NULL,
  `pdp_drug_cost` double DEFAULT NULL,
  `lis_suppress_flag` varchar(5) NOT NULL DEFAULT '',
  `lis_claim_count` int(11) DEFAULT NULL,
  `lis_drug_cost` double DEFAULT NULL,
  `nonlis_suppress_flag` varchar(5) NOT NULL DEFAULT '',
  `nonlis_claim_count` int(11) DEFAULT NULL,
  `nonlis_drug_cost` double DEFAULT NULL,
  `opioid_claim_count` int(11) DEFAULT NULL,
  `opioid_drug_cost` double DEFAULT NULL,
  `opioid_day_supply` int(11) DEFAULT NULL,
  `opioid_bene_count` int(11) DEFAULT NULL,
  `opioid_prescriber_rate` double DEFAULT NULL,
  `antibiotic_claim_count` int(11) DEFAULT NULL,
  `antibiotic_drug_cost` double DEFAULT NULL,
  `antibiotic_bene_count` int(11) DEFAULT NULL,
  `hrm_ge65_suppress_flag` varchar(5) NOT NULL DEFAULT '',
  `hrm_claim_count_ge65` int(11) DEFAULT NULL,
  `hrm_drug_cost_ge65` double DEFAULT NULL,
  `hrm_bene_ge65_suppress_flag` varchar(5) NOT NULL DEFAULT '',
  `hrm_bene_count_ge65` int(11) DEFAULT NULL,
  `antipsych_ge65_suppress_flag` varchar(5) NOT NULL DEFAULT '',
  `antipsych_claim_count_ge65` int(11) DEFAULT NULL,
  `antipsych_drug_cost_ge65` double DEFAULT NULL,
  `antipsych_bene_ge65_suppress_flg` varchar(5) NOT NULL DEFAULT '',
  `antipsych_bene_count_ge65` int(11) DEFAULT NULL,
  `average_age_of_beneficiaries` double DEFAULT NULL,
  `beneficiary_age_less_65_count` int(11) DEFAULT NULL,
  `beneficiary_age_65_74_count` int(11) DEFAULT NULL,
  `beneficiary_age_75_84_count` int(11) DEFAULT NULL,
  `beneficiary_age_greater_84_count` int(11) DEFAULT NULL,
  `beneficiary_female_count` int(11) DEFAULT NULL,
  `beneficiary_male_count` int(11) DEFAULT NULL,
  `beneficiary_race_white_count` int(11) DEFAULT '0',
  `beneficiary_race_black_count` int(11) DEFAULT '0',
  `beneficiary_race_asian_pi_count` int(11) DEFAULT '0',
  `beneficiary_race_hispanic_count` int(11) DEFAULT '0',
  `beneficiary_race_nat_ind_count` int(11) DEFAULT '0',
  `beneficiary_race_other_count` int(11) DEFAULT '0',
  `beneficiary_nondual_count` int(11) DEFAULT '0',
  `beneficiary_dual_count` int(11) DEFAULT '0',
  `beneficiary_average_risk_score` double DEFAULT '0',
  KEY `idx_npi` (`npi`),
  KEY `idx_state` (`nppes_provider_state`),
  KEY `idx_specialty` (`specialty_description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=
'Summary of NPI Prescriptions Table';
```

The above schema is about the NPI Prescribers of Drug with their varying Claim Counts, Drug Cost and Supply in the relevant drugs classified into Health Risks. The health risk types identified are:
- Opioid
- Antibiotic
- HRM
- Antipsychotic, and
- Generic (this is the category that is assumed for the rest of the drugs, used for calculation purposes)

The general trend is to have huge value for generic category and lesser values for the other categories. From this category, the health risk ratio is evaluated as the ratio of Total Claim Count versus the Count of drugs under a specified category. For statistical evaluation purposes, this risk ratio is aggregated over the NPI Prescribers. The beneficiary average risk score is a viable alternative attribute which can be used as an order parameter indicating the risk level that the Medicare beneficiary will have on every claim of the drug. The plots provided below is of the drug weights formed from the formulaic project metric formed by the Data Stewards.

> kde plot of Drug Weights

![kde plot of Drug Weights](http://localhost:3000/Images/4_Factors/kde_drug_weights.png)

    - The Drug Weights are sensitive to their risk levels 
    posed by their NPIS, so hence the inverse hyperbolic tangent 
    to indicate the loss and gain in forming the drug weights 
    from their risk level. The metric measures the entropy value 
    to indicate that the greater the risk ratio is, the greater 
    is the drug's weight; and lesser the risk ratio is the greater 
    is the entropy factor.

> kde plot of Health Risk Ratio Aggregated OVER NPIs taken as an Average for the Drug

![kde plot of Health Risk Ratio Aggregated OVER NPIs taken as an Average for the Drug](http://localhost:3000/Images/4_Factors/kde_health_risk_ratio_avg.png)

The specialty factor drug is devised to reflect the Availability and Awareness of the Drug. These factors are used when the Order Number has been selected, the Cluster has been formed, Optimization has been conducted and when the Nearest Neighbors show distinctive values for this factor. The plot given below is that of the Specialty Factor drug formed from the dataset of the State 'Pennsylvania'.

> kde plot for Specialty Factor Drug

![kde plot for Specialty Factor Drug](http://localhost:3000/Images/4_Factors/kde_specialty_factor_drug.png)

### Exploratory Visualization

The Canberra distance is an interesting metric that is useful for observations because when the claims are recorded against the NPI(s), there is a cost of the drug that are recorded against them as well; which in turn will query the Medicare to Supply sufficient drugs to the Specified NPI(s). The Canberra distance can evaluate whether Supplying the drugs is a good decision compared with the prescriptions in finding the NPI(s) Availability for prescribing the drug and the knowledge factor in finding the NPI(s) Awareness of the drug. 

```sql
SELECT batch, cluster, npi, 
specialty_description, drug_name,
specialty_factor_drug 
FROM pa_kmeans_y_pred_spl_factor_drug
```

The feature sets used for evaluating the Canberra distance is the same as those feature sets used for the centroid calculation of the K-Means clustering method. These feature sets as is recollected in this scenario has been obtained via a Feature Selection method. The below SQL code explains the feature sets accepted for Specialty Factor for Drugs:

```sql
SELECT claim_per_dollar, supply_per_claim, 
drug_mean_claim_per_dollar, 
drug_mean_supply_per_claim, drug_std_claim_per_dollar,
drug_std_supply_per_claim, drug_weight, 
specialty_weight, bene_risk, 
IF(medicare_prvdr_enroll_status = 'E', 1, 0) 
as enroll_status, npi_claim_count,
pa_specialty_entropy.median_count,
IF(drug_state_category = 'opioid', pa_cost_function.opioid, 
IF(drug_state_category = 'antibiotic', pa_cost_function.antibiotic, 
IF(drug_state_category = 'antipsychotic', pa_cost_function.antipsychotic, 
IF(drug_state_category = 'HRM', pa_cost_function.hrm, 
pa_cost_function.generic)))) as health_risk_ratio
FROM pa_cost_function
INNER JOIN claim_aggregates.pa_specialty_entropy ON 
pa_specialty_entropy.specialty_description = 
pa_cost_function.specialty_description
ORDER BY 
supply_per_claim, 
(drug_mean_supply_per_claim / drug_std_supply_per_claim),
(IF(drug_state_category = 'opioid', pa_cost_function.opioid, 
IF(drug_state_category = 'antibiotic', pa_cost_function.antibiotic, 
IF(drug_state_category = 'antipsychotic', pa_cost_function.antipsychotic, 
IF(drug_state_category = 'HRM', pa_cost_function.hrm, 
pa_cost_function.generic)))))
```

| Name   | Factor Variables       | Latent Variables                |
| ------ | ---------------------- | ------------------------------- |
| Safety | ** Specialty weight ** | ** Drug Factor for Specialty ** |
| Health | ** Drug Weight **      | ** Specialty Factor for Drug ** |

Referring to the table above, each variable for the Latent vector and the Factor vector are run through a data load routine using the K-Means Clustering technique. For the state of Pennsylania, there are around 1.253 million data rows which are then divided into 150000 data rows by forming 9 batches and 50 clusters for the variable: "Specialty Factor for Drugs". The variable: "Drug Factor for Specialty" has 9 batches and 100 clusters. Provided below is the Cluster batch specification for all the Variables.

| Name                       | Clusters | Batches |
| -------------------------- | -------- | ------- |
| Specialty Factor for Drugs | 50       | 9       |
| Drug Factor for Specialty  | 100      | 9       |
| Specialty Weight           | 300      | 51      |
| Drug Weight                | 300      | 52      |

The distance evaluation reveals a distance of around 1.10 - 1.30 for those records which have the same cluster and batch numbers and the same Drug Name and Specialty. The distance evaluation for the same Domain records are more or less the same, with some records having around 1.40 or 1.50. In the analysis conducted, there are 27 records with same Domain Records and more than 80 records for the same cluster and batch numbers. Provided below are the bar plot and Facet Grid Plot of the pair-wise comparison between the Specialty factor for drug feature set.

> Bar Plot indicating the distribution of Canberra Distance over NPI(s) in Comparison with the selected NPI

![Bar Plot indicating the distribution of NPI(s) Distance in Comparison with the selected NPI](http://localhost:3000/Images/Canberra_Distance/BarPlot.png)

Four different Facets have been termed in the Facet Grid plot, by increasing Canberra Distance, as Cluster Types and Resource Types.

> Facet Grid indicating the Split of Dataset by canberra distance

![Facet Grid indicating the Split of Dataset by canberra distance](http://localhost:3000/Images/Canberra_Distance/FacetGrid.png)

The discussion here refers to the feature sets selected for "Specialty Factor for Drugs" with the Resource Type of 'Taper' and Cluster Type of 'Node' which is found as the best solution for finding the nearest NPI(s) based on availability. But when the selection is made on the dataset and feature set for "Drug Weights" or "Drug Factor for Specialty", the possibility of finding the best solution would lead to the distance measurement of the Awareness of the drugs.

The feature set for the current data model contains important characteristics such as: 
- The median count (Indicates how far the NPI(s) have gone in Prescribing medications)
- The Net Claim Count (Aggregated over NPI(s) for a specific specialty)
- The Health Risk Ratio (Indicates the relevance of the Drug)
- The quantities Drug Mean and Drug STDDEV (Indicating a non-heterogenous distribution for nearest solutions)

When the distance is low as well as when the combinations are in the same cluster and batch, there is a tendency for the prescriptions to be grouped together for further investigations. It is found that the distance metric for the selected Drug and Specialty Combination (eg:- 'XOLAIR', 'Allergy/Immunology', '1851444269') is less for those specialties which are related to the type Disciplinary. There are 6 different categories termed as Primary Ownership categories: 

    - Family, 
    - Clinic, 
    - Department, 
    - Hospital, 
    - Disciplinary, and
    - Heathcare

These categories have been managed properly before going into the database. The category weights have been used widely within Formulaic metrics. Since they are inputs to supervised learning algorithms, they are put to feature scaling. An example available within Likelihood Estimation has used the concept of Primary Ownership weights before being scaled into usable values.

### Algorithms and Techniques

In this section an elicitation of what worked and what did not work has been addressed. Ensebmble Boosting has been used to precisely understand the formulaic Factors obtained using Data Stewardship. Gradient Boosting Regressor provided a good score with respect to the Feature sets selected individually for each of the 4 factors. The problem domain started with an understanding of what is required for the Specialty Weight which is estimated using a Chebychev's probability such that the prediction interval can be extracted. The number of NPI prescribers who have surpassed the median of the Claim Count has been recorded using SQL code. This has been included in the Formulaic metric. 

Feature Selection results from the Gradient Boosting regresion gave rise to Cluster Analysis. The best clustering method to be used is Affinity Propagation, but computing requirements did not permit the Algorithm to fit the data. Similarly, Agglomerative Clustering has not been considered because k-means proved to be a good alternative for clustering the factor data and for pushing the dataset into batches of data based on the ORDER BY parameters obtained from Gradient Boosting Regression. 

The reason for accepting a formulaic metric as presented in the Problem Statement is because there must be a comparison between the results obtained via the prediction of problem domain and the results manually estimated using estimation techniques. It is known that Chebychev's probability is a good measure of the spread of the data, and hence the formula has been chosen in such a way that the Specialties or the corresponding NPI Prescribers must reflect upon the decisions made by the Medicare Drug Plan. In the case of Drug weights, the Health Risk Ratio as explained previously, has been chosen because it is a good measure of the importance of the risk type of the drugs with respect to the NPI Prescribers as explained in the SQL code below:

```sql
-- Dumping database structure for prescription_aggregates
CREATE DATABASE IF NOT EXISTS `prescription_aggregates` 
/*!40100 DEFAULT CHARACTER SET utf8 */;
USE `prescription_aggregates`;

-- Dumping structure for table prescription_aggregates.pa_pres_drug_risk
CREATE TABLE IF NOT EXISTS `pa_pres_drug_risk` (
  `nppes_provider_state` varchar(10) NOT NULL,
  `specialty_description` varchar(128) NOT NULL,
  `npi` varchar(128) NOT NULL,
  `generic` double NOT NULL DEFAULT '0',
  `brand` double NOT NULL DEFAULT '0',
  `other` double NOT NULL DEFAULT '0',
  `opioid` double NOT NULL DEFAULT '0',
  `antibiotic` double NOT NULL DEFAULT '0',
  `antipsychotic` double NOT NULL DEFAULT '0',
  `hrm` double NOT NULL DEFAULT '0',
  KEY `idx_npi` (`npi`),
  KEY `idx_specialty` (`specialty_description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

Regression analysis of the Health side of data involving the Weightage on Drugs Applied by the formulaic metric reveals closer prediction values to the scaled over drug weights. The algorithm score was around -3.0 indicating the closeness to the true value. The Coefficient of determination was negative as well. The analysis of the Four Factors ("Drug Factor for Specialty", "Specialty Factor for Drugs", "Drug Weights" and "Specialty Weights") is accepted as an initial stage preceeding the Likelihood Estimation in the Factor Analysis problem. It is interesting to note that a linear regresion between total day's supply and total claim count or even claim per dollar did not provide sufficient results. Provided that the Total Drug Cost and Total Day's Supply quantities are the net values based on all the claims applied, an investigation of regression analysis on selected drugs did not reveal much information on model fitting characteristics. However, the regression analysis on the formulaic metrics proved successful.

One of the techniques which is prominent in this section is the Minimum Mean Square Estimation (in short, MMSE). In MMSE, the objective is to attain probabilities in the vector form. The covariance metric is an important determinant to suggest in what directions the search algorithm must be performed such that optimum solution can be found out. MMSE not only provides a covariant value that denotes the functor, but also specifies the expectation of the result. In this project, MMSE is used to determine the expectation of claim per dollar or supply per claim of an observation provided a measurement is taken for a known value. 

Given below is the subscript for MMSE:

```mathematica
ϕ(mmse) = x_mean + σ(xy) / σ(y) * (y - y_mean)
```

Also, 
```mathematica
̂x ~ x / |x|
```

The MMSE Algorithm for minimisation, takes control of Expectation values of an observation given the measurement. Contexually, the MMSE is used within an optimization algorithm to obtain good results. Since there are clustering data fed into the data, the canberra distance provides how far the observations are ***(the desired specialty and drug combination)*** and then minimise them by selecting the domain measurements and using MMSE as comparison. The algorithm step will have a search direction thereby, and suitable decisions can be made on the right prescriptions. Provided below are the plots comparing the Expectation of Observation values which is bound by a probability of clustering and the Observation Quantity which is the actual Quantity from the `pa_state_spl_drug_npi` database table.

> Box plot between Observation Quantity and Observation Expectation

![Box plot between Observation Quantity and Observation Expectation](http://localhost:3000/Images/MMSE/BoxPlot.png)

Also, provided below is a theoretical regression plot between the Expectation of Observations and Observation Quantity with the slope being the Bayesian probability of Observation given Measutement. The Expectation of Observation is obtained using the formula provided below.

```mathematica
Expectation(Observation GIVEN Measurement) = 
Observation Quantity * Prob(Observation GIVEN Measurement)
```
> Regression plot between Observation Quantity and Observation Expectation

![Regression plot between Observation Quantity and Observation Expectation](http://localhost:3000/Images/MMSE/Obs_Meas_Regression.png)

In the evaluation stage of the solution, the results may end up in couple of clusters and batches. If appropriate filters such as the canberra distance and MMSE are applied to narrow the results down, the result shows varying Factor data as entered by the data stewards. These factor data can then be applied to fit a newly devised algorithm for solving the purpose.

In this section, the below table provided as a reference is required for providing a direction to the Likelihood Estimation. The below decision model has been obtained from the State wise seggregation of mapping of death due to Overdoses which shows the priority that an Analyst from the Data Governance Council should be following:

| Less Visibility | Greater Visibility |
| --------------- | ------------------ |
| Evidence Based  | Risk Aware         |
| Reasoning       | Risk Assessor      |

Based on our previous findings what is required is to understand the Problem domain. There are two decision models by which the Likelihood Estimation can be conducted. One is a priority based evaluation, to understand what comes first; and the other one is a Solution oriented way towards reaching the goal of determining the best prescriptions. Provided below are two decison models that have been discussed here:

#### What Comes First ? (Priority Based)

| Supply             | Demand                  |
| ------------------ | ----------------------- |
| The Drug Influence | The Safety Vector       |
| The Health Vector  | The Specialty Influence |

This model restricts our calculations because assumptions have to be made such that the initial metrics obtained from the Data Stewardship Team is a True Value estimation. But in reality it is not as it is used for comparison of prescriptions. A better decision model is to go towards finding the problem of drug administration and medication coverage for the solutions: `OPTIMIZED MEDICATION DOSES` and `THE EVALUATION OF DRUG FACILITY`.

#### Solution Oriented Way

| Medication Coverage | Cost and Quality |
| ------------------- | ---------------- |
| Medication          | Cost             |
| Coverage            | Quality          |

##### The Likelihood Problem Domain: Medication Coverage

This is a method to evaluate the risk by calculating the maximum likelihood estimate in making the drug available to each Specialty such that the coverage value is increased. In categorizing the prescription into a positive class, a deeper analysis of the problem of Optimality of Naive Bayes is required. In this section, the likelihood estimate is calculated based on the four measurable values:
- Drug Cost per claim
- Supply per Claim
- Beneficiary Count
- Claim Count

The Z vector must be originated from the Demand side of things, and hence the z vector must be a 4 dimensional vector involving:
- Specialty Weight
- Specialty Factor for Drugs
- A suitable Weight * Drug Factor for Specialty
- A suitable Weight * Specialty Factor for Drugs

The lambda vector generated must be of the complex form, preferably the log distribution such that the positive or negative classification due to the Optimality of Naive Bayes is addressed. More explanation about the lambda vector has been addressed in later sections. Provided below are few decision nodes and the spheres of influence that corresponds to the Medication Coverage.

> Medication Coverage Decision Nodes

![Medication Coverage Decision Nodes](http://localhost:3000/Images/Medication_Coverage/Medication_Coverage_Coord.png)

The problem domain of Medication Coverage has addressed three major decision nodes: Cost per Claim, Supply per Claim and Claim Count. The spheres of influence which have been addressed here are: Cost per Claim and Supply per Claim; and Beneficiary Count and Claim Count.

> Medication Coverage Spheres of Influence

![Medication Coverage Spheres of Influence](http://localhost:3000/Images/Medication_Coverage/Medication_Coverage_Sphere.png)

##### The Likelihood Problem Domain: Cost and Quality

This method evaluates the risk by calculating maximum likelihood estimate in the origins of Supply and Demand such that a Drug Plan can be made as per the historic data and the findings of Availability from the Medication Coverage problem domain. In analysing the problem of cost and quality, four feature sets are used:
- Drug Cost per Fill
- Supply per Claim
- Fill Count
- Claim Count

The cost v/s quality chart for the medication will reveal the dependency of the State and Drug with respect to their drug_cost, claim_count and fill_count.
The evaluation of the dependency is done using a multivariate change of variables.

The Z vector must be originated from the Supply side of things, and hence the z vector must be a 4 dimensional vector involving:
- Drug Weight
- Drug Factor for Specialty
- A suitable Weight * Specialty Factor for Drugs
- A suitable Weight * Drug Factor for Specialty

The lambda vector generated must be of the complex form, preferably the log distribution such that the problem of outliers has been addressed.

> Cost and Quality Decision Nodes

![Cost and Quality Decision Nodes](http://localhost:3000/Images/Cost_Quality/Cost_Quality_Coord.png)

The problem domain of Cost and Quality has addressed three major decision nodes: Cost per Fill, Fill Count and Claim Count. The spheres of influence which have been addressed here are: Cost per Fill and Fill Count; and Supply per Claim and Claim Count.

> Cost and Quality Spheres of Influence

![Cost and Quality Spheres of Influence](http://localhost:3000/Images/Cost_Quality/Cost_Quality_Sphere.png)

##### The choice of Z Vector

| For Medication Coverage                  | For Cost and Quality                     |      |
| ---------------------------------------- | ---------------------------------------- | ---- |
| The choice of the z vector for the medication coverage is based on the contribution of the specialty towards care; the awareness of the specialty on the drug; the suitability of the drug for the specialty; and the empirical error found due to the mismatch between the specialty and the drug. | The choice of z vector for the cost and quality is based on the marginal weight of the drug towards care; the usage variables of the drug with that specialty; the suitability of the specialty with the drug; and the debiasing factor from the distribution of the drug factor specialty. |      |

##### The evaluation of Z Vector

| For Medication Coverage                  | For Cost and Quality                     |      |
| ---------------------------------------- | ---------------------------------------- | ---- |
| The evaluation of z vector is related to the awareness of the drug. The suitability value here is the negated suitability of the drug towards the beneficiary. | The evaluation of z vector here is related to the hindrance of making a drug plan or the negated cognitive bias of a particular drug for that specialty; and negated suitability of the specialty towards the supply of the drug. |      |

This section explains the integration of Safety and Health parameters with the Z and Lambda values of the Likelihood Estimation. One method to find the empirical error applicable for the Medication Coverage domain is to conduct a Gaussian Process Regression to evaluate a popularity check because the known instances of Specialty and Drug is inductive in forming a desired prescription. Hence, the canberra distance provides a distance measure indicating the error with which the prediction is made. This method has been found useful. The method used for finding the Cognitive Bias within the Cost and Quality domain is to evaluate a liblinear based Support Vector Classification. 

### Benchmark

Referring to the tables provided below:

| Name                | Safety                          | Health                          |
| ------------------- | ------------------------------- | ------------------------------- |
| Drug Influence      | ** Drug Factor for Specialty ** | ** Drug Weight **               |
| Specialty Influence | ** Specialty Weight **          | ** Specialty Factor for Drug ** |

| Name   | Factor Variables       | Latent Variables                |
| ------ | ---------------------- | ------------------------------- |
| Safety | ** Specialty weight ** | ** Drug Factor for Specialty ** |
| Health | ** Drug Weight **      | ** Specialty Factor for Drug ** |

A correlation between derived factors have been conducted. The derived factors are:

##### Table of Correlation Coefficients obtained

| Derived Factors         | PearsonR     | KendallTau  |
| ----------------------- | ------------ | ----------- |
| The Factor Variables    | -0.016882665 | 0.019395117 |
| The Latent Variables    | -0.00719662  | 0.226525425 |
| The Safety Vector       | -0.045233174 | -0.3636945  |
| The Health Vector       | 0.007356439  | 0.145354934 |
| The Specialty Influence | -0.082344774 | -0.21938194 |
| The Drug Influence      | -0.006151139 | -0.02664661 |

Kendall Tau evaluates the Discordants and Concordants and is equivalent to maintaining the order of the two variables compared. But PearsonR evaluates the direct correlation between the two variables. The role of data stewardship has provided a set of benchmark values which can be compared against the algorithms that has been devised.

Estimation of the likelihood of the NPI Prescribers has been conducted based on a known value of z vector. A Gaussian deterministic Regression analysis being done on the z vector makes it a good candidate for Likelihood estimation so that the likelihood and the z vector can be assessed together. This proves helpful for the solution towards optimized medication doses. The files provided in the Project with the likelihood values are that of the Gaussian Covariance values. The selection of environment for Likelihood estimation was apt for the development. It is found that the likelihood values are dependent on the Drug Name and Specialty. The likelihood value helps in decision making to enable the selection of alternative Specialties for the prescription of the drug, during the medication routine, when the results of Canberra distance has provided a selection of alternatives. It is known that greater the likelihood, the greater is the usage factor on the drug and as well as the Specialty. 

The likelihood value not only promotes a workflow dependent alternative selection, but also enables the pharmacists to plan their business based on cost-wise likelihood and market their products based on coverage-wise likelihood.

Methodology
-----------

### Data Preprocessing

##### Suitability of the Drug with a Specialty in terms of the Beneficiary Count

A suitable weight that can negate the beneficiary factor considered for medication coverage has been obtained using the SQL code provided below. This suitable weight is negated because for the drug to be available to a broad range of specialties, the administration of the drug as well as the NPI prescribers must not develop any constraints with the beneficiaries of the Claim in the planning stage. This value comes from the basic factor that greater the beneficiary count the more it is that the drug administration team must suppress them in the likelihood evaluation.

```sql
SELECT bene_count / total_claim_count 
FROM table_summary
WHERE specialty_description = 'Clinic/Center'
```

##### Suitability of the Specialty with a Drug in terms of Supply

A suitable weight that can negate the decisions of supply based on the reduced claim count compared with the standardised count, must be applied to the Specialty Factor for Drugs. An example from this parameter is that the Allergy/Immunology specialty is predominently found to have more standardised fill count than the claim count.

```sql
SELECT (part_d_30_day_fills - part_d_claims) / 
part_d_30_day_fills
FROM state_wise_drug_state
WHERE drug_name = 'FORTEO'
```

##### Empirical Error for the Specialty to prescribe from the Knowledgebase so that the drug is made available to more specialities

An initial empirical error based on prediction models using a Gaussian Process Prediction model has been conducted. This reveals the following metric scores:
    - The Mean Squared Error:  380107700.754
    - The Explained Variance Score:  -2.71976308142e-09
    - The R2 Score:  -9.35018942398

The prediction model used a prefix sum to the Gaussian Function which is close to the maximum of the `y` value. The model was predicting only for those values which are closer to the maximum value. It has also been found that the result is not suitable to be used as a parameter by estimating for each Specialty. The plot of the Gaussian has been provided below:

> Gaussian Prediction Model for finding the empirical error

![Gaussian Prediction Model for finding the empirical error](http://localhost:3000/Images/Z_Vector/Empirical_Error_Gauss.png)

Given below is the python code for the gaussian:

```python
def gaussian(x, det, value, sigma):    
    return 25800 + np.exp(
        np.log(np.sum(
        np.exp(-0.5 * (1/sigma) * np.square(
        np.subtract(x, value))), 
            axis=1)) + np.log(
            1 / (2 * math.pi)**0.5 * 1 / det))
```

The above model is an optimization model for finding the empirical error, and since it is difficult to conduct for each Specialty, another calculation of Empirical Error is inherited from the theory of Parameter estimation using Naive Bayes. Provided the NPI is an opioid prescriber, the empirical error is proportional to:

- The number of prescribers for the drug minus the number of prescribers under the specialty for the drug, and
- (1 - probability of the drug given specialty) 

The SQL code given below explains the usage:

```sql
SELECT state_wise_drug_state.prescribers, 
pa_specialty_entropy.drug_count, pa_state_spl_drug_npi.drug_name
FROM pa_state_spl_drug_npi
INNER JOIN drug_aggregates.state_wise_drug_state ON 
pa_state_spl_drug_npi.drug_name = state_wise_drug_state.drug_name
INNER JOIN claim_aggregates.pa_specialty_entropy ON 
pa_specialty_entropy.specialty_description = 
pa_state_spl_drug_npi.specialty_description

SELECT COUNT(DISTINCT npi) FROM pa_state_spl_drug_npi
WHERE specialty_description = 'Physician Assistant' 
AND drug_name = 'NOVOLOG'
```

##### Cognitive bias in prescribing the drug for a Specialty

The cognitive bias is based on a prediction model which predicts the Specialty and Drug Name based on two classifiers: 
- Stochastic Gradient Descent Classifier, and
- Linear Support Vector Machine Classification

The list of drugs obtained via Linear SVC:

| Specialty Description              | Drug Name                   |
| ---------------------------------- | --------------------------- |
| Oral &  Maxillofacial Surgery      | PENICILLIN V POTASSIUM      |
| Sports Medicine                    | GABAPENTIN                  |
| Plastic and Reconstructive Surgery | TRAMADOL HCL-ACETAMINOPHEN  |
| Plastic and Reconstructive Surgery | NEOMYCIN-POLYMYXIN-DEXAMETH |
| Maxillofacial Surgery              | CHLORHEXIDINE GLUCONATE     |

In the algorithmic view of estimating the cognitive bias, the linear support vector classification has been found useful because the drug names and specialties listed point towards the diagnosis of health conditions. Cognitive bias exactly refers to the diagnosis of health conditions and then prescribing an inappropriate care or medication. The distance between any prescription and the prescriptions listed in the Linear SVC algorithm conveys an important factor that is dependent on the diagnosis of each case. To conduct such similar experimentation, we have to consider the datasets from each US State filtered by each possible specialty and drug combinations, such that we get a list of prescriptions based on Linear SVC model. It is now known that the Linear SVC provides a retention factor, whereas the Stochastic Gradient Descent provides a growth factor. More details on the cognitive bias is provided in later sections.

The list of drugs obtained via Stochastic Gradient Descent Classifier:

| Specialty Description | Drug Name              |
| --------------------- | ---------------------- |
| Family Practice       | BRINTELLIX             |
| Internal Medicine     | TYPHIM VI              |
| Family Practice       | FLUTICASONE PROPIONATE |
| Internal Medicine     | HYDROMORPHONE ER       |
| Internal Medicine     | ADEFOVIR DIPIVOXIL     |

##### MMSE and Likelihood

In MMSE, the expectation is dependent on a vector form, similarly the λ factor is taken as the log of the resultant value apart from its complex nature. The logarithmic scale of a complex entity works better for polar coordinates and hence, it provides a direction to the λ vector. 

__________________

Certain Quality checks have been applied to the data handled in this project, some of the important ones are:
- ATORVASTATIN CALCIUM drug is found to have 0 drug cost for 'PA', 'Dentist', '1578621165'
    When 0 drug cost is obtained, it may be due to a carry over from previous plan
    So claim per dollar is equated to -ve number of cost_per_claim
    Or it is taken as an aggregate over the drug

- GABAPENTIN and TRAZON+DONE HCL in PA had 0 values for drug risk
    implying others were zero when compared to total claim count
    So that has been equated to 1.01 as the specialty was Acupuncturist

- Set claim_per_cost = total_claim_count when cost = 0 (Divide by zero Error)

- To evaluate the claim count, an sgn function has been used in calculating the Chebychev's probability. This sgn function makes sure it returns the lowest possible result, when standard deviation and mean of the distribution are comparison for the quantities: Supply per Claim, Claim per Dollar or Cost per Claim.
__________________

In evaluating the Z Vector for the Likelihood, the Z vector is scaled such that it has zero mean and unit variance. But the decision for scaling of the Z vector in the likelihood based on Safety and Health has been made using a MinMaxScaler with feature range of (0, 1).

##### Preprocessing Steps

In the death ratio evaluation, the death number from the drug overdose dataset is used for adding a noise covariance within the evaluation. A factor of 10000 has been considered for displaying appropriate values in the kernel density estimation plot. In the 4 factors' evaluation, the cost and supply are quantized such that unit values are produced within the formulaic metrics. A probabilistic value used in the outlier detection using Chebychev's rarity of outliers have been used for the estimation of the Specialty Weight. The dataset that is used in the 4 factor estimation is the Prescriber Info dataset. A decision node function based on `sgn` function has been used with respect to finding the least attribute for the distribution in terms of the mean and standard deviation. A central tendency estimation on the Specialty Weights has been included within the Formulaic Metric. Quality checks found from the integration of data from text file to the database, involved changing the data types of the data and aggregation of data by not including the cities in the state-wise dataset of prescribers. In the drug weight evaluation, the calculation of health risk ratio dependent on the types of drugs such as: (Opioid, Antibiotic, High Risk Medications and Antipsychotic) are considered as the criteria. 

In the linear regression of the 4 factors, the model presented in the 4 Factors has been evaluated. The train and test data for the Health and Safety vectors have been split using the train_test_split by accepting a test size factor for 0.33. For the linear regression, a Min Max Scaling has been applied to the training set of the regression values to be predicted, with a feature range of 0 to 1. The ratio of standard deviation and mean has been considered for debiasing of the distribution. The assessment on the drug weight reveals a coefficient of determination of -3.0, and a comparitively low explained variance score of -1.86.

The Cost function using K-Means algorithm and Gradient Boosting regression has been verified as a Feature Selection and Feature Extraction method by taking into consideration a variety of feature sets. The Stochastic Gradient Descent regressor algorithm has considered a test train split size of 0.33 and has come up with a score of around -2.13250093216e+28.

> Plot showing the underfitting and overfitting of the curve using the Stochastic Gradient Descent Regressor

![Plot showing the underfitting and overfitting of the curve using the Stochastic Gradient Descent Regressor](http://localhost:3000/Images/Cost_Function/OverFitting_UnderFitting.png)

The Stochastic Gradient Descent Regressor has not been found useful as the score is a huge value and the algorithm underfits the curve.

Considering the GradientBoostingRegressor Ensemble method, the data was not split into test and train data as the decision path had to be clear. The feature set with maximum accuracy of 0.99, termed as Trawling Mode, was accepted as the true feature set. The ensemble boosting method was conducted for the entire dataset which reveals an accuracy score of about 0.971235726988. The other feature sets were derived from the true feature set.

> Plot provided below is that of the Model Learning under the Gradient Boosting Regressor

![Plot provided below is that of the Model Learning plot under the Gradient Boosting Regressor](http://localhost:3000/Images/Cost_Function/Trawling_Mode_Model_Learning.png)

> Model complexity plot for Gradient Boosting Regressor, Trawling Mode

![Plot provided below is Model Complexity of Trawling mode](http://localhost:3000/Images/Cost_Function/Trawling_Mode_Model_Complexity.png)

A different feature set was chosen, termed as the Maximised Mode, by including the Specialty Weight and Drug Weights, and the score evaluated was lesser than the Feature set from the Trawling method. 

> Plot provided below shows the Model Learning plot under the Maximised Mode of Gradient Boosting Regressor

![Plot provided below shows the Model Learning plot under the Maximised Mode of Gradient Boosting Regressor](http://localhost:3000/Images/Cost_Function/Maximised_Model_Learning.png)

The task of MMSE calculation was completed by predicting the expectation values using the covariance matrix. The outcome of the task is a probability that is about the calculation of observed quantities given the measured quantities are provided. Canberra distance proved to be useful for finding the distance between the quantities, either measured, derived, or observed. A measured quantitiy is one which already exists in the database, an observed quantity is derived from the mean values of the quantities under consideration and a derived quantity is that which is derived from the global mean and defined covariance in which the observed quantity matches its specification. MMSE has been proved to be useful and must be moved towards the area of Optimization where Conjugate Gradient can be applied.

The Z vector for the likelihood takes into consideration the Cost per Fill, Supply per Claim, Cost per Claim and Supply per Fill for a Naive Bayes based Parameter estimation using probabilities. The Likelihood scales the Z vector with zero mean and unit variance with the lambda vector taken as a complex entity in a logarithmic scale. The Gaussian Covariance is calculated with the presentation of the Log likelihood using a pre-specified determinant value.

### Implementation

In the formation of the "Sigh Value Mapping", the noise covariance was taken as an estimation technique but it is not found to be useful. The 4 Factors Evaluation, has been conducted based on the concepts from Chebychev's Rarity of Outliers. It states a maximum bound probability for data that is outside a prediction band formed from quantized standard deviation taken from the mean. Another important concept is the usage of inverse Hyperbolic Tangent for obtaining an asymptotic estimation of drug weights from the Risk ratio in the Health domain. The estimation of latent variables are based on observed quantities obtained from unit values such as Claim per Dollar and Supply per Claim; and intangible quantities such as the Number of Prescribers for the drug and the Number of Drugs that a specialty has access to. These intangible quantities provide knowledge and helps in decision making. The estimated quantities with respect to Specialty and Drugs need to be evaluated for correctness and hence, a Linear regression analysis on these factors have been applied for evaluating the Four Factor model. The measure of accuracy of the linear regression reveals the degree of closeness with the true value. In the linear regression analysis, test train split has been conducted with two types of data: 
- (1) Data that is related to a single entity and requires responsibility assignment for those entities, and
- (2) Data that is related to multiple entities and requires an assessment based on delegation of tasks.

The data type (1) must be associated with the knowledge based attributes such as the intangible attributes described earlier, and the data type (2) must be associated with the distribution because of the seggregated data found by analysing the Mean and Standard deviation of the data row containing the drug or specialty.

The main aim of the Cost Function is to extract the corresponding attributes reated to the findings of the formulaic metrics from the Data Stewardship Team. While extracting the attributes it is important to note that the data type (2) must be associated to those attributes obtained directly from the master data distribution. The extraction of records termed as golden records from the master data is the key to the analysis of the latent variables. The golden records are those records which are obtained by mixing and matching of the dataset to expose the true value. This is related to the value obtained in the MMSE section using the covariance of irregular datasets. The data type (2) are those data that are directly obtained from the data mart, which provide the end user to analyse the data in a way that is suitable to the use case. In the analysis of data type (2) several optimization tables have been created such as `pa_cognitive_bias_estimate`, `pa_spl_drug_shared_states`; apart from the tables obtained via Clustering techniques. 

The K-Means clustering has been selected as an important method to reach to the respective prescriptions provided one of their prescriptions with close relation come into the dataset. The four database tables created using thiis method are: (1) `pa_kmeans_y_pred_drug_factor_spl`, (2) `pa_kmeans_y_pred_drug_weight`, (3) `pa_kmeans_y_pred_spl_factor_drug`; and (4) `pa_kmeans_y_pred_spl_weight`. The K-Means for the drug factor specialty has shown an algorithm score of -7.6491163062e+14.

In order to reach to the prescriptions, a classification cost metric has been devised. This classification cost does a summation of the derived function, the mutual information between two random variables such as the object to be classified and the classified measurements. There are two possibilities with which the object can be classified: (1) Positive Classification, (2) Negative Classification. Such a classification must be identified, from the Classification Cost metric. There is another metric to indicate whether the classified object is moving towards a different class which is more suitable for the object. This metric is called as Deviance, which calculates the degree of randomness of the object with respect to the classification and the other classes. In order to overcome this factor there is a defined distance metric that is used within the classification domain as well. The metric, which is the Canberra Distance, is in a ratio form suitable for identification of the prescriptions. The covariance calculation helps the optimizer to identify the expectation of an estimate, but the evaluation is bounded by a classification probability which is a bayesian probability of observation provided the measurement has been made. In the example provided by considering 'Allergy/Immunology' as the specialty and 'XOLAIR' as the drug compared with 'Physician Assistant' and 'NOVOLOG' on the other side, the probability obtained is around 0.5, which indicates out of two classes it is probable that the result would be from one of the classes.

> Plot for Mutual Information between two sets of prescriptions between Physician Assistant and NOVOLOG and Nurse Practioner and LANTUS are provided below. The selected cluster and batch have the same specialty and drug name. NPI identifier is along the `y` axis and Mutual Information is along the `x` axis

![Plot for Mutual Information between two sets of prescriptions having different Specialty and Drug name](http://localhost:3000/Images/Optimization/mutual_information.png)

> Provided below is a plot for Mutual Information by multiplying a factor of the precription consisting of Nurse Practitioner and LANTUS for each prescription

![Plot for Factir based Mutual Information](http://localhost:3000/Images/Optimization/factor_mutual_information.png)

In the measurement of the likelihood of Medication Coverage, there are two factors which come into play:
- A Suitability Weight which is the ratio of Beneficiary Count and Claim Count
- An empirical error which is the dependent on prescribers and the drug count

In the measurement of the likelihood of Cost and Quality, there are two factors:
- A suitability weight which is the percentage delta difference between Standardised fill and Claim count
- A congnitive bias which is estimated algorithmically, based on same classifications of prescriptions and does not include clustering

The following SQL code demonstrates a parameter estimation which has been used within the Likelihood Estimation of Cost and Qaulity for this project:

```sql
SELECT (drug_cost / total_claim_count) / 
(total_day_supply / fill_count), npi 
FROM 
pa_state_spl_drug_npi
WHERE npi = '{0}' 
AND specialty_description = '{1}' 
AND drug_name = '{2}'
ORDER BY (drug_cost / total_claim_count) / 
(total_day_supply / fill_count)
LIMIT 0,1
```

```sql
SELECT SUM(drug_cost / total_claim_count) / 
(total_day_supply / fill_count) FROM
pa_state_spl_drug_npi
GROUP BY drug_name
```

### Refinement

The K-Means algorithm for the four factors had iterations of data load in order to fine tune the model. The parameters of the model such as the number of clusters is modified based on the performance of the clustering and the need to split between the per unit values of the prescription and the values obtained from the distribution. The dataset is also split based on the intangible values required for the Factor weights. For the Specialty Weights data, the batch wise separation also improved the clustering algorithm score when the dataset is clustered from n_clusters = 100 to n_clusters = 300 clusters. 

An example sql providing the clustered data and the domain data is provided below:

```sql
SELECT pa_kmeans_y_pred_spl_weight.`*`, 
cost_per_claim, supply_per_claim,
CAST(IF(drug_mean_cost_per_claim  != 0, 
drug_std_cost_per_claim / drug_mean_cost_per_claim, 
drug_std_cost_per_claim) AS DECIMAL(22,15)), 
CAST(IF(drug_std_supply_per_claim != 0, 
drug_mean_supply_per_claim / drug_std_supply_per_claim, 
drug_mean_supply_per_claim) AS DECIMAL(22,15)),
swd.prescribers, swd.part_d_claims, pct.total_day_supply,
IF(drug_state_category = 'opioid', 2, 
IF(drug_state_category = 'antibiotic', 3, 
IF(drug_state_category = 'antipsychotic', 4, 
IF(drug_state_category = 'HRM', 5, 1)))) as risk_weight
FROM pa_cost_function
INNER JOIN `optimization_tables`.`pa_kmeans_y_pred_spl_weight`
ON pa_kmeans_y_pred_spl_weight.specialty_description = 
pa_cost_function.specialty_description
AND pa_kmeans_y_pred_spl_weight.drug_name = 
pa_cost_function.drug_name
INNER JOIN drug_aggregates.state_wise_drug_state swd ON
swd.drug_name = pa_kmeans_y_pred_spl_weight.drug_name AND
swd.state = 'Pennsylvania'
INNER JOIN drug_information.pa_cost_quality_table pct
ON pct.specialty_description = 
pa_kmeans_y_pred_spl_weight.specialty_description
AND pct.drug_name = 
pa_kmeans_y_pred_spl_weight.drug_name
WHERE swd.state = 'Pennsylvania'
ORDER BY pa_kmeans_y_pred_spl_weight.`specialty_weight` DESC
LIMIT 0,100;
```

An initial classification of the dataset based on Stochastic Gradient Descent Classifier reveals a maximised score of about 0.35 for the dataset of size 300,000 rows obtained by multiple iterations of the datasets with a chosen batch size of 5. This analogy was used by the Gradient Boosting ensemble Regressor and the K-Means Clustering algorithm for partitioning of the dataset. A natural split of the dataset was identified based on the Decision Tree Classifier of the Drug Overdoses data providing death numbers of each state. In the comparison between the 4 Factor plot of the Specialty Weights and the Cost of the drugs that they handle, a fine tuning of the model revealed a successful comparison of the exponential function of drug cost showing the comparison of expenditure and the modelled specialty weight.

The preprocessing stage of evaluating the Z vector for likelihood consists of: Fitting the curve using a Gaussian Process, searching for a valid Empirical Error and the cognitive bias element. The curve which is fit is equivalent to the popularity check conducted for all the drugs under the specialty 'Physician Assistant'.

Optimizing the curve and the curve plot involved writing covariance to reflect the product quantity of feature sets: Drug Count and Supply; and Claim Count and Prescribers. In the Gaussian optimization, the covariance is divided by the square of the length of the training set to obtain the sigma value. The determinant for the gaussian is taken as the square root of the L2-norm of the 4 dimensional vector matrix. The drawback is that the `error` value obtained from the Gaussian scipy `curve_fit` function is not suitable for the usage as an empirical error. But the model can be fine tuned to reflect and find the expectation with which the prescribers are alloted to a `Physician Assistant` Specialty. This would be useful only for a popularity check and not correspond with the parameters of the Z Vector.

The evaluation of Z vector for the Likelihood estimation has been conducted with the Safety and Health realms and the Medication Coverage and Cost and Quality Domains. The determinant calculation as per the Gaussian distribution is found to be ineffective because of the huge sizes of the data that are used within the Factor analysis algorithm. Finally a determinant with power 0.005 came in agreement with the Safety likelihood values, but L2 norm took over for Health likelihood values when the lambda vector decided to show characteristics of logarithmic complex number. 

Lambda vector has been accepted as a vector showing the characteristics of search vector in the optimization routine. The complexity of lambda vector has influenced lambda to take a logarithmic form. Scaling the Z vector with zero-mean and unit variance initially provided small marginal quantities in Gaussian Covariance, and huge quantities in λ and ψ which was impossible unless scaling had to be with a Min Max Scaler. Scaling with Min Max Scaler with a feature range of 0 and +1 has been replaced by zero-mean and unit variance scaling in Medication Coverage and Cost and Quality.

Results
-------

### Model Evaluation and Validation

The stochastic gradient descent regressor was underfitting the curve, and the option was to choose an ensemble method. The ensemble method provided good accuracy but overfitted the curve as shown in the Model Learning and Model Complexity plot, but it was useful to extract certain features from the algorithm's decision path. The K-Means algorithm was tested with 100 and 300 clusters and a batch size of 51 but the algorithm with the n_clusters = 300 has been found useful. 

> Evaluation Domain Model

![Evaluation Domain Model](http://localhost:3000/Images/Model/Evaluation.png)

The K-Means algorithm reveals a good inertia score that is usable for evaluating the Canberra Distance from the dataset. For validating the 4 Factor model, a linear regression was rigorously tested using train_test_split by accepting a test_size of 0.33. Since the likelihood estimation required a consistent domain, the Z_Vector algorithms such as Stochastic Gradient Descent Classifier and Linear SVC was tested using KFold splits of 75 and 100. The results from the KFold splits of 100 has been presented here.

The determining factors for clustering the prescriptions should be calculated in detail. The measure of deviance calculated in this section is the entropy sum of all classification costs for the specified prescription. While the deviance maps to the prescription's Specialty and Drug Name, the classification cost maps to the precription and all the prescriptions in the Cluster and Batch to be classified into. Plots of deviance and classification cost has been provided below:

> Deviance

![Deviance](http://localhost:3000/Images/Optimization/deviance.png)

> Classification Cost

![Classification Cost](http://localhost:3000/Images/Optimization/Classification_Cost.png)

> Drug Weight

![Drug Weight](http://localhost:3000/Images/Optimization/drug_weight.png)

> Specialty Weight

![Specialty Weight](http://localhost:3000/Images/Optimization/specialty_weight.png)

The Linear SVC Classifier was tested using KFold n_splits value as 75 and 100. The stochastic gradient descent classifier used the n_splits value of 75, and gave a score as presented below:
- Test data accuracy score:  0.0
- Train data accuracy score:  8.90432304884e-05
- The accuracy score of Stochastic Gradient Descent Classifier:  8.78580214374e-05

The scores as obtained using the Linear SVC and Stochastic Gradient Descent algorithms were not found to be useful and further fine tuning of the parameters proved to be ineffective. 

By altering the prescription entry to 'Cardiology' and 'FLUTICASONE PROPIONATE', the results obtained are:
    - Measurement ('Cardiology', 'FLUTICASONE PROPIONATE') 
    v/s Observation ('Endocrinology', 'NOVOLOG FLEXPEN') 
    - The Average of Mean Square Estimation (MSE) 
    Value is:  [  2.39793523e-02   5.03216263e+01] 
    - The Average of Minimum Mean Square Estimation (MMSE) 
    Value is:  [  1.43958761e-03   4.96767214e+01]

The classification Costs obtained for classifying the prescription into the listed cluster batch combinations are shown in the plot below:

> Plot for Classification Cost with Cluster in the X axis and Classification Cost in the y axis

![Plot for Classification Cost with Cluster in the X axis and Classification Cost in the y axis](http://localhost:3000/Images/General/Classification_cost.png)

The result here shows that the cluster 30 has got the least classification cost and implies it would be better identifying any of the related prescriptions if the prescription is classified into this cluster. If appropriate specialities are found within this cluster, it is likely that the prescription entry can look for an alternative supply of drugs for the chosen specialty.

The final model has been derived after the evaluation of the Z Vector which provided the empirical error and cognitive bias to be fed into the Likelihood Estimation model. 

The model has shown good performance in feature selection, and since the data selected for evaluating the canberra distance and the data selected for clustering the dataset using K-Means are based on the distribution values and intangible values respectively, the canberra distance metric will act as a statistical model. The relevance of this statement can be found within the graph of cluster distance shown below:

> Cluster Distance of NPI(s) where `y` is the NPI identifier and `x` is the canberra distance

![Cluster Distance of NPI(s)](http://localhost:3000/Images/Optimization/cluster_distance.png)

The K-Means clustering model has been tested with the prescriptions using SQL. The K-Means clustering provides a continuously increasing distributions of prescriptions which are related to each other by their medication usage. Depending on the value of the  specialty weight, the clustering technique changes dynamically. Also, this is applicale to the other 3 factors.

### Justification

The health and safety vector provided in the benchmark were only acting as statistical functors which represent the Four Factors estimated via Formulaic metrics. The vectors took shape in the form of the Product domains such as Medication Coverage and Cost and Quality. The shift towards domain understanding has confined the Lambda vector as a logarithmic complex entity and the Z Vector on the basis of Naive Bayes parametric form. The parametric form is in concordance with the minimum mean square estimation method that has been demonstrated earlier. Provided below is a correlation matrix representing a heatmap of the Four Factors:

> Heatmap of 4 Factors

![Heatmap of 4 Factors](http://localhost:3000/Images/Z_Vector/Correlation_Factors.png)

The closer the specialty weights and the farther the drug weights are, the relying latent factors must be compared against each prescription set. This enables us to define which is the precription set corresponding to `optimized medication dosages` and which ones are corresponding to `the drug facility`. If medication prescriptions are likely to occur within a specific cluster and batch, the classification cost must be the least, the deviance must be the least and the mutual information between the prescriptions of the cluster and the prescription must show good relevance or a relatively high number. The final results will be justified provided the formulaic metrics show closeness of true factors in their prescriptions. The proposed Optimization domain model has been shown below:

> Optimization Domain Model

![Optimization Domain Model](http://localhost:3000/Images/Model/Optimization.png)

Conclusion
----------

### Free-Form Visualization

The project considers claim and supply as the prime entities for analysis as that is more closely related to the solution of 'OPTIMIZED MEDICATION DOSAGES'. In such an optimized dosage solution, the findings are:
- Lesser the distance with the other specialties, better is the optimization
- Closer the Specialty Weights are in comparison, the better is the selection of a Specialty
- Closer the latent variables are in comparison, the better is the drug administration
- Lesser the classification cost, better suited the prescription is towards the selected cluster and batch

Considering three cases of searching through the spectrum of metrics developed here, there are many possibilities of arriving at a good result. 

> Instrumentalising the case where a clinic requires more coverage of drugs within their specialization areas

If the prescriptions need to be selected across the batches and along the clusters, the optimization by evaluating the prescriptions with greatest deviance will reveal those NPI(s) with higher likelihood of presenting better coverage of drugs to the `Clinic`. If a new patient arrives at the Clinic and require a specialty or NPI that offers more coverage of drugs, then that NPI can be assigned to the new Patient.

A free-form visualization between Claim Count and Day Supply has been provided below for the drug, GABAPENTIN:

> GABAPENTIN

![GABAPENTIN](http://localhost:3000/Images/Z_Vector/Gabapentin_Claim_Supply.png)

A popularity check based on the number of prescribers for the drug over all the states for the Specialty Description 'Physician Assistant' reveals the following plot:

> Physician Assistant, Popularity Check on the Drugs. The popularity check displays the number of instances of NPIs for the data row that consists of a Specialty and a Drug Name. The popularity check is useful to select the NPI(s) for a feature set or conduct a new machine learning algorithm such as perceptrons on those NPI specialties. Recollecting that these intangible values are used within the feature selection process, the data rows can be better filtered using the metrics found in the Evaluation stage of the project.

![Physician Assistant, Popularity Check on the Drugs](http://localhost:3000/Images/Z_Vector/Drug_Data_Physician_Assistant.png)

Consider a case where there needs to be less recall for a Clinic or a Hospital, then the best methid is to select the prescriptions across the batchs and along the clusters with the least classification cost and least deviance. 

> Instrumentalising the case where a new clinic requires Less Recall

If a pharmacy which is operational or at inception stage needs to access the Medicare Claims data, they can identify the prescription list and can market the specified drugs based onb their target market. If a clinic or a new hospital needs less recall from the prescription sets they handle, they can obtain the specified NPI(s) for alotting them for the medication routine to the patients. This helps in creating better Drug Plans and can develop the healthcare within a particular region of the city. More datasets on the supply of drugs will reveal a cost-wise analysis on the drugs.

### Reflection

The distance metrics and classification metrics found are usable in the context of `OPTIMIZED MEDICATION DOSAGES` when the process of prescribing medications to patients starts. The work of prescription is dependent on the location of the patient such as the City; the type of medication and the routine check ups required. The distance metrics works based on NPI prescribers and deviance works based on the specialty and drug of the prescription. The benefits of switching an NPI prescriber are related to the Care that the patient receives. An outcome of the process would be the set of NPI(s) the patient is able to consult with. The better the classification and lesser the distance, the patient will be able to see any of the NPI(s) from the classified set of prescribers if all of them are based in the same city. It should be made sure that the patient is accessible to the location of the NPI.

The benefits of switching a specialty is when the patient routine is changed. When the patient is being monitored for adverse effects of the drugs or adverse effects of the health condition, the switching of the specialty is possible via the project's findings. Such a switching will be efficient for the NPI prescribers to make use because the drug administration, the medication process, the checkup process and the delivery of results can be measured under a workflow of team within the healthcare. When there is any change required or before a booking for an appointment is required, the patient can be monitoried using the implementation of the drug administration provided the patient's doctor has visibility to all of these changes. From the project's findings the `DRUG FACILITY` is also a subject area where the availability of the prescribers and the awareness of the drugs to the specialties are known to the healthcare. The `DRUG FACILITY` also measures: 
- the event of making the drug available to every specialty; and 
- the event of creating a drug plan for the NPI(s) with the Cost and Quality Likelihood value

The prediction of the classification are presented in the tables `pa_kmeans_y_pred_spl_weight`, `pa_kmeans_y_pred_drug_weight`, `pa_kmeans_y_pred_spl_factor_drug` and `pa_kmeans_drug_factor_spl`, while the actual classification is a process which takes place by comparing the metrics such as classification cost, deviance, mutual information and canberra distance with the predicted classification. The post classification process will retain the batch and cluster because they are tangible quantities obtained from the summary data for Factor variables and unit values from the distribution for Latent variables. Once the prescriptions are classified into the real set, the predicted values of cluster and batch will be from K-Means and the true values of cluster and batch will be from the post classification process.

A confusion matrix is related to the recall incidents and is defined as "The evaluation between the true values of K-Means predicted cluster and batch compared with the predicted value of cluster and batch obtained based on the reduction of the measure of Deviance and Classification Cost". 

The confusion matrix provides the awareness metric of the Specialty towards a drug and is useful to the solution of `DRUG FACILITY`. Similarly, a cohen kappa score found from the predicted cluster and batch and assigned cluster and batch values indicate the availability of the specialty towards the drug under consideration in the cluster and batch.

An initial analysis was conducted based on Microsoft R programming language, and resulted in issues with the database in SQLite generated. An attempt on the creation of MySQL database table with Microsoft R was made but resulted in errors due to parsing of the .txt File. Finally the best solution found was to import the .txt file using the LOAD DATA syntax available within the MySQL client 'HeidiSQL'.

### Improvement

The solution of `OPTIMIZED MEDICATION DOSAGES` is better understood when a Markov Chain Monte Carlo simulation of entry of prescriptions is executed using a sequential loading of prescription entries. The Gradient Based Optimization methods usable for the project are: The Conjugate Gradient method for optimizing the Gradient of the cost function, the BFGS algorithm for optimizing the datasets with latent variables by specifying the search directions; and the AffinityInvariant Monte Carlo Markov Chain method which optimises the Likelihood by walking along the samples to generate the Markov Chain.

A K-Nearest Neighbor algorithm will improve the sample accuracy in finding the appropriate prescription. Decisions can be made based on the values of the Factor variables and the Latent variables such that the prescriptions compared in the samples are close to each other. The coverage factor for a set of prescriptions can be obtained from the R2 score and F1 score of a K-Nearest Neighbors algorithm fitted locally.

In the Optimization algorithm, the BFGS is useful for the latent variables; the Conjugate Gradient method for Specialty Weights and Drug Weights; and the overall Affinity Invariant MCMC for sampling based on our domain knowledge. The optimization routine is conducted based on the value from least classification cost, least canberra distance and least deviance. Since optimization process is understood, only codebase has been presented in this project. The project provides leads into which the models can be optimized.

The modelling of Hamiltonian using a Markov Chain Monte Carlo will benefit the project in understanding how the prescriptions of entries are sequentially loaded or monitored such that a change of medication requirement from another city's prescription is handled by the project. The data from the 2013, 2014 and 2015 for all the cities provide enough information about all the prescription sets required for the project. The project has considered only the state of `Pennsylvania` for the year `2015`.

An improved model will provide better results for the likelihood obtained which will enable the prescriber or a Physician Assistant to choose the appropriate Specialty for the patient. An augmented Naive Bayes model which is formed by simulating the prescriptions using Markov Chain Monte Carlo supports decision making by identifying where in the cluster the classification cost is minimum, and when is it better to prescribe a medication under a selected Specialty provided the sequence of entry of prescriptions is known to the Optimizer.

## APPENDIX

#### Term Definitions

| Primary  Ownership     | The term for the category of a  Specialty |
| ---------------------- | ---------------------------------------- |
| Drug Risk Type         | The risk types which accepts the values of: generic, opioid,  antibiotic, HRM and antipsychotic |
| Health Risk Ratio      | A ratio between Total Claim Count and the relevant claim count  in the specified drug risk type |
| Availability           | Availability of the specialty for prescription of drugs |
| Awareness              | The knowledge base awareness of the drug for a specialty |
| NPI                    | The National Provider Identifier is a Prescriber of  medications |
| Clinic                 | A generic term for Primary Ownership     |
| Factor weights         | The weights corresponding to specialty and drugs |
| Latent variables       | The relational factors corresponding to specialty with drugs  and viceversa |
| Z Vector               | The z vector used in Likelihood estimation, also termed in  some instances for Latent Variables |
| Lambda Vector          | The λ vector  used in Likelihood estimation, also termed in some instances for Factor  weights |
| Unit Values            | Claim per Dollar, Supply per Claim, Cost per Claim and Cost  per Fill |
| Intangible values      | NPI Count, Prescriber Count, Drug Count, Total Day Supply |
| Tangible Values        | Distribution specific values             |
| Medication Coverage    | A domain to investigate the coverage factor |
| Cost and Quality       | A domain to investigate the beneficiary claims and the  standardisation of fill and claim |
| Original Likelihood    | Likelihood split by Safety and Health realms |
| Likelihood (optimized) | Likelihood by Medication Coverage and Cost and Quality |

#### Excel Workbook based Evaluation of Likelihood

| nppes_provider_state | specialty_description | drug_name                      | SUM(total_drug_cost) | SUM(total_claim_count) | SUM(total_30_day_fill_count) | X            | Ratio    | Multiplying Factor (Ratio) | Sigh       | Median Weightage | Mu          | Drug_Factor^2 | Factor_Sigh | Log Likelihood            | Likelihood  |
| -------------------- | --------------------- | ------------------------------ | -------------------- | ---------------------- | ---------------------------- | ------------ | -------- | -------------------------- | ---------- | ---------------- | ----------- | ------------- | ----------- | ------------------------- | ----------- |
| PA                   | Acupuncturist         | GABAPENTIN                     | 117.1999969          | 11                     | 11.10000038                  | 1289.199966  | 0.000214 | 10000                      | 4.57426265 | 0.487967467      | 1126.319992 | 0.0001172     | 4.574379851 | #NUM!                     | 0           |
| PA                   | Acupuncturist         | TRAZODONE HCL                  | 56.08000183          | 15                     | 15                           | 143.6571226  | 0.000214 | 10000                      | 4.57426265 | 0.487967467      | 192.3488872 | 0.0001172     | 4.574379851 | -264.56606736594100000000 | 1.2601E-115 |
| PA                   | Addiction Medicine    | AMITRIPTYLINE HCL              | 287.25               | 13                     | 13                           | 9.681320117  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -107.14026204537100000000 | 2.94833E-47 |
| PA                   | Addiction Medicine    | AMLODIPINE BESYLATE            | 463.4599915          | 46                     | 46                           | 55.27150265  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -88.98524560976470000000  | 2.26047E-39 |
| PA                   | Addiction Medicine    | AMLODIPINE BESYLATE-BENAZEPRIL | 719.6199951          | 13                     | 13                           | 24.25368681  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -101.14333438075000000000 | 1.18579E-44 |
| PA                   | Addiction Medicine    | ATORVASTATIN CALCIUM           | 690.0100098          | 21                     | 21                           | 37.56694478  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -95.82381399681150000000  | 2.4224E-42  |
| PA                   | Addiction Medicine    | BACLOFEN                       | 223.3800049          | 12                     | 12                           | 6.949550785  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -108.28473705934300000000 | 9.3872E-48  |
| PA                   | Addiction Medicine    | BUPRENORPHINE HCL              | 663.8499756          | 13                     | 13                           | 22.37404394  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -101.90662489338000000000 | 5.52733E-45 |
| PA                   | Addiction Medicine    | BUPRENORPHINE-NALOXONE         | 10755.28992          | 57                     | 57                           | 1589.381553  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -517.61737094182700000000 | 1.5909E-225 |
| PA                   | Addiction Medicine    | BUPROPION XL                   | 608.6599731          | 13                     | 13                           | 20.51394967  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -102.66496099991900000000 | 2.58925E-45 |
| PA                   | Addiction Medicine    | BUSPIRONE HCL                  | 204.1199951          | 14                     | 14                           | 7.408747194  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -108.09190904033100000000 | 1.13836E-47 |
| PA                   | Addiction Medicine    | CARBIDOPA-LEVODOPA             | 376.8399963          | 16                     | 16                           | 15.63177029  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -104.66948164250900000000 | 3.48836E-46 |
| PA                   | Addiction Medicine    | CELECOXIB                      | 2600.75              | 11                     | 17                           | 45.17566983  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -92.85191441201340000000  | 4.73071E-41 |
| PA                   | Addiction Medicine    | CITALOPRAM HBR                 | 403.5799942          | 59                     | 59                           | 61.73235318  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -86.55664798436220000000  | 2.56404E-38 |
| PA                   | Addiction Medicine    | CLONAZEPAM                     | 524.2300034          | 88                     | 92                           | 119.6012549  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -66.40092533376090000000  | 1.4536E-29  |
| PA                   | Addiction Medicine    | CLONIDINE                      | 3023.120117          | 16                     | 16                           | 125.4026103  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -64.53876022138530000000  | 9.35777E-29 |
| PA                   | Addiction Medicine    | CLONIDINE HCL                  | 577.7099991          | 76                     | 79.70000076                  | 113.8294579  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -68.28225593627810000000  | 2.2151E-30  |
| PA                   | Addiction Medicine    | CLOPIDOGREL                    | 329.5800018          | 30                     | 44                           | 17.43099627  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -103.92837772680400000000 | 7.31946E-46 |
| PA                   | Addiction Medicine    | CLOTRIMAZOLE-BETAMETHASONE     | 689.0900269          | 30                     | 30                           | 53.59551026  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -89.62109159612870000000  | 1.19689E-39 |
| PA                   | Addiction Medicine    | DIGOX                          | 745.0800171          | 30                     | 30                           | 57.95025634  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -87.97397116819310000000  | 6.21427E-39 |
| PA                   | Addiction Medicine    | DILTIAZEM HCL                  | 232.5                | 12                     | 12                           | 7.233281951  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -108.16556991429100000000 | 1.05752E-47 |
| PA                   | Addiction Medicine    | DIVALPROEX SODIUM              | 822.1599731          | 15                     | 15                           | 31.97266072  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -98.04057439079470000000  | 2.63949E-43 |
| PA                   | Addiction Medicine    | DIVALPROEX SODIUM ER           | 4164.400146          | 31                     | 31                           | 334.6920046  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -16.66940216256570000000  | 5.76197E-08 |
| PA                   | Addiction Medicine    | DOXEPIN HCL                    | 394.8500061          | 11                     | 11                           | 11.26045722  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -106.48160253837600000000 | 5.69676E-47 |
| PA                   | Addiction Medicine    | DULOXETINE HCL                 | 1563.420044          | 31                     | 31                           | 125.6517554  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -64.45943429861570000000  | 1.01303E-28 |
| PA                   | Addiction Medicine    | ENDOCET                        | 3276.280029          | 29                     | 29                           | 246.3259709  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -32.29714586916190000000  | 9.40866E-15 |
| PA                   | Addiction Medicine    | ESCITALOPRAM OXALATE           | 209.0899963          | 25                     | 25                           | 13.55203312  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -105.52958902894300000000 | 1.47598E-46 |
| PA                   | Addiction Medicine    | EXELON                         | 6071.359863          | 13                     | 13                           | 204.6258603  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -41.99847418411000000000  | 5.7583E-19  |
| PA                   | Addiction Medicine    | FAMOTIDINE                     | 72.29000092          | 13                     | 13                           | 2.436423464  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -110.18953782985000000000 | 1.39731E-48 |
| PA                   | Addiction Medicine    | FENTANYL                       | 2837.429932          | 15                     | 15                           | 110.3437135  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -69.43228458577720000000  | 7.01362E-31 |
| PA                   | Addiction Medicine    | FUROSEMIDE                     | 660.7000122          | 104                    | 104                          | 178.1430341  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -48.93413259190420000000  | 5.59985E-22 |
| PA                   | Addiction Medicine    | GABAPENTIN                     | 4398.100021          | 176                    | 190.7999992                  | 2006.822495  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | #NUM!                     | 0           |
| PA                   | Addiction Medicine    | HEPARIN SODIUM                 | 416                  | 26                     | 26                           | 28.04128229  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -99.61446489861500000000  | 5.47E-44    |
| PA                   | Addiction Medicine    | HUMULIN R                      | 567.7600098          | 15                     | 15                           | 22.07939909  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -102.02654988876900000000 | 4.90267E-45 |
| PA                   | Addiction Medicine    | HYDRALAZINE HCL                | 305.8299866          | 29                     | 29                           | 22.99372084  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -101.65464988899600000000 | 7.11126E-45 |
| PA                   | Addiction Medicine    | HYDROCHLOROTHIAZIDE            | 76.19000244          | 14                     | 16                           | 2.686383966  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -110.08358293385200000000 | 1.55349E-48 |
| PA                   | Addiction Medicine    | HYDROCODONE-ACETAMINOPHEN      | 252.5700073          | 26                     | 26                           | 17.02496844  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -104.09537855537600000000 | 6.19372E-46 |
| PA                   | Addiction Medicine    | HYDROMORPHONE HCL              | 1131.819946          | 36                     | 36.29999924                  | 105.6357779  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -71.00208985974990000000  | 1.45943E-31 |
| PA                   | Addiction Medicine    | IBUPROFEN                      | 121.7600021          | 22                     | 22                           | 6.944780419  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -108.28674120458000000000 | 9.36841E-48 |
| PA                   | Addiction Medicine    | IPRATROPIUM-ALBUTEROL          | 156.7299957          | 11                     | 11                           | 4.469675535  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -109.32922496085200000000 | 3.30309E-48 |
| PA                   | Addiction Medicine    | ISOSORBIDE MONONITRATE ER      | 313.4099884          | 28                     | 28                           | 22.75108199  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -101.75327323293800000000 | 6.4434E-45  |
| PA                   | Addiction Medicine    | KLOR-CON M20                   | 228.8000031          | 12                     | 16                           | 5.694537402  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -108.81267130299900000000 | 5.53678E-48 |
| PA                   | Addiction Medicine    | LAMOTRIGINE                    | 131.6300049          | 13                     | 14.19999981                  | 4.436387169  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -109.34328142950800000000 | 3.25699E-48 |
| PA                   | Addiction Medicine    | LANTUS                         | 11257.62012          | 29                     | 29                           | 846.4002408  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -57.88933170781820000000  | 7.22741E-26 |
| PA                   | Addiction Medicine    | LEVETIRACETAM                  | 1069.910034          | 28                     | 28                           | 77.66699151  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -80.71998570886950000000  | 8.78528E-36 |
| PA                   | Addiction Medicine    | LEVOTHYROXINE SODIUM           | 1559.76001           | 94                     | 94                           | 380.1165911  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -11.24289102422800000000  | 1.31001E-05 |
| PA                   | Addiction Medicine    | LISINOPRIL                     | 578.1899872          | 85                     | 123.6999969                  | 87.94635932  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -77.07036772420140000000  | 3.37882E-34 |
| PA                   | Addiction Medicine    | LORAZEPAM                      | 331.159996           | 85                     | 85                           | 72.97733258  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -82.41512907759560000000  | 1.61274E-36 |
| PA                   | Addiction Medicine    | LOSARTAN POTASSIUM             | 78.30999756          | 11                     | 20                           | 0.81209791   | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -110.87937353132500000000 | 7.00971E-49 |
| PA                   | Addiction Medicine    | METFORMIN HCL                  | 190.4499969          | 27                     | 37                           | 10.2207439   | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -106.91502732178500000000 | 3.69312E-47 |
| PA                   | Addiction Medicine    | METHADONE HCL                  | 2083.800049          | 30                     | 30                           | 162.0721858  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -53.43632244906780000000  | 6.20726E-24 |
| PA                   | Addiction Medicine    | METHOCARBAMOL                  | 324.3999939          | 11                     | 11                           | 9.251341516  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -107.31997707257800000000 | 2.46335E-47 |
| PA                   | Addiction Medicine    | METOCLOPRAMIDE HCL             | 212.8300018          | 20                     | 20                           | 11.03555133  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -106.57528033761600000000 | 5.18733E-47 |
| PA                   | Addiction Medicine    | METOPROLOL SUCCINATE           | 644.9099731          | 17                     | 43                           | -7.858291629 | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -114.59989431045700000000 | 1.69785E-50 |
| PA                   | Addiction Medicine    | METOPROLOL TARTRATE            | 690.0400238          | 88                     | 98                           | 157.0724393  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -54.88217290272830000000  | 1.46209E-24 |
| PA                   | Addiction Medicine    | MIRTAZAPINE                    | 204.0899963          | 12                     | 13                           | 6.349421449  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -108.53701902945800000000 | 7.2941E-48  |
| PA                   | Addiction Medicine    | MORPHINE SULFATE               | 905.1500244          | 22                     | 22                           | 51.62670873  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -90.37110304806240000000  | 5.65365E-40 |
| PA                   | Addiction Medicine    | MORPHINE SULFATE ER            | 27749.5              | 93                     | 93                           | 6690.66525   | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | #NUM!                     | 0           |
| PA                   | Addiction Medicine    | NEXIUM                         | 3195.800049          | 32                     | 32                           | 265.1311577  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -28.41028329315070000000  | 4.58744E-13 |
| PA                   | Addiction Medicine    | NORTRIPTYLINE HCL              | 65.48999786          | 12                     | 12                           | 2.037452127  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -110.35876747469000000000 | 1.17977E-48 |
| PA                   | Addiction Medicine    | NOVOLIN R                      | 2300.909912          | 18                     | 18                           | 107.3750332  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -70.41994100648930000000  | 2.61221E-31 |
| PA                   | Addiction Medicine    | NYSTATIN                       | 2828.669922          | 32                     | 32                           | 234.6731709  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -34.85792028986420000000  | 7.26771E-16 |
| PA                   | Addiction Medicine    | OLANZAPINE                     | 237.1000061          | 22                     | 22                           | 13.52338577  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -105.54146249508700000000 | 1.45856E-46 |
| PA                   | Addiction Medicine    | OMEPRAZOLE                     | 977.8900223          | 97                     | 99                           | 245.9194846  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -32.38451276885740000000  | 8.62154E-15 |
| PA                   | Addiction Medicine    | OXYCODONE HCL                  | 13701.55984          | 243                    | 243.5                        | 8631.921385  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | #NUM!                     | 0           |
| PA                   | Addiction Medicine    | OXYCODONE-ACETAMINOPHEN        | 356.6199951          | 44                     | 44                           | 40.68080676  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -94.60156366481440000000  | 8.22362E-42 |
| PA                   | Addiction Medicine    | OXYCONTIN                      | 148300.0313          | 158                    | 158                          | 60747.65535  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | #NUM!                     | 0           |
| PA                   | Addiction Medicine    | PENTAZOCINE-NALOXONE HCL       | 2311.080078          | 12                     | 12                           | 71.89975835  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -82.80729928530540000000  | 1.08955E-36 |
| PA                   | Addiction Medicine    | PHENOBARBITAL                  | 373.980011           | 23                     | 23                           | 22.30013113  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -101.93670154837200000000 | 5.36356E-45 |
| PA                   | Addiction Medicine    | PIOGLITAZONE-METFORMIN         | 1271.589966          | 12                     | 12                           | 39.56029569  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -95.04042756506290000000  | 5.30233E-42 |
| PA                   | Addiction Medicine    | POTASSIUM CHLORIDE             | 2375.01001           | 51                     | 51                           | 314.0268706  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -19.72394910157640000000  | 2.71643E-09 |
| PA                   | Addiction Medicine    | PRAVASTATIN SODIUM             | 623.1300049          | 19                     | 35                           | 10.50081993  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -106.79818089934900000000 | 4.15087E-47 |
| PA                   | Addiction Medicine    | QUETIAPINE FUMARATE            | 2385.500031          | 57                     | 57                           | 352.5213892  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -14.32841942618600000000  | 5.98751E-07 |
| PA                   | Addiction Medicine    | RANEXA                         | 2942.929932          | 26                     | 26                           | 198.3738677  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -43.58156954960220000000  | 1.1824E-19  |
| PA                   | Addiction Medicine    | RANITIDINE HCL                 | 251.5699997          | 29                     | 37                           | 16.10968295  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -104.47235781640100000000 | 4.24846E-46 |
| PA                   | Addiction Medicine    | RISPERIDONE                    | 77.66000366          | 18                     | 18                           | 3.62410776   | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -109.68657216305000000000 | 2.31061E-48 |
| PA                   | Addiction Medicine    | SERTRALINE HCL                 | 335.1199951          | 48                     | 48                           | 41.70352537  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -94.20194179282710000000  | 1.22636E-41 |
| PA                   | Addiction Medicine    | SIMVASTATIN                    | 326.5999985          | 41                     | 58.5                         | 24.85166409  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -100.90114165653700000000 | 1.51074E-44 |
| PA                   | Addiction Medicine    | SUBOXONE                       | 96600.85205          | 333                    | 333                          | 83398.14318  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | #NUM!                     | 0           |
| PA                   | Addiction Medicine    | TAMSULOSIN HCL                 | 334.7799988          | 11                     | 17                           | 5.815211263  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -108.76184988886900000000 | 5.82544E-48 |
| PA                   | Addiction Medicine    | TIZANIDINE HCL                 | 1877.609985          | 39                     | 39                           | 189.8458833  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -45.79504629475280000000  | 1.2926E-20  |
| PA                   | Addiction Medicine    | TOBRAMYCIN-DEXAMETHASONE       | 849.1699829          | 16                     | 16                           | 35.2245787   | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -96.74871972290440000000  | 9.60649E-43 |
| PA                   | Addiction Medicine    | TOPIRAMATE                     | 70.27999878          | 14                     | 16                           | 2.478003095  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -110.17190906676700000000 | 1.42216E-48 |
| PA                   | Addiction Medicine    | TRAMADOL HCL                   | 159.8600006          | 40                     | 40                           | 16.57795637  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -104.27939993094100000000 | 5.15267E-46 |
| PA                   | Addiction Medicine    | TRAZODONE HCL                  | 321.7900009          | 47                     | 51                           | 39.21042898  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -95.17767844814650000000  | 4.62231E-42 |
| PA                   | Addiction Medicine    | VENLAFAXINE HCL ER             | 264.8500061          | 29                     | 30.10000038                  | 19.9126553   | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -102.91073571180600000000 | 2.02505E-45 |
| PA                   | Addiction Medicine    | WARFARIN SODIUM                | 1434.530029          | 175                    | 175.3999939                  | 650.846964   | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -15.61649957443480000000  | 1.65135E-07 |
| PA                   | Addiction Medicine    | ZOLPIDEM TARTRATE              | 212.6900024          | 41                     | 45.59999847                  | 22.58594309  | 0.000214 | 10000                      | 4.57426265 | 32.14308547      | 496.6513866 | 1161.044173   | 1165.618435 | -101.82042471705200000000 | 6.02492E-45 |
| PA                   | Allergy/Immunology    | ACETAMINOPHEN-CODEINE          | 183.3000031          | 13                     | 13                           | 1.191672923  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529707508994000000   | 0.004403758 |
| PA                   | Allergy/Immunology    | ADVAIR DISKUS                  | 2575496.683          | 5622                   | 8478                         | 4654535.589  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | #NUM!                     | 0           |
| PA                   | Allergy/Immunology    | ADVAIR HFA                     | 545572.0286          | 1224                   | 1685                         | 254147.7107  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -10.12770130938320000000  | 3.99572E-05 |
| PA                   | Allergy/Immunology    | AEROSPAN                       | 4332.209961          | 16                     | 26                           | 18.63198795  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42526973326208000000   | 0.004403878 |
| PA                   | Allergy/Immunology    | ALBUTEROL SULFATE              | 3753.310043          | 67                     | 74.80000019                  | 125.1587675  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42510377024560000000   | 0.004404609 |
| PA                   | Allergy/Immunology    | ALENDRONATE SODIUM             | 339.7200012          | 27                     | 63.40000153                  | -0.519869077 | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529976092655000000   | 0.004403746 |
| PA                   | Allergy/Immunology    | ALLOPURINOL                    | 1324.580017          | 57                     | 143                          | -9.737484526 | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42531423362220000000   | 0.004403682 |
| PA                   | Allergy/Immunology    | ALPHAGAN P                     | 2905.469971          | 20                     | 22.10000038                  | 29.06013542  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42525340762119000000   | 0.00440395  |
| PA                   | Allergy/Immunology    | ALPRAZOLAM                     | 1184.989965          | 203                    | 220.2000008                  | 120.2989835  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42511130250770000000   | 0.004404576 |
| PA                   | Allergy/Immunology    | ALVESCO                        | 59664.62891          | 174                    | 225                          | 4341.413816  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.41997450981115000000   | 0.004427259 |
| PA                   | Allergy/Immunology    | AMIODARONE HCL                 | 224.5399933          | 23                     | 27                           | 2.436714716  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529512159932000000   | 0.004403766 |
| PA                   | Allergy/Immunology    | AMITRIPTYLINE HCL              | 678.230011           | 28                     | 44                           | 5.562526563  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529021822923000000   | 0.004403788 |
| PA                   | Allergy/Immunology    | AMLODIPINE BESYLATE            | 2342.389946          | 301                    | 619                          | 52.59649248  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42521662374011000000   | 0.004404112 |
| PA                   | Allergy/Immunology    | AMOXICILLIN                    | 2125.140015          | 283                    | 342.7999992                  | 273.641711   | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42487543222217000000   | 0.004405615 |
| PA                   | Allergy/Immunology    | AMOXICILLIN-CLAVULANATE POTASS | 7388.679855          | 451                    | 451                          | 1666.459019  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42290309363865000000   | 0.004414313 |
| PA                   | Allergy/Immunology    | ANORO ELLIPTA                  | 16640.55029          | 44                     | 63                           | 260.4733337  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42489554190946000000   | 0.004405526 |
| PA                   | Allergy/Immunology    | ARNUITY ELLIPTA                | 2807.699951          | 12                     | 16                           | 13.47948111  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42527780603761000000   | 0.004403843 |
| PA                   | Allergy/Immunology    | ASMANEX                        | 247964.2513          | 883                    | 1378.5                       | 65146.19534  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.65824725393468000000   | 0.003488626 |
| PA                   | Allergy/Immunology    | ASTEPRO                        | 7083.390015          | 42                     | 46.5                         | 148.7790195  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42506721394328000000   | 0.00440477  |
| PA                   | Allergy/Immunology    | ATENOLOL                       | 491.7800064          | 58                     | 126                          | 0.639433615  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529794164070000000   | 0.004403754 |
| PA                   | Allergy/Immunology    | ATORVASTATIN CALCIUM           | 11011.12982          | 493                    | 984.4000244                  | 550.879635   | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42445841535339000000   | 0.004407453 |
| PA                   | Allergy/Immunology    | ATROVENT HFA                   | 24226.03003          | 82                     | 95.59999847                  | 944.5073376  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42388718251077000000   | 0.004409971 |
| PA                   | Allergy/Immunology    | AVODART                        | 1913.859985          | 12                     | 12                           | 11.48530824  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42528093156657000000   | 0.004403829 |
| PA                   | Allergy/Immunology    | AZELASTINE HCL                 | 348484.7612          | 4477                   | 6253.799982                  | 579565.3869  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -31.04399754882880000000  | 3.2943E-14  |
| PA                   | Allergy/Immunology    | AZITHROMYCIN                   | 14402.83015          | 1461                   | 1487.200001                  | 10523.2358   | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.41753064667291000000   | 0.004438092 |
| PA                   | Allergy/Immunology    | BACLOFEN                       | 520.75               | 12                     | 12                           | 3.125084548  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529404164108000000   | 0.004403771 |
| PA                   | Allergy/Immunology    | BD ULTRA-FINE PEN NEEDLE       | 524.8099976          | 15                     | 18.29999924                  | 3.55100403   | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529337346908000000   | 0.004403774 |
| PA                   | Allergy/Immunology    | BENICAR                        | 9252.759766          | 32                     | 50                           | 87.91766281  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42516158610839000000   | 0.004404354 |
| PA                   | Allergy/Immunology    | BERINERT                       | 2483763.25           | 30                     | 30                           | 37263.41889  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.47648647741441000000   | 0.004184004 |
| PA                   | Allergy/Immunology    | BETAMETHASONE DIPROPIONATE     | 1974.099945          | 23                     | 23                           | 22.70639661  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42526335258386000000   | 0.004403906 |
| PA                   | Allergy/Immunology    | BISOPROLOL-HYDROCHLOROTHIAZIDE | 64.20999908          | 11                     | 11                           | 0.353221065  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529839077237000000   | 0.004403752 |
| PA                   | Allergy/Immunology    | BREO ELLIPTA                   | 76855.85889          | 201                    | 284.5                        | 5609.605581  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.41898101277348000000   | 0.00443166  |
| PA                   | Allergy/Immunology    | BRIMONIDINE TARTRATE           | 118.9899979          | 11                     | 11                           | 0.654567425  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529791789271000000   | 0.004403754 |
| PA                   | Allergy/Immunology    | BUDESONIDE                     | 25063.20007          | 136                    | 165                          | 1547.942148  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42305899517089000000   | 0.004413625 |
| PA                   | Allergy/Immunology    | BUMETANIDE                     | 2486.689941          | 65                     | 113                          | 35.19324558  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42524381403796000000   | 0.004403992 |
| PA                   | Allergy/Immunology    | BUPROPION HCL SR               | 378.3699951          | 22                     | 22                           | 4.162848609  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529241367250000000   | 0.004403778 |
| PA                   | Allergy/Immunology    | BUPROPION XL                   | 1244.099976          | 27                     | 37                           | 12.87884374  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42527874736907000000   | 0.004403838 |
| PA                   | Allergy/Immunology    | BUTALBITAL-ACETAMINOPHEN-CAFFE | 1021.76001           | 13                     | 17                           | 5.467438739  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529036736797000000   | 0.004403787 |
| PA                   | Allergy/Immunology    | BYSTOLIC                       | 49796.08057          | 292                    | 449                          | 4479.995432  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.41985358207066000000   | 0.004427795 |
| PA                   | Allergy/Immunology    | CARAFATE                       | 9288.120117          | 32                     | 34                           | 148.6377246  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42506743235878000000   | 0.004404769 |
| PA                   | Allergy/Immunology    | CARISOPRODOL                   | 300.6399994          | 25                     | 25                           | 3.758703059  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529304764634000000   | 0.004403775 |
| PA                   | Allergy/Immunology    | CARTIA XT                      | 1358.75              | 33                     | 69                           | 2.650058194  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529478688454000000   | 0.004403768 |
| PA                   | Allergy/Immunology    | CARVEDILOL                     | 95.52999878          | 20                     | 28                           | 0.707054246  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529783553065000000   | 0.004403754 |
| PA                   | Allergy/Immunology    | CEFADROXIL                     | 134.8200073          | 15                     | 15                           | 1.011339226  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529735805672000000   | 0.004403756 |
| PA                   | Allergy/Immunology    | CEFDINIR                       | 5617.300049          | 124                    | 124                          | 348.3377597  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42476188075961000000   | 0.004406115 |
| PA                   | Allergy/Immunology    | CEFPROZIL                      | 1417.819946          | 39                     | 39                           | 27.65266139  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42525561006980000000   | 0.00440394  |
| PA                   | Allergy/Immunology    | CEFUROXIME                     | 6250.040039          | 111                    | 111                          | 346.9421178  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42476399430749000000   | 0.004406106 |
| PA                   | Allergy/Immunology    | CELECOXIB                      | 12114.78955          | 65                     | 103                          | 225.9830953  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42494834244887000000   | 0.004405294 |
| PA                   | Allergy/Immunology    | CEPHALEXIN                     | 389.6600037          | 58                     | 58                           | 11.3022542   | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42528121850433000000   | 0.004403828 |
| PA                   | Allergy/Immunology    | CINRYZE                        | 3285513.625          | 41                     | 41                           | 67365.63008  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.67799166327362000000   | 0.003420421 |
| PA                   | Allergy/Immunology    | CIPROFLOXACIN HCL              | 556.6100082          | 39                     | 39                           | 10.85592577  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42528191814765000000   | 0.004403824 |
| PA                   | Allergy/Immunology    | CITALOPRAM HBR                 | 51.68000031          | 17                     | 27                           | 0.250694894  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529855166208000000   | 0.004403751 |
| PA                   | Allergy/Immunology    | CLARINEX                       | 10985.51978          | 34                     | 58                           | 86.80184254  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42516332177100000000   | 0.004404347 |
| PA                   | Allergy/Immunology    | CLARINEX-D 12 HOUR             | 3806.030029          | 16                     | 22                           | 23.2211267   | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42526254668361000000   | 0.00440391  |
| PA                   | Allergy/Immunology    | CLARITHROMYCIN                 | 3198.850006          | 45                     | 55                           | 64.7888314   | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42519760330284000000   | 0.004404196 |
| PA                   | Allergy/Immunology    | CLARITHROMYCIN ER              | 4754.559937          | 48                     | 48                           | 114.1307867  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42512086807322000000   | 0.004404534 |
| PA                   | Allergy/Immunology    | CLEMASTINE FUMARATE            | 1250.039993          | 54                     | 54.30000019                  | 33.75739415  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42524605950444000000   | 0.004403982 |
| PA                   | Allergy/Immunology    | CLINDAMYCIN HCL                | 101.7399979          | 12                     | 12                           | 0.610554192  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529798695840000000   | 0.004403754 |
| PA                   | Allergy/Immunology    | CLOBETASOL PROPIONATE          | 4501.009888          | 26                     | 26                           | 58.52407549  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42520737358889000000   | 0.004404153 |
| PA                   | Allergy/Immunology    | CLONAZEPAM                     | 607.5900269          | 89                     | 106                          | 25.09816273  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42525960820899000000   | 0.004403923 |
| PA                   | Allergy/Immunology    | CLOPIDOGREL                    | 950.120018           | 90                     | 160.5                        | 16.89154289  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42527245966423000000   | 0.004403866 |
| PA                   | Allergy/Immunology    | CLOTRIMAZOLE-BETAMETHASONE     | 1030.569946          | 18                     | 20                           | 9.276864758  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42528439366153000000   | 0.004403814 |
| PA                   | Allergy/Immunology    | COMBIVENT RESPIMAT             | 50593.54028          | 151                    | 172                          | 3724.381212  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42054976113155000000   | 0.004424713 |
| PA                   | Allergy/Immunology    | CRESTOR                        | 45959.61865          | 110                    | 218                          | 547.0217826  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42446413497910000000   | 0.004407427 |
| PA                   | Allergy/Immunology    | CROMOLYN SODIUM                | 4201.489861          | 67                     | 72.20000076                  | 140.7762426  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42507958971219000000   | 0.004404716 |
| PA                   | Allergy/Immunology    | CYCLOSPORINE MODIFIED          | 1055.150024          | 15                     | 15                           | 7.915105707  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42528652882467000000   | 0.004403804 |
| PA                   | Allergy/Immunology    | DANAZOL                        | 2506.080109          | 23                     | 23                           | 28.82531304  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42525377505505000000   | 0.004403948 |
| PA                   | Allergy/Immunology    | DESLORATADINE                  | 42142.12988          | 570                    | 1154                         | 2137.005713  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42230601001211000000   | 0.004416949 |
| PA                   | Allergy/Immunology    | DESOXIMETASONE                 | 4584.72998           | 11                     | 11                           | 25.22073244  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42525941634711000000   | 0.004403924 |
| PA                   | Allergy/Immunology    | DEXILANT                       | 22245.09033          | 85                     | 97                           | 920.0065728  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42392202377709000000   | 0.004409817 |
| PA                   | Allergy/Immunology    | DICLOFENAC SODIUM              | 158.8300018          | 14                     | 14                           | 1.112018016  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529720007811000000   | 0.004403757 |
| PA                   | Allergy/Immunology    | DICYCLOMINE HCL                | 471.3299866          | 60                     | 96                           | 7.919825181  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42528652142429000000   | 0.004403804 |
| PA                   | Allergy/Immunology    | DILTIAZEM 24HR CD              | 1835.319946          | 26                     | 58                           | -0.183566331 | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529923314707000000   | 0.004403748 |
| PA                   | Allergy/Immunology    | DILTIAZEM 24HR ER              | 1029.619995          | 41                     | 69                           | 10.24663559  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42528287329289000000   | 0.00440382  |
| PA                   | Allergy/Immunology    | DILTIAZEM ER                   | 1680.02002           | 40                     | 78                           | 8.233638201  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42528602935703000000   | 0.004403806 |
| PA                   | Allergy/Immunology    | DIPHENHYDRAMINE HCL            | 34.52999878          | 12                     | 12                           | 0.207218753  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529861988773000000   | 0.004403751 |
| PA                   | Allergy/Immunology    | DIVALPROEX SODIUM ER           | 1063.599976          | 20                     | 20                           | 10.6379896   | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42528225978551000000   | 0.004403823 |
| PA                   | Allergy/Immunology    | DONEPEZIL HCL                  | 148.7100067          | 12                     | 28                           | -0.089242697 | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529908512271000000   | 0.004403749 |
| PA                   | Allergy/Immunology    | DORZOLAMIDE HCL                | 486.6199951          | 14                     | 16.10000038                  | 3.287732955  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529378647888000000   | 0.004403772 |
| PA                   | Allergy/Immunology    | DORZOLAMIDE-TIMOLOL            | 219.2700043          | 11                     | 23.70000076                  | 0.073469119  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529882977842000000   | 0.00440375  |
| PA                   | Allergy/Immunology    | DOXAZOSIN MESYLATE             | 922.25               | 13                     | 27                           | 0.784059159  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529771469570000000   | 0.004403755 |
| PA                   | Allergy/Immunology    | DOXEPIN HCL                    | 6780.550003          | 272                    | 326.5                        | 848.2359545  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42402463074052000000   | 0.004409365 |
| PA                   | Allergy/Immunology    | DOXYCYCLINE HYCLATE            | 14485.86993          | 240                    | 256.1000004                  | 1738.629603  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42280924463834000000   | 0.004414727 |
| PA                   | Allergy/Immunology    | DULERA                         | 545318.4792          | 1622                   | 2171.899998                  | 351602.599   | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -14.63706679254680000000  | 4.39747E-07 |
| PA                   | Allergy/Immunology    | DULOXETINE HCL                 | 3972.189941          | 47                     | 53                           | 91.9734008   | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42515527904919000000   | 0.004404382 |
| PA                   | Allergy/Immunology    | DYMISTA                        | 214006.13            | 916                    | 1316.299997                  | 69279.25304  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.69564016885989000000   | 0.003360585 |
| PA                   | Allergy/Immunology    | ELIQUIS                        | 6874.950195          | 19                     | 21                           | 65.32424578  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42519676857754000000   | 0.004404199 |
| PA                   | Allergy/Immunology    | ENALAPRIL MALEATE              | 3055.140015          | 84                     | 176                          | 14.66741562  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42527594444957000000   | 0.004403851 |
| PA                   | Allergy/Immunology    | ENALAPRIL-HYDROCHLOROTHIAZIDE  | 314.2600098          | 29                     | 45                           | 2.750289528  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529462963399000000   | 0.004403768 |
| PA                   | Allergy/Immunology    | ENBREL                         | 115449.6719          | 29                     | 36.20000076                  | 1467.639812  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42316588879668000000   | 0.004413153 |
| PA                   | Allergy/Immunology    | ENDOCET                        | 4863.890137          | 60                     | 60                           | 145.944003   | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42507159695303000000   | 0.004404751 |
| PA                   | Allergy/Immunology    | EPINASTINE HCL                 | 758.0999756          | 13                     | 13                           | 4.928571733  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529121257096000000   | 0.004403784 |
| PA                   | Allergy/Immunology    | EPIPEN 2-PAK                   | 453053.397           | 966                    | 1003.600002                  | 218865.7297  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -8.86502652133525000000   | 0.000141243 |
| PA                   | Allergy/Immunology    | ERYTHROMYCIN                   | 4328.52002           | 11                     | 11                           | 23.81131402  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42526162269332000000   | 0.004403914 |
| PA                   | Allergy/Immunology    | ESCITALOPRAM OXALATE           | 2156.530029          | 149                    | 252                          | 76.78683205  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42517890896154000000   | 0.004404278 |
| PA                   | Allergy/Immunology    | ESZOPICLONE                    | 1011.969971          | 11                     | 11                           | 5.566876125  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529021140727000000   | 0.004403788 |
| PA                   | Allergy/Immunology    | FAMOTIDINE                     | 4226.859978          | 439                    | 610.5999985                  | 694.3070874  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42424743916564000000   | 0.004408382 |
| PA                   | Allergy/Immunology    | FELODIPINE ER                  | 603.8800049          | 17                     | 25                           | 3.472959647  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529349590104000000   | 0.004403773 |
| PA                   | Allergy/Immunology    | FENOFIBRATE                    | 947.6200104          | 23                     | 37                           | 6.018512831  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42528950306583000000   | 0.004403791 |
| PA                   | Allergy/Immunology    | FENTANYL                       | 923.3599854          | 18                     | 18                           | 8.311794593  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42528590680813000000   | 0.004403807 |
| PA                   | Allergy/Immunology    | FIRAZYR                        | 2557538.25           | 62                     | 62                           | 79298.51857  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.79748590965720000000   | 0.003035176 |
| PA                   | Allergy/Immunology    | FLOVENT DISKUS                 | 40897.39087          | 195                    | 263.7999992                  | 3120.645681  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42117082381210000000   | 0.004421966 |
| PA                   | Allergy/Immunology    | FLOVENT HFA                    | 405845.7883          | 1438                   | 2003.000008                  | 217837.8876  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -8.83118951298639000000   | 0.000146104 |
| PA                   | Allergy/Immunology    | FLUCONAZOLE                    | 2938.309998          | 67                     | 85.5                         | 83.8309733   | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42516794392816000000   | 0.004404326 |
| PA                   | Allergy/Immunology    | FLUNISOLIDE                    | 5192.419876          | 93                     | 123.1999998                  | 195.0637777  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42499583604980000000   | 0.004405085 |
| PA                   | Allergy/Immunology    | FLUOCINOLONE ACETONIDE         | 2745.290039          | 36                     | 40                           | 49.42446557  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42522157605224000000   | 0.00440409  |
| PA                   | Allergy/Immunology    | FLUOCINONIDE                   | 556.9400024          | 11                     | 11                           | 3.063743088  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529413787442000000   | 0.004403771 |
| PA                   | Allergy/Immunology    | FLUTICASONE PROPIONATE         | 139955.3098          | 8950                   | 13288.89999                  | 415744.3359  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -18.42359729544540000000  | 9.97088E-09 |
| PA                   | Allergy/Immunology    | FORADIL                        | 30234.54956          | 91                     | 137                          | 887.5500462  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42396832452000000000   | 0.004409613 |
| PA                   | Allergy/Immunology    | FUROSEMIDE                     | 164.3800049          | 62                     | 78.70000076                  | 4.370859944  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42529208737995000000   | 0.00440378  |
| PA                   | Allergy/Immunology    | GABAPENTIN                     | 2172.509998          | 148                    | 200                          | 126.0291537  | 0.000214 | 10000                      | 4.57426265 | 166.6354916      | 9935.131398 | 6330921135    | 6330921139  | -5.42510242161312000000   | 0.004404615 |

#### Quality Checks

- ATORVASTATIN CALCIUM drug is found to have 0 drug cost for 'PA', 'Dentist', '1578621165'
    When 0 drug cost is obtained, it may be due to a carry over from previous plan
    So claim per dollar is equated to -ve number of cost_per_claim
    Or it is taken as an aggregate over the drug

- GABAPENTIN and TRAZON+DONE HCL in PA had 0 values for drug risk
    implying others were zero when compared to total claim count
    So that has been equated to 1.01 as the specialty was Acupuncturist

- Set claim_per_cost = total_claim_count when cost = 0 (Divide by zero Error)

#### Preprocessing

| Name                  | Problem Domain                           | Dataset Type                             | Data Type                             | Preprocessing                            | Related Technique and Usage              |
| --------------------- | ---------------------------------------- | ---------------------------------------- | ------------------------------------- | ---------------------------------------- | ---------------------------------------- |
| Sigh Value  Mapping   | Death Evaluation                         | Drug Overdose                            | Death Number                          | Death Ratio calculation using a  factor of 10000, Addition of Variance | Decision Tree Evaluation, A  Noise Covariance, calculated using Numpy Library |
| 4 Factors  Evaluation | Data Stewardship for Assessments         | Prescriber Info                          | Unit Values, Probabilistic and  Ratio | Cost and Supply quantized,  Aggregation of Ratio, Average taken over Drug and Specialty, Quality Checks | Outlier Detection, Decision Node  sgn Function, Central Tendency Estimation, Hyperbolic Function, Health Risk  Entropy |
| Linear  Regression    | Model Evaluation and Validation          | Health, Safety                           | Observation, Intangible Quantities    | Debiasing Distribution,  Assessment on Formulaic Metrics | Linear Regression, Metric  Accuracy, Coefficient of determination, Explained Variance Score |
| Cost  Function        | Classification Cost, Feature  Selection  | Health, Safety                           | Unit Values, Intangible Quantities    | Aggregated, Partitioned Data,  Debiasing Distribution, Seggregation by Unit Values and Intangible Quantities | Gradient Boosting,  Stochastic Gradient Descent Classifier |
| Canberra  Distance    | Optimization                             | Cluster, Batch, Prescription Data        | Factors, Latent Variables             | Seggregation by Observation  Quantities and Intangible Quantities | K-Means Clustering, Metric  Evaluation   |
| MMSE                  | Optimization                             | Cluster, Batch, Prescription Data        | Observed, Measured Quantities         | Covariance on Irregular Data,  Probabilistic Evaluation of Observation Over Measurement | Minimum Mean Square Estimation,  Bayesian Inference |
| Z Vector              | Likelihood, Medication Coverage,  Cost-Quality | Planning, Standardisation,  Drug/Specialty | Intangible, Tangible, Metrics         | Quantization of Fill, Scaling  data over States | Appropriation of Context, Rate  of Change, Canberra Distance |
| Likelihood            | Likelihood                               | Medication Coverage, Cost-Quality        | Unit Values, Tangible Quantities      | Avalability and Awareness,  Empirical Risk, Cognitive Bias, Scaling - zero mean, unit variance | Gaussian Covariance with Factor  Analysis, Likelihood Estimation, Vectorization |

##### Sample of Original Likelihood from Health realm

| "1770571754" | "Neurology" | "GABAPENTIN"                     | 0.093170598  |
| ------------ | ----------- | -------------------------------- | ------------ |
| "1518911478" | "Neurology" | "AZILECT"                        | -0.037137571 |
| "1518911478" | "Neurology" | "EXELON"                         | 0.014948197  |
| "1518911478" | "Neurology" | "NAMENDA"                        | 0.032271213  |
| "1518911478" | "Neurology" | "ENTACAPONE"                     | 0.050770747  |
| "1518911478" | "Neurology" | "CARBAMAZEPINE ER"               | 0.074583285  |
| "1518911478" | "Neurology" | "SELEGILINE HCL"                 | 0.076292022  |
| "1518911478" | "Neurology" | "LEVETIRACETAM ER"               | 0.078925757  |
| "1518911478" | "Neurology" | "LEVETIRACETAM"                  | 0.08277765   |
| "1518911478" | "Neurology" | "CARBAMAZEPINE"                  | 0.083560135  |
| "1518911478" | "Neurology" | "AMANTADINE"                     | 0.083629731  |
| "1518911478" | "Neurology" | "PHENYTOIN SODIUM  EXTENDED"     | 0.087307397  |
| "1518911478" | "Neurology" | "PROPRANOLOL HCL"                | 0.088534968  |
| "1518911478" | "Neurology" | "PHENOBARBITAL"                  | 0.090063832  |
| "1518911478" | "Neurology" | "CARBIDOPA-LEVODOPA"             | 0.0904193    |
| "1518911478" | "Neurology" | "LAMOTRIGINE"                    | 0.091639703  |
| "1518911478" | "Neurology" | "NORTRIPTYLINE HCL"              | 0.09202879   |
| "1518911478" | "Neurology" | "DONEPEZIL HCL"                  | 0.092166689  |
| "1518911478" | "Neurology" | "PRIMIDONE"                      | 0.092187741  |
| "1518911478" | "Neurology" | "VALPROIC ACID"                  | 0.092956166  |
| "1518911478" | "Neurology" | "TRIHEXYPHENIDYL HCL"            | 0.093067335  |
| "1518911478" | "Neurology" | "AMITRIPTYLINE HCL"              | 0.093720514  |
| "1437376571" | "Neurology" | "VENLAFAXINE HCL"                | 0.090408988  |
| "1437376571" | "Neurology" | "CLOPIDOGREL"                    | 0.09122497   |
| "1437376571" | "Neurology" | "GABAPENTIN"                     | 0.091343735  |
| "1437376571" | "Neurology" | "LEVETIRACETAM"                  | 0.09259391   |
| "1437376571" | "Neurology" | "ATORVASTATIN CALCIUM"           | 0.09259497   |
| "1437376571" | "Neurology" | "TRIAMTERENE-HYDROCHLOROTHIAZID" | 0.093162852  |
| "1427025626" | "Neurology" | "TECFIDERA"                      | -0.928945049 |
| "1427025626" | "Neurology" | "OXYCONTIN"                      | -0.116401148 |
| "1427025626" | "Neurology" | "VIMPAT"                         | -0.041856501 |
| "1427025626" | "Neurology" | "FENTANYL"                       | 0.042587759  |
| "1427025626" | "Neurology" | "MORPHINE SULFATE ER"            | 0.059925824  |
| "1427025626" | "Neurology" | "BUTALBITAL-ACETAMINOPHEN-CAFFE" | 0.074524282  |
| "1427025626" | "Neurology" | "METHYLPHENIDATE HCL"            | 0.074818883  |
| "1427025626" | "Neurology" | "HYDROCODONE-ACETAMINOPHEN"      | 0.08172353   |
| "1427025626" | "Neurology" | "HYDROMORPHONE HCL"              | 0.080569729  |
| "1427025626" | "Neurology" | "HYDROMORPHONE HCL"              | 0.080569729  |
| "1427025626" | "Neurology" | "OXYCODONE-ACETAMINOPHEN"        | 0.085467085  |
| "1427025626" | "Neurology" | "CARBAMAZEPINE"                  | 0.082779152  |
| "1427025626" | "Neurology" | "ENDOCET"                        | 0.084962786  |
| "1427025626" | "Neurology" | "OXYCODONE HCL"                  | 0.085303442  |
| "1427025626" | "Neurology" | "DULOXETINE HCL"                 | 0.086574722  |
| "1427025626" | "Neurology" | "MORPHINE SULFATE"               | 0.089126987  |
| "1427025626" | "Neurology" | "MORPHINE SULFATE"               | 0.089126987  |
| "1427025626" | "Neurology" | "ONDANSETRON HCL"                | 0.09006895   |
| "1427025626" | "Neurology" | "OXCARBAZEPINE"                  | 0.089007484  |
| "1427025626" | "Neurology" | "PRAMIPEXOLE  DIHYDROCHLORIDE"   | 0.089026936  |
| "1427025626" | "Neurology" | "PHENYTOIN SODIUM  EXTENDED"     | 0.08996351   |
| "1427025626" | "Neurology" | "GABAPENTIN"                     | 0.090377591  |
| "1427025626" | "Neurology" | "ATORVASTATIN CALCIUM"           | 0.090876459  |
| "1427025626" | "Neurology" | "CARISOPRODOL"                   | 0.091231306  |
| "1427025626" | "Neurology" | "DEXAMETHASONE"                  | 0.091438318  |
| "1427025626" | "Neurology" | "TRAZODONE HCL"                  | 0.091430622  |
| "1427025626" | "Neurology" | "LEVETIRACETAM"                  | 0.091500747  |
| "1427025626" | "Neurology" | "ROPINIROLE HCL"                 | 0.091800481  |
| "1427025626" | "Neurology" | "TOPIRAMATE"                     | 0.092031059  |
| "1427025626" | "Neurology" | "TEMAZEPAM"                      | 0.09245314   |
| "1427025626" | "Neurology" | "OMEPRAZOLE"                     | 0.092470243  |
| "1427025626" | "Neurology" | "NORTRIPTYLINE HCL"              | 0.092687114  |
| "1427025626" | "Neurology" | "LORAZEPAM"                      | 0.092713731  |
| "1427025626" | "Neurology" | "CLONAZEPAM"                     | 0.093055715  |
| "1427025626" | "Neurology" | "PROCHLORPERAZINE  MALEATE"      | 0.093141668  |
| "1427025626" | "Neurology" | "ALPRAZOLAM"                     | 0.093418127  |
| "1427025626" | "Neurology" | "LAMOTRIGINE"                    | 0.093346281  |
| "1427025626" | "Neurology" | "TRAMADOL HCL"                   | 0.093495801  |
| "1427025626" | "Neurology" | "CITALOPRAM HBR"                 | 0.094611187  |
| "1023275039" | "Neurology" | "NAMENDA"                        | 0.030856042  |
| "1023275039" | "Neurology" | "MEMANTINE HCL"                  | 0.042910086  |
| "1023275039" | "Neurology" | "SUMATRIPTAN SUCCINATE"          | 0.090037787  |
| "1023275039" | "Neurology" | "PRIMIDONE"                      | 0.0903016    |
| "1023275039" | "Neurology" | "AMITRIPTYLINE HCL"              | 0.090399934  |
| "1023275039" | "Neurology" | "GABAPENTIN"                     | 0.092600397  |
| "1023275039" | "Neurology" | "PROPRANOLOL HCL"                | 0.092561753  |
| "1023275039" | "Neurology" | "TOPIRAMATE"                     | 0.093368946  |
| "1023275039" | "Neurology" | "QUETIAPINE FUMARATE"            | 0.093211996  |
| "1023275039" | "Neurology" | "DONEPEZIL HCL"                  | 0.094327027  |
| "1295782456" | "Neurology" | "XENAZINE"                       | -1.539050974 |
| "1295782456" | "Neurology" | "AZILECT"                        | -0.035201975 |
| "1295782456" | "Neurology" | "LYRICA"                         | -0.004742128 |
| "1295782456" | "Neurology" | "ENTACAPONE"                     | 0.014090992  |
| "1295782456" | "Neurology" | "NAMENDA"                        | 0.030335273  |

##### Table of Metrics

|                                |             |             |                       |                |                       |           |       |
| ------------------------------ | ----------- | ----------- | --------------------- | -------------- | --------------------- | --------- | ----- |
| Attribute                      | Type        | Computation | Entity                | Sub Entity 2   | Super Entity          | Bundle    | Value |
| Family                         | weight      | manual      | specialty_description |                |                       |           | 3     |
| Clinic                         | weight      | manual      | specialty_description |                |                       |           | 5     |
| Department                     | weight      | manual      | specialty_description |                |                       |           | 15    |
| Hospital                       | weight      | manual      | specialty_description |                |                       |           | 7     |
| Disciplinary                   | weight      | manual      | specialty_description |                |                       |           | 12    |
| Healthcare                     | weight      | manual      | specialty_description |                |                       |           | 10    |
| Headache                       | weight      | manual      | drug_name             |                |                       |           | 1     |
| Nausea                         | weight      | manual      | drug_name             |                |                       |           | 2     |
| Constipation                   | weight      | manual      | drug_name             |                |                       |           | 3     |
| Diarrhoea                      | weight      | manual      | drug_name             |                |                       |           | 4     |
| Heart Problems                 | weight      | manual      | drug_name             |                |                       |           | 5     |
| Breathing                      | weight      | manual      | drug_name             |                |                       |           | 6     |
| Allergic Reactions             | weight      | manual      | drug_name             |                |                       |           | 7     |
| Pricing Factor                 | determinant | derived     | claim_per_dollar      | cost_per_claim | country               | reasoning | 0     |
| Multivariate Log Normal        | variance    | statistics  | factor                | covariance     | state                 |           | 0     |
| Beneficiary Average Risk Score | database    | summary     | npi                   |                |                       |           | 0     |
| Health Risk                    | ratio       | aggregate   | npi                   |                | specialty             |           | 0     |
| Outlier Prediction Band        | probability | formulaic   | npi                   |                | specialty             |           | 0     |
| Median Factor Insight          | probability | formulaic   | npi                   |                | specialty             |           | 0     |
| Confusion Matrix (Awareness)   | matrix      | derived     | specialty_description |                | drug_name             |           | 0     |
| Cohen's Kappa (Availability)   | score       | derived     | npi                   |                | specialty_description |           | 0     |
