-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.19-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for drug_aggregates
CREATE DATABASE IF NOT EXISTS `drug_aggregates` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `drug_aggregates`;

-- Dumping structure for table drug_aggregates.drug_categories
CREATE TABLE IF NOT EXISTS `drug_categories` (
  `drug_name` varchar(200) DEFAULT NULL,
  `generic_name` varchar(200) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  KEY `idx_category` (`category`),
  KEY `idx_drug` (`drug_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table drug_aggregates.drug_national_summary
CREATE TABLE IF NOT EXISTS `drug_national_summary` (
  `drug_name` varchar(50) NOT NULL,
  `generic_name` varchar(50) NOT NULL,
  `prescribers` int(11) NOT NULL,
  `part_d_claims` int(11) NOT NULL,
  `part_d_30_day_fills` int(11) NOT NULL,
  `agg_cost_part_d` double NOT NULL,
  `med_bene_count` int(11) NOT NULL,
  `ge65_sup` varchar(10) NOT NULL DEFAULT '',
  `med_part_d_claims_65` int(11) NOT NULL,
  `part_d_30_day_fills_65` int(11) NOT NULL,
  `agg_cost_part_d_65` double NOT NULL,
  `bene_65_sup` varchar(10) NOT NULL DEFAULT '',
  `med_bene_65` int(11) NOT NULL,
  `opioid` varchar(1) NOT NULL DEFAULT '',
  `antibiotic` varchar(1) NOT NULL DEFAULT '',
  `high_risk` varchar(1) NOT NULL DEFAULT '',
  `antipsychotic` varchar(1) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table drug_aggregates.medicare_enrolees_utilizers
CREATE TABLE IF NOT EXISTS `medicare_enrolees_utilizers` (
  `bene_residence` varchar(200) NOT NULL,
  `bene_enrolled` double NOT NULL,
  `bene_utilizing` double NOT NULL,
  `bene_65_enrolled` double NOT NULL,
  `bene_65_utilizing` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table drug_aggregates.metric_attributes
CREATE TABLE IF NOT EXISTS `metric_attributes` (
  `attr_name` varchar(100) NOT NULL,
  `attr_type` varchar(50) NOT NULL,
  `computation` varchar(50) DEFAULT NULL,
  `attr_entity` varchar(50) NOT NULL,
  `attr_entity_2` varchar(50) DEFAULT NULL,
  `attr_entity_3` varchar(50) DEFAULT NULL,
  `attr_bundle` varchar(50) DEFAULT NULL,
  `attr_value` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table drug_aggregates.state_wise_drug_state
CREATE TABLE IF NOT EXISTS `state_wise_drug_state` (
  `state` varchar(200) NOT NULL,
  `drug_name` varchar(200) NOT NULL,
  `generic_name` varchar(200) NOT NULL,
  `prescribers` int(11) NOT NULL,
  `part_d_claims` int(11) NOT NULL,
  `part_d_30_day_fills` int(11) NOT NULL,
  `agg_cost_part_d` double NOT NULL,
  `med_bene_count` int(11) NOT NULL DEFAULT '0',
  `ge65_sup` varchar(3) NOT NULL DEFAULT '',
  `part_d_claims_bene_65` int(11) NOT NULL DEFAULT '0',
  `part_d_30_day_fills_bene_65` int(11) NOT NULL DEFAULT '0',
  `agg_cost_part_d_bene_65` double NOT NULL,
  `bene_65_sup` varchar(3) NOT NULL DEFAULT '',
  `bene_65_count` int(11) NOT NULL DEFAULT '0',
  `opioid` varchar(1) NOT NULL DEFAULT '',
  `antibiotic` varchar(1) NOT NULL DEFAULT '',
  `HRM` varchar(1) NOT NULL DEFAULT '',
  `antipsychotic` varchar(1) NOT NULL DEFAULT '',
  KEY `idx_drug` (`state`,`drug_name`),
  KEY `idx_state` (`state`),
  KEY `idx_drug_type` (`opioid`,`antibiotic`,`HRM`,`antipsychotic`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
