-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.19-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for backup_table_metadata
CREATE DATABASE IF NOT EXISTS `backup_table_metadata` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `backup_table_metadata`;

-- Dumping structure for table backup_table_metadata.backup_table_metadata
CREATE TABLE IF NOT EXISTS `backup_table_metadata` (
  `npi` int(11) NOT NULL DEFAULT '0',
  `nppes_provider_last_org_name` varchar(200) DEFAULT NULL,
  `nppes_provider_first_name` varchar(200) DEFAULT NULL,
  `nppes_provider_city` varchar(160) NOT NULL DEFAULT '',
  `nppes_provider_state` varchar(120) NOT NULL DEFAULT '',
  `specialty_description` varchar(150) NOT NULL DEFAULT '',
  `description_flag` varchar(6) DEFAULT NULL,
  `drug_name` varchar(200) NOT NULL DEFAULT '',
  `generic_name` varchar(200) NOT NULL DEFAULT '',
  `bene_count` int(11) DEFAULT NULL,
  `total_claim_count` int(11) DEFAULT NULL,
  `total_30_day_fill_count` float NOT NULL DEFAULT '0',
  `total_day_supply` float NOT NULL DEFAULT '0',
  `total_drug_cost` float NOT NULL DEFAULT '0',
  `bene_count_ge65` int(11) NOT NULL DEFAULT '0',
  `bene_count_ge65_suppress_flag` varchar(6) DEFAULT NULL,
  `total_claim_count_ge65` int(11) NOT NULL DEFAULT '0',
  `ge65_suppress_flag` varchar(5) DEFAULT NULL,
  `total_30_day_fill_count_ge65` int(11) NOT NULL DEFAULT '0',
  `total_day_supply_ge65` int(11) NOT NULL DEFAULT '0',
  `total_drug_cost_ge65` float NOT NULL DEFAULT '0',
  KEY `idx_state` (`nppes_provider_state`),
  KEY `idx_specialty` (`specialty_description`,`drug_name`),
  KEY `idx_cost` (`total_drug_cost`,`total_day_supply`,`total_30_day_fill_count`,`total_claim_count`),
  KEY `idx_identifier` (`nppes_provider_state`,`specialty_description`,`drug_name`),
  KEY `idx_value` (`total_claim_count`,`bene_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table backup_table_metadata.npi_entropy
CREATE TABLE IF NOT EXISTS `npi_entropy` (
  `npi` int(11) NOT NULL DEFAULT '0',
  `nppes_provider_state` varchar(50) DEFAULT NULL,
  `specialty_description` varchar(150) NOT NULL DEFAULT '',
  `drug_count` bigint(21) NOT NULL DEFAULT '0',
  `claim_count` decimal(32,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table backup_table_metadata.specialty_integration
CREATE TABLE IF NOT EXISTS `specialty_integration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `specialty` varchar(255) NOT NULL,
  `primary_ownership` varchar(255) NOT NULL,
  `weight` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_sp` (`specialty`),
  KEY `idx_own` (`primary_ownership`,`weight`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=utf8 COMMENT='Specialties weights';

-- Data exporting was unselected.
-- Dumping structure for table backup_table_metadata.specialty_weights
CREATE TABLE IF NOT EXISTS `specialty_weights` (
  `specialty_description` varchar(128) DEFAULT NULL,
  `primary_ownership` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table backup_table_metadata.states
CREATE TABLE IF NOT EXISTS `states` (
  `state` varchar(50) DEFAULT NULL,
  `abbrev` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table backup_table_metadata.states_integration
CREATE TABLE IF NOT EXISTS `states_integration` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `state` varchar(128) NOT NULL,
  `death_ratio` double NOT NULL,
  `iteration` int(11) NOT NULL DEFAULT '0',
  `deaths` int(11) DEFAULT NULL,
  `population` int(11) DEFAULT NULL,
  `abbrev` varchar(5) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `idx_itr` (`iteration`),
  KEY `idx_state` (`state`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='All states integrated into Max, Arg Max to Min';

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
