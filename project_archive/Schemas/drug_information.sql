-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.19-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for drug_information
CREATE DATABASE IF NOT EXISTS `drug_information` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `drug_information`;

-- Dumping structure for table drug_information.cost_quality_csv
CREATE TABLE IF NOT EXISTS `cost_quality_csv` (
  `nppes_provider_state` varchar(120) NOT NULL DEFAULT '',
  `specialty_description` varchar(150) NOT NULL DEFAULT '',
  `drug_name` varchar(200) NOT NULL DEFAULT '',
  `SUM(total_drug_cost)` double NOT NULL DEFAULT '0',
  `SUM(total_claim_count)` decimal(32,0) NOT NULL DEFAULT '0',
  `SUM(total_30_day_fill_count)` double NOT NULL DEFAULT '0'
) ENGINE=CSV DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table drug_information.pa_cost_quality_table
CREATE TABLE IF NOT EXISTS `pa_cost_quality_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nppes_provider_state` varchar(10) DEFAULT NULL,
  `specialty_description` varchar(128) NOT NULL DEFAULT '0',
  `drug_name` varchar(128) NOT NULL DEFAULT '0',
  `total_claim_count` int(11) NOT NULL DEFAULT '0',
  `total_bene_count` int(11) NOT NULL DEFAULT '0',
  `total_drug_cost` double NOT NULL DEFAULT '0',
  `total_day_supply` int(11) NOT NULL DEFAULT '0',
  `total_30_day_fill` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `nppes_provider_state` (`nppes_provider_state`),
  KEY `specialty_description` (`specialty_description`),
  KEY `drug_name` (`drug_name`),
  KEY `total_claim_count` (`total_claim_count`),
  KEY `total_drug_cost` (`total_drug_cost`),
  KEY `total_day_supply` (`total_day_supply`)
) ENGINE=InnoDB AUTO_INCREMENT=22765 DEFAULT CHARSET=utf8 COMMENT='Cost Quality Table obtained from table_metadata, including more parameters';

-- Data exporting was unselected.
-- Dumping structure for table drug_information.pa_drug_prescriptions
CREATE TABLE IF NOT EXISTS `pa_drug_prescriptions` (
  `nppes_provider_state` varchar(50) NOT NULL,
  `drug_name` varchar(200) NOT NULL,
  `claim_count` decimal(32,0) DEFAULT NULL,
  `npi_count` bigint(21) NOT NULL DEFAULT '0',
  `opioid_prescriber_count` decimal(25,0) DEFAULT NULL,
  `day_supply` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table drug_information.pa_state_spl_drug_npi
CREATE TABLE IF NOT EXISTS `pa_state_spl_drug_npi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `npi` varchar(200) NOT NULL,
  `nppes_provider_state` varchar(50) NOT NULL,
  `specialty_description` varchar(255) NOT NULL,
  `drug_name` varchar(200) NOT NULL,
  `total_claim_count` int(11) NOT NULL DEFAULT '0',
  `bene_count` int(11) NOT NULL DEFAULT '0',
  `opioid_prescriber` tinyint(1) NOT NULL,
  `total_day_supply` double NOT NULL DEFAULT '0',
  `drug_cost` double NOT NULL DEFAULT '0',
  `fill_count` double DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `npi` (`npi`),
  KEY `drug_name` (`drug_name`),
  KEY `specialty_description` (`specialty_description`),
  KEY `opioid_prescriber` (`opioid_prescriber`),
  KEY `total_claim_count` (`total_claim_count`),
  KEY `bene_count` (`bene_count`),
  KEY `fill_count` (`fill_count`)
) ENGINE=InnoDB AUTO_INCREMENT=1241152 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table drug_information.pa_state_wise_drug
CREATE TABLE IF NOT EXISTS `pa_state_wise_drug` (
  `nppes_provider_state` varchar(10) NOT NULL,
  `drug_name` varchar(128) NOT NULL,
  `mean_claim_count` double NOT NULL,
  `mean_cost_per_claim` double NOT NULL,
  `mean_supply_per_claim` double NOT NULL,
  `std_cost_per_claim` double NOT NULL,
  `std_supply_per_claim` double NOT NULL,
  `mean_claim_per_dollar` double NOT NULL,
  `std_claim_per_dollar` double NOT NULL,
  `mean_cost_per_fill` double NOT NULL,
  `mean_fill_count` double NOT NULL,
  KEY `idx_drug` (`drug_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table drug_information.pa_state_wise_specialty
CREATE TABLE IF NOT EXISTS `pa_state_wise_specialty` (
  `nppes_provider_state` varchar(10) DEFAULT NULL,
  `specialty_description` varchar(128) DEFAULT NULL,
  `mean_claim_count` double DEFAULT NULL,
  `mean_cost_per_claim` double DEFAULT NULL,
  `mean_supply_per_claim` double DEFAULT NULL,
  `std_cost_per_claim` double DEFAULT NULL,
  `std_supply_per_claim` double DEFAULT NULL,
  `mean_claim_per_dollar` double DEFAULT NULL,
  `std_claim_per_dollar` double DEFAULT NULL,
  `mean_bene_count` double DEFAULT NULL,
  KEY `specialty_description` (`specialty_description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table drug_information.reference_table
CREATE TABLE IF NOT EXISTS `reference_table` (
  `multiplying_factor_sigh` float DEFAULT NULL,
  `drug_reduction_factor` float DEFAULT NULL,
  `drug_count_factor` float DEFAULT NULL,
  `pricing_factor` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table drug_information.state_alpha
CREATE TABLE IF NOT EXISTS `state_alpha` (
  `state` varchar(50) DEFAULT NULL,
  `alpha` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table drug_information.state_multi_log_normal
CREATE TABLE IF NOT EXISTS `state_multi_log_normal` (
  `state` varchar(50) DEFAULT NULL,
  `multi_log_normal` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table drug_information.table_metadata
CREATE TABLE IF NOT EXISTS `table_metadata` (
  `npi` varchar(50) NOT NULL DEFAULT '0',
  `nppes_provider_last_org_name` varchar(50) NOT NULL DEFAULT '0',
  `nppes_provider_first_name` varchar(200) DEFAULT NULL,
  `nppes_provider_city` varchar(200) DEFAULT NULL,
  `nppes_provider_state` varchar(160) NOT NULL DEFAULT '',
  `specialty_description` varchar(120) NOT NULL DEFAULT '',
  `description_flag` varchar(150) NOT NULL DEFAULT '',
  `drug_name` varchar(150) DEFAULT NULL,
  `generic_name` varchar(200) NOT NULL DEFAULT '',
  `bene_count` varchar(200) NOT NULL DEFAULT '',
  `total_claim_count` varchar(50),
  `total_30_day_fill_count` varchar(50) DEFAULT '0',
  `total_day_supply` varchar(50) DEFAULT '0',
  `total_drug_cost` varchar(50) DEFAULT '0',
  `bene_count_ge65` varchar(50) DEFAULT '0',
  `bene_count_ge65_suppress_flag` varchar(50) DEFAULT '0',
  `total_claim_count_ge65` varchar(150) DEFAULT NULL,
  `ge65_suppress_flag` varchar(50) DEFAULT '0',
  `total_30_day_fill_count_ge65` varchar(150) DEFAULT NULL,
  `total_day_supply_ge65` varchar(50) DEFAULT '0',
  `total_drug_cost_ge65` varchar(50) DEFAULT '0',
  KEY `idx_city` (`nppes_provider_state`),
  KEY `idx_state` (`specialty_description`),
  KEY `idx_specialty` (`generic_name`),
  KEY `bene_count` (`bene_count`),
  KEY `total_claim_count` (`total_claim_count`),
  KEY `npi` (`npi`),
  KEY `drug_name` (`drug_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
