-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.19-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for claim_aggregates
CREATE DATABASE IF NOT EXISTS `claim_aggregates` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `claim_aggregates`;

-- Dumping structure for table claim_aggregates.pa_claim_statistics
CREATE TABLE IF NOT EXISTS `pa_claim_statistics` (
  `nppes_provider_state` varchar(5) NOT NULL,
  `specialty_description` varchar(128) NOT NULL,
  `total_claim_count` int(11) NOT NULL,
  `drug_count` int(11) NOT NULL,
  `median_claim` float NOT NULL,
  `median_supply` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table claim_aggregates.pa_specialty_entropy
CREATE TABLE IF NOT EXISTS `pa_specialty_entropy` (
  `nppes_provider_state` varchar(5) NOT NULL,
  `specialty_description` varchar(128) NOT NULL,
  `drug_count` int(11) NOT NULL,
  `npi_count` int(11) NOT NULL,
  `total_claim_count` int(11) NOT NULL,
  `median_count` int(11) NOT NULL,
  KEY `idx_specialty` (`specialty_description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
