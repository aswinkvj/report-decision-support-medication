-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.19-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for drug_database
CREATE DATABASE IF NOT EXISTS `drug_database` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `drug_database`;

-- Dumping structure for table drug_database.claim_statistics
CREATE TABLE IF NOT EXISTS `claim_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `specialty_description` varchar(255) NOT NULL,
  `claim_sum` double unsigned NOT NULL DEFAULT '0',
  `claim_length` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `mean` double unsigned NOT NULL DEFAULT '0',
  `median` double unsigned NOT NULL DEFAULT '0',
  `IQR_25` double unsigned NOT NULL DEFAULT '0',
  `IQR_75` double unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_value` (`IQR_75`,`IQR_25`,`median`,`mean`),
  KEY `idx_identifier` (`specialty_description`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=utf8 COMMENT='claim statistics';

-- Data exporting was unselected.
-- Dumping structure for table drug_database.cost_quality_table
CREATE TABLE IF NOT EXISTS `cost_quality_table` (
  `nppes_provider_state` varchar(120) NOT NULL DEFAULT '',
  `specialty_description` varchar(150) NOT NULL DEFAULT '',
  `drug_name` varchar(200) NOT NULL DEFAULT '',
  `SUM(total_drug_cost)` double NOT NULL DEFAULT '0',
  `SUM(total_claim_count)` decimal(32,0) NOT NULL DEFAULT '0',
  `SUM(total_30_day_fill_count)` double NOT NULL DEFAULT '0',
  UNIQUE KEY `idx_unique` (`nppes_provider_state`,`specialty_description`,`drug_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table drug_database.drug_prescriptions
CREATE TABLE IF NOT EXISTS `drug_prescriptions` (
  `drug_name` varchar(200) NOT NULL DEFAULT '',
  `opioid_prescriber_rate` decimal(38,30) unsigned NOT NULL DEFAULT '0.000000000000000000000000000000',
  `opioid_prescriber_rate_fills` double(38,30) unsigned NOT NULL DEFAULT '0.000000000000000000000000000000',
  `claim_count` decimal(54,0) unsigned NOT NULL,
  `30_day_fill_count` double(54,12) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table drug_database.pa_drug_factor_data
CREATE TABLE IF NOT EXISTS `pa_drug_factor_data` (
  `nppes_provider_state` varchar(120) NOT NULL DEFAULT '',
  `specialty_description` varchar(150) NOT NULL DEFAULT '',
  `drug_count` int(1) NOT NULL DEFAULT '0',
  `drug_cost` double NOT NULL DEFAULT '0',
  `value` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table drug_database.pa_drug_factor_vector
CREATE TABLE IF NOT EXISTS `pa_drug_factor_vector` (
  `nppes_provider_state` varchar(120) NOT NULL DEFAULT '',
  `specialty_description` varchar(150) NOT NULL DEFAULT '',
  `drug_count` decimal(32,0) DEFAULT NULL,
  `drug_reduction` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for view drug_database.pa_state_wise_drug
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `pa_state_wise_drug` (
	`nppes_provider_state` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`drug_name` VARCHAR(200) NOT NULL COLLATE 'utf8_general_ci',
	`mean_claim_acount` DECIMAL(14,4) NULL,
	`mean_cost_per_claim` DOUBLE NULL,
	`mean_supply_per_claim` DOUBLE NULL,
	`std_cost_per_claim` DOUBLE NULL,
	`std_supply_per_claim` DOUBLE NULL,
	`mean_claim_per_dollar` DOUBLE NULL,
	`std_claim_per_dollar` DOUBLE NULL
) ENGINE=MyISAM;

-- Dumping structure for view drug_database.pa_state_wise_specialty
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `pa_state_wise_specialty` (
	`nppes_provider_state` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`specialty_description` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',
	`mean_claim_acount` DECIMAL(14,4) NULL,
	`mean_cost_per_claim` DOUBLE NULL,
	`mean_supply_per_claim` DOUBLE NULL,
	`std_cost_per_claim` DOUBLE NULL,
	`std_supply_per_claim` DOUBLE NULL,
	`mean_claim_per_dollar` DOUBLE NULL,
	`std_claim_per_dollar` DOUBLE NULL
) ENGINE=MyISAM;

-- Dumping structure for table drug_database.specialty_entropy
CREATE TABLE IF NOT EXISTS `specialty_entropy` (
  `specialty_description` varchar(128) NOT NULL,
  `drug_count` int(11) DEFAULT NULL,
  `claim_count` int(11) DEFAULT NULL,
  `weight` mediumint(9) NOT NULL DEFAULT '0',
  `opioid_prescriber_rate` double NOT NULL,
  `opioid_prescriber_rate_fills` double NOT NULL DEFAULT '0',
  `median_count` double DEFAULT NULL,
  `IQR_count` double DEFAULT NULL,
  `median_weightage` double DEFAULT '0',
  `iqr_weightage` double DEFAULT '0',
  PRIMARY KEY (`specialty_description`),
  KEY `idx_index` (`specialty_description`),
  KEY `idx_values` (`claim_count`,`drug_count`),
  KEY `idx_params` (`opioid_prescriber_rate`,`median_count`,`IQR_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Entropies for Specialty based on DISTINCT npi and state values.';

-- Data exporting was unselected.
-- Dumping structure for table drug_database.states_death
CREATE TABLE IF NOT EXISTS `states_death` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(50) NOT NULL,
  `population` int(11) NOT NULL,
  `deaths` int(11) NOT NULL,
  `abbrev` varchar(12) NOT NULL,
  `ratio` double NOT NULL DEFAULT '0',
  `sigh` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for view drug_database.pa_state_wise_drug
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `pa_state_wise_drug`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pa_state_wise_drug` AS (select `cqt`.`nppes_provider_state` AS `nppes_provider_state`,`cqt`.`drug_name` AS `drug_name`,avg(`cqt`.`total_claim_count`) AS `mean_claim_acount`,avg((`cqt`.`drug_cost` / `cqt`.`total_claim_count`)) AS `mean_cost_per_claim`,avg((`cqt`.`total_day_supply` / `cqt`.`total_claim_count`)) AS `mean_supply_per_claim`,std((`cqt`.`drug_cost` / `cqt`.`total_claim_count`)) AS `std_cost_per_claim`,std((`cqt`.`total_day_supply` / `cqt`.`total_claim_count`)) AS `std_supply_per_claim`,avg((`cqt`.`total_claim_count` / `cqt`.`total_day_supply`)) AS `mean_claim_per_dollar`,std((`cqt`.`total_claim_count` / `cqt`.`total_day_supply`)) AS `std_claim_per_dollar` from `drug_information`.`pa_state_spl_drug_npi` `cqt` group by `cqt`.`drug_name`);

-- Dumping structure for view drug_database.pa_state_wise_specialty
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `pa_state_wise_specialty`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pa_state_wise_specialty` AS (select `cqt`.`nppes_provider_state` AS `nppes_provider_state`,`cqt`.`specialty_description` AS `specialty_description`,avg(`cqt`.`total_claim_count`) AS `mean_claim_acount`,avg((`cqt`.`drug_cost` / `cqt`.`total_claim_count`)) AS `mean_cost_per_claim`,avg((`cqt`.`total_day_supply` / `cqt`.`total_claim_count`)) AS `mean_supply_per_claim`,std((`cqt`.`drug_cost` / `cqt`.`total_claim_count`)) AS `std_cost_per_claim`,std((`cqt`.`total_day_supply` / `cqt`.`total_claim_count`)) AS `std_supply_per_claim`,avg((`cqt`.`total_claim_count` / `cqt`.`total_day_supply`)) AS `mean_claim_per_dollar`,std((`cqt`.`total_claim_count` / `cqt`.`total_day_supply`)) AS `std_claim_per_dollar` from `drug_information`.`pa_state_spl_drug_npi` `cqt` group by `cqt`.`specialty_description`);

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
