-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.19-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for optimization_tables
CREATE DATABASE IF NOT EXISTS `optimization_tables` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `optimization_tables`;

-- Dumping structure for table optimization_tables.pa_cognitive_bias_estimate
CREATE TABLE IF NOT EXISTS `pa_cognitive_bias_estimate` (
  `specialty_description` varchar(128) NOT NULL,
  `drug_name` varchar(128) NOT NULL,
  `pred_specialty` varchar(128) NOT NULL,
  `pred_drug_name` varchar(128) NOT NULL,
  `hindrance_value` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table optimization_tables.pa_cost_function
CREATE TABLE IF NOT EXISTS `pa_cost_function` (
  `nppes_provider_state` varchar(5) NOT NULL,
  `specialty_description` varchar(128) NOT NULL,
  `drug_name` varchar(128) NOT NULL,
  `npi` int(11) NOT NULL,
  `claim_per_dollar` double DEFAULT NULL,
  `supply_per_claim` double DEFAULT NULL,
  `cost_per_claim` double DEFAULT NULL,
  `bene_count` int(11) DEFAULT NULL,
  `specialty_weight` double DEFAULT NULL,
  `drug_factor_specialty` double DEFAULT NULL,
  `drug_weight` double DEFAULT NULL,
  `specialty_factor_drug` double DEFAULT NULL,
  `specialty_mean_cost_per_claim` double DEFAULT NULL,
  `specialty_mean_supply_per_claim` double DEFAULT NULL,
  `specialty_mean_claim_per_dollar` double DEFAULT NULL,
  `specialty_std_cost_per_claim` double DEFAULT NULL,
  `specialty_std_supply_per_claim` double DEFAULT NULL,
  `specialty_std_claim_per_dollar` double DEFAULT NULL,
  `drug_mean_cost_per_claim` double DEFAULT NULL,
  `drug_mean_supply_per_claim` double DEFAULT NULL,
  `drug_mean_claim_per_dollar` double DEFAULT NULL,
  `drug_std_cost_per_claim` double DEFAULT NULL,
  `drug_std_supply_per_claim` double DEFAULT NULL,
  `drug_std_claim_per_dollar` double DEFAULT NULL,
  `npi_claim_count` int(11) DEFAULT NULL,
  `npi_drug_cost` double DEFAULT NULL,
  `npi_day_supply` int(11) DEFAULT NULL,
  `npi_bene_count` int(11) DEFAULT NULL,
  `opioid_prescriber` int(11) DEFAULT NULL,
  `generic` double DEFAULT NULL,
  `brand` double DEFAULT NULL,
  `other` double DEFAULT NULL,
  `opioid` double DEFAULT NULL,
  `antibiotic` double DEFAULT NULL,
  `antipsychotic` double DEFAULT NULL,
  `hrm` double DEFAULT NULL,
  `part_d_claims` int(11) DEFAULT NULL,
  `med_bene_count` int(11) DEFAULT NULL,
  `agg_cost_part_d` double DEFAULT NULL,
  `prescribers` int(11) DEFAULT NULL,
  `drug_state_category` varchar(20) DEFAULT NULL,
  `drug_category` varchar(20) DEFAULT NULL,
  `bene_risk` double DEFAULT NULL,
  `medicare_prvdr_enroll_status` varchar(2) DEFAULT NULL,
  KEY `specialty_description` (`specialty_description`),
  KEY `drug_name` (`drug_name`),
  KEY `npi` (`npi`),
  KEY `linear_key` (`cost_per_claim`,`supply_per_claim`,`claim_per_dollar`),
  KEY `idx_order` (`specialty_mean_supply_per_claim`,`specialty_std_supply_per_claim`,`bene_risk`),
  KEY `idx_risk` (`opioid`,`antibiotic`,`antipsychotic`,`hrm`,`generic`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cost Function for the State ''PA'''
/*!50100 PARTITION BY LINEAR KEY (cost_per_claim,supply_per_claim,claim_per_dollar)
PARTITIONS 50 */;

-- Data exporting was unselected.
-- Dumping structure for table optimization_tables.pa_kmeans_y_pred_drug_factor_spl
CREATE TABLE IF NOT EXISTS `pa_kmeans_y_pred_drug_factor_spl` (
  `specialty_description` varchar(128) NOT NULL,
  `drug_name` varchar(128) NOT NULL,
  `npi` int(10) NOT NULL,
  `drug_factor_spl` double NOT NULL,
  `cluster` tinyint(2) NOT NULL,
  `batch` tinyint(1) NOT NULL DEFAULT '1',
  KEY `idx_npi` (`npi`),
  KEY `idx_drug` (`drug_name`),
  KEY `idx_specialty` (`specialty_description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table optimization_tables.pa_kmeans_y_pred_drug_weight
CREATE TABLE IF NOT EXISTS `pa_kmeans_y_pred_drug_weight` (
  `specialty_description` varchar(128) NOT NULL,
  `drug_name` varchar(128) NOT NULL,
  `npi` int(11) NOT NULL,
  `drug_weight` double NOT NULL,
  `cluster` int(11) NOT NULL,
  `batch` int(11) NOT NULL,
  KEY `idx_npi` (`npi`),
  KEY `idx_drug` (`drug_name`,`specialty_description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table optimization_tables.pa_kmeans_y_pred_spl_factor_drug
CREATE TABLE IF NOT EXISTS `pa_kmeans_y_pred_spl_factor_drug` (
  `specialty_description` varchar(128) NOT NULL,
  `drug_name` varchar(128) NOT NULL,
  `npi` int(10) NOT NULL,
  `specialty_factor_drug` double NOT NULL,
  `cluster` tinyint(2) NOT NULL,
  `batch` tinyint(1) NOT NULL DEFAULT '1',
  KEY `idx_specialty` (`specialty_description`),
  KEY `idx_drug` (`drug_name`),
  KEY `idx_npi` (`npi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table optimization_tables.pa_kmeans_y_pred_spl_weight
CREATE TABLE IF NOT EXISTS `pa_kmeans_y_pred_spl_weight` (
  `specialty_description` varchar(128) NOT NULL,
  `drug_name` varchar(128) NOT NULL,
  `npi` int(11) NOT NULL,
  `specialty_weight` double NOT NULL,
  `cluster` int(11) NOT NULL,
  `batch` int(11) NOT NULL,
  KEY `idx_drug` (`drug_name`),
  KEY `idx_specialty` (`specialty_description`),
  KEY `idx_npi` (`npi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table optimization_tables.pa_likelihood_cost_quality
CREATE TABLE IF NOT EXISTS `pa_likelihood_cost_quality` (
  `nppes_provider_state` varchar(5) NOT NULL,
  `batch` smallint(6) NOT NULL,
  `sample` smallint(6) NOT NULL,
  `lambda` double NOT NULL,
  `psi_fill_claim` double NOT NULL,
  `psi_claim_fill` double NOT NULL,
  `psi_cost_fill` double NOT NULL,
  `psi_fill_cost` double NOT NULL,
  `psi_cost_claim` double NOT NULL,
  `psi_claim_cost` double NOT NULL,
  `npi` int(11) NOT NULL,
  `specialty_description` varchar(128) NOT NULL,
  `drug_name` varchar(128) NOT NULL,
  `drug_cost` double NOT NULL,
  `fill_count` double NOT NULL,
  `claim_count` int(11) NOT NULL,
  `day_supply` int(11) NOT NULL,
  `gaussian_covariance` double NOT NULL,
  `natural_exponential_value` decimal(34,30) NOT NULL,
  `likelihood` decimal(36,30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table optimization_tables.pa_likelihood_medication_coverage
CREATE TABLE IF NOT EXISTS `pa_likelihood_medication_coverage` (
  `nppes_provider_state` varchar(5) NOT NULL,
  `batch` varchar(5) NOT NULL,
  `sample` varchar(5) NOT NULL,
  `lambda` double NOT NULL,
  `psi_cost_supply` double NOT NULL,
  `psi_supply_cost` double NOT NULL,
  `psi_claim_supply` double NOT NULL,
  `psi_supply_claim` double NOT NULL,
  `psi_cost_claim` double NOT NULL,
  `psi_claim_cost` double NOT NULL,
  `npi` int(11) NOT NULL,
  `specialty_description` varchar(128) NOT NULL,
  `drug_name` varchar(128) NOT NULL,
  `drug_cost` double NOT NULL,
  `bene_count` int(11) NOT NULL,
  `claim_count` int(11) NOT NULL,
  `day_supply` int(11) NOT NULL,
  `gaussian_covariance` double NOT NULL,
  `natural_exponential_value` decimal(34,30) NOT NULL,
  `likelihood` decimal(36,30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table optimization_tables.pa_spl_drug_shared_states
CREATE TABLE IF NOT EXISTS `pa_spl_drug_shared_states` (
  `specialty_description` varchar(128) NOT NULL,
  `drug_name` varchar(128) NOT NULL,
  `empirical_estimation` int(11) NOT NULL,
  `empirical_error` double DEFAULT NULL,
  KEY `idx_spl_drug` (`specialty_description`,`drug_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
