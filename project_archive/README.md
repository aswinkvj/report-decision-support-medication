#### Software Requirements

Project
------

- Mysql 5.6+
- Julia 0.6.0
- Conda Python 3.6
- Microsoft Excel 2010

Report
------

- Node.js

Prerequisites
-------------

- The state-wise databases must be created using the syntax which is same as those provided in the `Schemas` folder.
- The project prefers a pdf format but the project docs can be run using the markserv plugin at port 3000, localhost, for viewing the docs in the browser
- Each exported SQL schema content from the relevant database is exported based on the ${DATABASE_NAME}.sql

Software Packages
-----------------

> These command should be run: 

pip install grip
pip install pymysql
pip install scikit-learn
pip install seaborn
pip install matplotlib
pip install scipy
pip install jupyter
pip install pandas
pip install lightgbm
pip install numpy

npm install -g markserv

grip README.md 3000
markserv --port 3000 --file capstone_project_report.md

cd /path/to/docs/folder # such that docs/*.md is visible

markserv -p 3000 --file capstone_project_report.md

> These Julia commands should be run:

Pkg.add("MySQL")
Pkg.add("Logging")
Pkg.add("PyCall")
Pkg.add("DataFrames")

> Inside the Conda module for the Julia project, these commands must be run

pip install scipy
pip install scikit-learn

Libraries Used (Python)
--------------

Scikit Learn
Scipy
Matplotlib
Seaborn
Jupyter
PyMySQL, the MySQL client
Pandas
Lightgbm
Numpy

Libraries Used (Julia)
---------------------

MySQL
Logging
PyCall
DataFrames

Folder Structure
----------------

- The code/ folder contains the codebase required to generate the cluster, the gradient boosting regressor values, the Z vector and the Four Factors. 

	- code\logs: The log files obtained from DecisionTreeClassifiers of GradientBoostingRegressor
	- code\Plotting: all the code for plotting functions

The /path/to/docs/folder contains the documentation for the project provided in markdown format. 

	- docs\us-opiate-prescriptions: Files obtained form Kaggle dataset
	- docs\Worksheets: Files analysed analysis based on a manually selected variables for a decison tree classifier presented in report
	
- The Notebook/ folder contains all the relevant notebooks used for this data analysis and machine learning project.

Before going to the notebook contents, please view the Notebook exported html and markdown files as MySQL connectivity kis a restriction for some notebooks.

	- logs: The log folder contains the log content from the Gradient Boosting Regressor
	- pickle: The pickle folder contains the exported pickle objects

	- Exports\Death - Evidence, Hypothesis and Risk: Exported notebook from Death domain
	- Exports\Factors - Estimation and Risk: Exported notebook from factor domain
	- Exports\General: Exported notebook from general notebooks
	- Exports\Likelihood - Optimisation and Risk: Exported notebook from Likelihood
	- Exports\metaobjects\cost_quality: Exported content from the Notebook regarding the Medication Coverage and Cost/Quality domains
	- Exports\metaobjects\npi: Contains useful pickle objects

	- Exports\all.ipynb: Contains a notebook evaluated for generating content from two different notebooks to generate a distribution.
	- 4_Factors.ipynb: Contains the Estimation, Kendall Tau Correlation and PearsonR correlation of Factor weights (Specialty weight and drug weight), and Latent Variables (Specialty Factor for Drugs and Drug Factor for Specialty).
	- Canberra_Distance.ipynb: Contains the canberra distance distribution and matric values taken for selected prescription
	- Conjugate_Gradient.ipynb: Contains the parameters which can be used for optimizing the domain using a proposed Conjugate Gradient algorithm
	- Cost_Function.ipynb: Contains the GradientBoostingTRegressor algorithm, Stochastic Gradient Descent Regressor and Classifier; and K-Means algorithm with the inertia value.
	- Death_Evaluation.ipynb: Contains proposed notes for evaluating the death rate and prescribing rate with a simulation of prescription entries using MCMC and a Lagrangian to evaluate the dual optimality.
	- Information_Architecture and Decision_Models.ipynb: Contains the concepts based on IA and decision models proposed which have bene used in the project.
	- Likelihood_Estimation.ipynb: Contains the likelihood estimation evaluated for Health and Safety realms
	- Likelihood_Estimation_Optimization.ipynb: Contains the likelihood estimation evaluated for Medication Coverage and Cost and Quality
	- Linear_Regression.ipynb: Contains linear regression taken against Specialty Weights and Drug Weights with a feature selection and their scores and metric values
	- Minimum_Mean_Square_Estimation.ipynb: Contains a demonstration of MMSE conducted against selected specialty and drug combinations
	- Sigh_Value.ipynb: Contains an evaluation of State Deaths and Population with the Death Ratio, termed as Sigh Value Mapping, with distributions and a decision tree classifier
	- Z_Vector_Likelihood.ipynb: Contains the evaluation of Z Vector calculated using Machine Learning algorithms and then settling with the Naive Bayes parameter estimation values.

- The 'R Files' folder contains all the R files worked on and obtained from Kaggle dataset

- The Schemas folder consists of SQL code for initiating the databases and database table schema

- The Spreadsheets folder consists of .xlsx files worked on and generated from the MySQL databases

	- Spreadsheets\PA-selected: All 'Pennsylvania' related files and data rows

Executing the Jupyter Notebooks
-------------------------------

> jupyter notebook "${file}.ipynb" --port 8888

Executing the codebase
----------------------

> python FactorAnalysis.py --state PA --write-pickle True

> python FactorAnalysis.py --state PA --write-database False --write-pickle False --write-coeff 0 --tau False --pearsonr False

> python CostFunction.py --state PA --write-database True --write-pickle True

> python GradientBoostingRegressor.py --state PA --run-boosting 1 --write-pickle 1 --write-database 1

> python KMeans_Data_Load.py --state PA --run-lambda-health 1 --start 0 

> python KMeans_Data_Load.py --state PA --run-lambda-safety 1 --start 0 

> python KMeans_Data_Load.py --state PA --run-z-safety 1 --start 0 --batch 1

> python KMeans_Data_Load.py --state PA --run-z-health 1 --start 0 --batch 1

> python Z_Vector_Likelihood.py --state PA --run-empirical-error --specialty-selected 'Physician Assistant'

> python Z_Vector_Likelihood.py --state PA --run-cognitive-bias

